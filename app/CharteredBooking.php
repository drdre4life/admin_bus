<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CharteredBooking extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chartered_bookings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['contact_name', 'contact_mobile', 'pickup_location', 'destination', 'budget'];

}
