<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Booking;
use App\Models\CustomerFeedback;
use App\Models\BuscomBooking;
use App\Models\BookingNote;
use App\Models\Operator;
use App\Models\Trip;
use App\Models\User;
use App\Models\Passenger;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mockery\CountValidator\Exception;
use Session;
use Auth;
use DB;

class BookingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        // if (!$request->session()->has('operator_id')){
        //     $request->session()->flash('select', 'Please select an Operator');
        //     return redirect()->action('OperatorsController@index');
        // }

        return redirect('bookings/status/ALL');

        
        // $start_date = date('Y-m-d 00:00:00');
        // $end_date = date('Y-m-d 23:59:59');

        // if(isset($request->daterange)){


        //     $arr = explode(' ', $request->daterange);
        //     $start_date = $arr[0].' 00:00:00';
        //     $end_date = $arr[1].' 23:59:59';
        // }

        

        // if(Auth::user()->role->name == 'Operator'){
        //     $trip_ids = Trip::where('operator_id', session('operator_id'))->lists('id')->toArray();
        //     $bookings = $bookings->whereIn('trip_id', $trip_ids);

        // }else
        //     $bookings = BuscomBooking::with(['trip','trip.sourcepark', 'trip.destpark', 'parent', 'child']);


        // $bookings = $bookings->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->paginate(5000)->toArray();
        // $bookings = ($bookings['data']);

        // // dump($bookings);
        
        // $page_title = 'All Bookings';
        // return view('bookings.index', compact('bookings', 'page_title', 'start_date', 'end_date'));
    }


    public function status(Request $request, $status = 'ALL'){


        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){


            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }

        
        $bookings = BuscomBooking::with(['trip','trip.sourcepark', 'trip.destpark', 'parent', 'child']);

        if(Auth::user()->role->name == 'Operator'){
            $trip_ids = Trip::where('operator_id', session('operator_id'))->lists('id')->toArray();
            $bookings = $bookings->whereIn('trip_id', $trip_ids);

        }

        if(empty($status) || $status == 'ALL')
            $page_title    = 'All Bookings';
        else{
            $page_title = $status.' Bookings';
            $bookings = $bookings->where('status', $status);
        }

        $bookings = $bookings->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->paginate(5000)->toArray();
        $bookings = ($bookings['data']);

        // dump($bookings);
        
        return view('bookings.index', compact('bookings', 'page_title', 'start_date', 'end_date'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add booking';
        return view('bookings.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
        Booking::create($request->all());

        Session::flash('flash_message', 'Booking added!');

        return redirect('bookings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show( $id)
    {


        $booking = BuscomBooking::with(['trip.sourcepark','trip.destpark', 'trip.operator','trip.busType', 'paymentmthd', 'transactions', 'seats', 'passengers', 'child', 'parent' ])->findOrFail($id)->toArray();
        //dd($booking['trip']['departure_time']);
                $notes =  BookingNote::with(['user'])->get();
//                $to = $booking['contact_phone'];
//                $msg = "Bus.com.ng Confirmation notice: Book Code: " . $booking['booking_code'] . " \nfrom: " . $booking['trip']['name'] . "\nDetails are: " . $booking['contact_name'] . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking['date'])) . "\nDeparture Time: " . $booking['trip']['departure_time'] . "\nTotal Passengers: " . $booking['passenger_count'] . "\nSeats: "
//            . $booking['seats'][0]['seat_no'] . "\n Trip Cost: NGN " . $booking['final_cost']
//            . "\nTotal Trip Cost: NGN " . $booking['final_cost'];
//        $resp = self::send_sms($to,$msg);
//        dd($resp);

        $page_title = 'Booking Details';
        $booked_seats = '';
        foreach ($booking['seats'] as $seat) {
           $booked_seats .= $seat['seat_no'].',';
        }

        $booked_seats = substr($booked_seats, 0, strlen($booked_seats) - 1);

        $payment_methods = [null => ' -Choose- '] + PaymentMethod::where('active', 1)->lists('name', 'id')->toArray();



        return view('bookings.show', compact('booking', 'page_title', 'notes', 'booked_seats', 'payment_methods'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $booking = Booking::with(['trip'])->findOrFail($id);

        $page_title = 'Edit booking';
        return view('bookings.edit', compact('booking', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
       //  dd($request);
        $booking = Booking::findOrFail($id);
        //dd($booking);
        $booking->update($request->all());

        Session::flash('flash_message', 'Booking updated!');

        return redirect('bookings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Booking::destroy($id);

        Session::flash('flash_message', 'Booking deleted!');

        return redirect('bookings');
    }


    public function AdminBook(Request $request, $trip_id, $date){



        $page_title = 'Add booking';

        $trip = Trip::find($trip_id);
        $trip_name = $trip->name;
        $fare = $trip->fare;
        // dd($trip);
        if ($request->isMethod('post')) {
            // dd($request->request);
            $this->validate($request, [
                'passenger_count' => 'required',
                'contact_phone' => 'required',
                'contact_name' => 'required',
                'next_of_kin' => 'required',
                'next_of_kin_phone' => 'required',
            ]);

            $k = 1;
            for ($i=1; $i < 10 ; $i++) { 
                $p = 'psg'.$i;
                $pssg = $request->$p;
                // dd($pssg);
                if(!empty($pssg))
                    $k=$i+1;
                    //echo $i.''.$pssg.'<br>';
            }
            // exit;
               if($request->passenger_count != $k)
                    return redirect()->back()->with('error', 'Passenger Count and Names of passengers inputed do not match');

                if(empty($request->paid_date)){
                    $status  = 'PENDING';
                    $paid_date = NULL;
                }else{
                    $status = 'PAID';
                    $paid_date = $request->paid_date;
                }

            $booking_code = $trip->operator->code.rand(100000,999999);
               
            $booking = Booking::firstOrCreate([
                    'trip_id' => $trip_id,
                    'date' => date('Y-m-d', strtotime($date)),
                    'passenger_count' => $request->passenger_count,
                    'contact_name' => $request->contact_name,
                    'contact_phone' => $request->contact_phone,
                    'next_of_kin' => $request->next_of_kin,
                    'next_of_kin_phone' => $request->next_of_kin_phone,
                    'status' => $status,
                    'paid_date' => $paid_date,
                    'final_cost' => $request->passenger_count*$fare,
                    'unit_cost' => $fare,
                    'booking_code' => $booking_code,
                ]);

            for ($i=1; $i < $k ; $i++) { 
                $p = 'psg'.$i;
                $psg_name = $request->$p;
                // echo $booking->id.'<br>';
                $response =  Passenger::firstOrCreate([
                    'booking_id'=>$booking->id,
                    'name'=>$psg_name,
                ]);
            }
            Session::flash('flash_message', 'Booking has been saved!');

            return redirect('bookings/status/PENDING');

        }


        return view('bookings.create', compact('page_title', 'trip_name', 'trip', 'fare', 'date'));
    }

    public function AgentBooking(Request $request, $user_id){
        $user = User::find($user_id);
        $page_title = $user->first_name.' Bookings';

        $bookings = Booking::with(['trip'])->where('user_id', $user_id)->get();
        return view('bookings.agentIndex', compact('bookings', 'page_title'));
    }
   

   public function UpdateStatus(Request $request){


        if ($request->isMethod('post')) {
          // dd($request->seat);
           $id = $request->booking_id;
            $booking = Booking::findOrFail($id);
            //dd($booking);
            $to = $booking['contact_phone'];

            $msg = "Bus.com.ng Confirmation Notice: "  . " \nFrom: " . $booking->trip->name . "\nContact Name: "
                . $booking['contact_name'] . "\nDeparture Date: " . date("D, d/m/Y", strtotime($booking['date']))  ."\nBooked Seat:". $request->seat. "\nTotal Passengers: " . $booking['passenger_count']  . "\nFinal Cost: NGN "
                . number_format($booking['final_cost']);

            $res =Booking::where('id', $request->booking_id)
                    ->update(['status' => $request->status]);

            if($res)
                $resp = self::send_sms($to,$msg);
                return redirect()->back()->withInput($request->all);
        }

   }

   public function Note(Request $request){
       
        if ($request->isMethod('post')) {

            $user = Auth::user();
            $store = DB::table('booking_notes')->insert([
                ['booking_id' => $request->booking_id, 'notes' => $request->notes, 'user_id'=> $user->id, 'created_at'=>date('Y-m-d H:i:s')],
            ]);

            if($store){

                $notes =  BookingNote::with(['user'])->get();

                 $view = view('bookings.notes', compact('notes'));
                 return $view;
            }
        }    
   }


   public function finance_summary(Request $request){

      $start_date = date('Y-m-d 00:00:00');
      $end_date = date('Y-m-d 23:59:59');

        if(isset($request->daterange)){
            $arr = explode(' ', $request->daterange);
            $start_date = $arr[0].' 00:00:00';
            $end_date = $arr[1].' 23:59:59';
        }


        $bookings = BuscomBooking::with(['trip','trip.sourcepark', 'trip.destpark', 'parent', 'child']);

        if(Auth::user()->role->name == 'Operator'){
            $trip_ids = Trip::where('operator_id', session('operator_id'))->lists('id')->toArray();
            $bookings = $bookings->whereIn('trip_id', $trip_ids);

        }

        if(empty($status) || $status == 'ALL')
            $page_title    = 'All Bookings';
        else{
            $page_title = $status.' Bookings';
            $bookings = $bookings->where('status', $status);
        }

        $bookings = $bookings->whereBetween('created_at', array($start_date, $end_date))->orderBy('created_at', 'desc')->paginate(5000)->toArray();
        $bookings = ($bookings['data']);



        $trip_ids = Trip::where('operator_id', session('operator_id'))->lists('id')->toArray();

        $paidTotal = $bookings = BuscomBooking::whereIn('trip_id', $trip_ids)
                            ->where('status', 'PAID')
                            ->sum('final_cost');
        $paidTotalCount = $bookings = BuscomBooking::whereIn('trip_id', $trip_ids)
                            ->where('status', 'PAID')
                            ->count();                            

        $paidSum = $bookings = BuscomBooking::whereIn('trip_id', $trip_ids)
                            ->where('created_at', '>=', $start_date)
                            ->where('created_at', '<=', $end_date)                           
                            ->where('status', 'PAID')
                            ->sum('final_cost');                            

        

        $paidCount = $bookings = BuscomBooking::whereIn('trip_id', $trip_ids)
                            ->where('created_at', '>=', $start_date)
                            ->where('created_at', '<=', $end_date)                           
                            ->where('status', 'PAID')
                            ->count();

        $psgCount = $bookings = BuscomBooking::whereIn('trip_id', $trip_ids)
                            ->where('created_at', '>=', $start_date)
                            ->where('created_at', '<=', $end_date)                           
                            ->where('status', 'PAID')
                            ->sum('passenger_count');
        $paidTotal = $paidTotal - ($paidTotalCount * 150);
        $paidSum = $paidSum - ($paidCount * 150);                            
        // dump($bookings);
        
        return view('bookings.fin_summary', compact('bookings', 'page_title', 'start_date', 'end_date', 'psgCount', 'paidSum', 'paidCount', 'paidTotal'));
        // return view('bookings.fin_summary');

   }

    public function cancelRedundantBookings(){
        $today = date('Y-m-d H:i:s');
        try {
            $bookings = BuscomBooking::where('status', 'PENDING');
            if(!empty($bookings)){
                dd($bookings); exit;
                foreach($bookings as $booking){
                    $booking_date = $booking->created;
                    $date_diff = date_diff($booking_date, $today);
                    if($date_diff > 24){
                        $this->cancel($booking->id);
                    }
                }
            }else{
                dd($today);
            }
        } catch (Exception $e) {
           return response()->view('errors/404', [], 404)->with($e);
        }
    }

    public function cancel(Request $request){
       // dd($request->all());
        $id  = $request->booking_id;
        $booking = BuscomBooking::find($id);
        $booking->status = 'CANCELLED';
        $booking->save();
        return redirect()->back()->withInput($request->all);

    }
	
	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function feedbacks()
    {
        $feedbacks = CustomerFeedback::paginate(1000);
        $page_title = 'Feedbacks';
        return view('feedbacks.index', compact('feedbacks', 'page_title'));
    }
	
	 public function changeFeedBackStatus($id = null, $type = null)
    {
        $feedback = CustomerFeedback::find($id);
        if($type == 1){
            $feedback->active = 1;
        }else{
            $feedback->active = 0;
        }
        $feedback->save();
        return redirect()->back();
    }

    function send_sms($to, $msg)
    {

        $cid = "";
        $user = "info@bus.com.ng";
        // $senderArray = 'Buscomng';
        $senderId = env('SMS_USERNAME');
        $to = $to;

        $pass = env('SMS_PASS');
        $ch = curl_init();
        // dd($msg);
        $postdata = 'username=' . $user . '&password=' . $pass . '&message=' . $msg . '&sender=' . $senderId . '&mobiles=' . $to; //initialize the request variable
        // echo $postdata;
        $nmsg = urlencode($msg);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://portal.nigerianbulksms.com/api/?username=$user&password=$pass&message='$nmsg'&sender=$senderId&mobiles=$to",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",

            ),
        ));

        $response = curl_exec($curl);
        //dd($response);
        $err = curl_error($curl);

        curl_close($curl);
        //dd($response);
        return $response;

    }


}
