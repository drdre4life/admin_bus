<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CharteredBooking;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class CharteredBookingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
        if(Auth::user()->role->name == 'Operator'){
            $charteredbooking = CharteredBooking::orderBy('created_at', 'desc')->where('operator_id', session('operator_id'))->paginate(100000);

        }else
            $charteredbooking = CharteredBooking::orderBy('created_at', 'desc')->paginate(100000);

        $page_title = 'Chartered booking';

        return view('chartered-booking.index', compact('charteredbooking', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add Chartered Booking';

        return view('chartered-booking.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
        CharteredBooking::create($request->all());

        Session::flash('flash_message', 'CharteredBooking now added!');

        return redirect('chartered-bookings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $charteredbooking = CharteredBooking::findOrFail($id);

        $page_title = 'View chartered booking';
        return view('chartered-booking.show', compact('charteredbooking', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $charteredbooking = CharteredBooking::findOrFail($id);

        $page_title = 'Edit chartered booking';
        return view('chartered-booking.edit', compact('charteredbooking', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $charteredbooking = CharteredBooking::findOrFail($id);
        $charteredbooking->update($request->all());

        Session::flash('flash_message', 'Chartered Booking updated!');

        return redirect('chartered-bookings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        CharteredBooking::destroy($id);

        Session::flash('flash_message', 'Chartered Booking deleted!');

        return redirect('chartered-bookings');
    }

}
