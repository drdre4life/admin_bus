<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Models\State;
use App\Models\Park;
use App\Models\Trip;

use App\Http\Requests;

class DataUploadController extends Controller
{
    //

    public function getUploadOkeyson(Request $request){

        

        Excel::filter('chunk')->load('CTS Inventory/CTS_Okeyson Inventory VALIDATED.xlsx')->chunk(250, function($results){

        	$operator_id = 42;	
        	$lag_parks = [];
		    
		    foreach($results as $sheet){
				
				// dump($sheet);
				switch ($sheet->getTitle()) {
	        		case 'Parks mapped with States':
	        			$state_id = 0;
	        			$state = '';
	        			foreach ($sheet->toArray() as $row) {
	        				
	        				if(!empty($row['state'])){
	        					$state = trim(str_ireplace("state", "", $row['state']));
	        					$db_state = State::where('name', $state)->first();

	        					if(empty($db_state))
	        						dd('state not found');		
	        					else
	        						$state_id = $db_state->id;
	        				}
	        				
	        				if(!empty($row['parks_terminalbranch'])){
	        					$parks = explode(",", $row['parks_terminalbranch']);
		        				foreach ($parks as $park) {
		        					$park = Park::firstOrCreate([
		        							'state_id' => $state_id,
		        							'name' => trim($park).' [in '.$state.'] ',
		        							'active' => 1,
		        							'boardable' => 1,
		        						]);	

		        					if($state_id == 1)
		        						$lag_parks[$park->id] = $park->name;
		        				}
	        				}
	        				
	        			}
	        			break;
	        		
	        		

	        		case '  Conditions':
	        			
	        			
	        			break;
	        		
	        		case 'Lagos parks':

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				// dump($row);

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {

	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						// dump($row['destination']);	
	        						$d = Park::where('name', 'LIKE' ,trim($row['destination']).' %')->first();
	        						$price = $row['price'];
	        						// dump($p->toArray());
	        						// dump($d->toArray());
	        						Trip::firstOrCreate([
	        								'name' => 'OKEYSON_'.$p->name.'_'.$d->name.'_'.$price.'_firstbus',
	        								'operator_id' => $operator_id,
	        								'source_park_id' => $p->id,
	        								'dest_park_id' => $d->id,
	        								'departure_time' => '6:00am',
	        								'trip_position'=>'First Bus',
	        								'fare' => $price,
	        								'bus_type_id' => 1,
	        								'ac' => true,
	        								'security' => true,
	        								'active' => 1,

	        							]);
	        						Trip::firstOrCreate([
	        								'name' => 'OKEYSON_'.$p->name.'_'.$d->name.'_'.$price.'_secondbus',
	        								'operator_id' => $operator_id,
	        								'source_park_id' => $p->id,
	        								'dest_park_id' => $d->id,
	        								'departure_time' => '7:00am',
	        								'trip_position'=>'Second Bus',
	        								'fare' => $price,
	        								'bus_type_id' => 1,
	        								'ac' => true,
	        								'security' => true,
	        								'active' => 1,

	        							]);

	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;

	        		default:

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				dump($row);

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {

	        					if(empty(trim($s_park)))
	        						continue;

	        					// dump
	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						dump($row['destination']);	

	        						// $ds = Park::where('state_id', 1)->get();
	        						// dump($ds);

	        						//lagos parks...

	        						foreach ($lag_parks as $p_id => $p_name) {
	        							$price = $row['price'];
	        							// dump($d->toArray());

		        						// dump($p->toArray());
		        						dump($d->toArray());

		        						Trip::firstOrCreate([
	        								'name' => 'OKEYSON_'.$p->name.'_'.$d->name.'_'.$price.'_firstbus',
	        								'operator_id' => $operator_id,
	        								'source_park_id' => $p->id,
	        								'dest_park_id' => $d->id,
	        								'departure_time' => '6:00am',
	        								'trip_position'=>'First Bus',
	        								'fare' => $price,
	        								'bus_type_id' => 1,
	        								'ac' => true,
	        								'security' => true,
	        								'active' => 1,

		        							]);

		        						Trip::firstOrCreate([
		        								'name' => 'OKEYSON_'.$p->name.'_'.$d->name.'_'.$price.'_secondbus',
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '7:00am',
		        								'trip_position'=>'Second Bus',
		        								'fare' => $price,
		        								'bus_type_id' => 1,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,

		        							]);
	        						}

	        						


	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;
	        	}

		        


		    }
		});



		dd("All Done.");


    	

    }

    public function getUploadAniekan(Request $request){

        

        Excel::filter('chunk')->load('CTS Inventory/CTS_ Aniekan Abasi Transport Company Inventory VALIDATED.xlsx')->chunk(250, function($results){

        	$operator_id = 71;	
        	$lag_parks = [];
		    
		    foreach($results as $sheet){
				
				// dump($sheet);
				switch ($sheet->getTitle()) {
	        		case 'Parks mapped with States':
	        			$state_id = 0;
	        			$state = '';
	        			foreach ($sheet->toArray() as $row) {
	        				
	        				if(!empty($row['state'])){
	        					$state = trim(str_ireplace("state", "", $row['state']));
	        					$db_state = State::where('name', $state)->first();

	        					if(empty($db_state))
	        						dd('state not found');		
	        					else
	        						$state_id = $db_state->id;
	        				}
	        				
	        				if(!empty($row['parks_terminalbranch'])){
	        					$parks = explode(",", $row['parks_terminalbranch']);
		        				foreach ($parks as $park) {

		        					$park = Park::firstOrCreate([
		        							'state_id' => $state_id,
		        							'name' => trim($park).' [in '.$state.'] ',
		        							'active' => 1,
		        							'boardable' => 1,


		        						]);	

		        					if($state_id == 1)
		        						$lag_parks[$park->id] = $park->name;


		        				}	
	        				}
	        				
	        			}

	        			// dd('parks')
	        			break;
	        		
	        		

	        		case '  Conditions':
	        			
	        			
	        			break;

	        		case 'Contacts':
	        			
	        			
	        			break;
	        		
	        		case 'Lagos parks':

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				dump($row);

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {

	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						dump($row['destination']);	
	        						$d = Park::where('name', 'LIKE' ,trim($row['destination']).' %')->first();

	        						$price = $row['price'];

	        						dump($p->toArray());
	        						dump($d->toArray());

	        						Trip::firstOrCreate([
	        								'name' => 'ANIEKAN'.$p->name.'_'.$d->name.'_'.$price.'_firstbus',
	        								'operator_id' => $operator_id,
	        								'source_park_id' => $p->id,
	        								'dest_park_id' => $d->id,
	        								'departure_time' => '6:00am',
	        								'trip_position'=>'First Bus',
	        								'fare' => $price,
	        								'bus_type_id' => 1,
	        								'ac' => true,
	        								'security' => true,
	        								'active' => 1,


	        							]);

	        						Trip::firstOrCreate([
	        								'name' => 'ANIEKAN'.$p->name.'_'.$d->name.'_'.$price.'_secondbus',
	        								'operator_id' => $operator_id,
	        								'source_park_id' => $p->id,
	        								'dest_park_id' => $d->id,
	        								'departure_time' => '7:00am',
	        								'trip_position'=>'Second Bus',
	        								'fare' => $price,
	        								'bus_type_id' => 1,
	        								'ac' => true,
	        								'security' => true,
	        								'active' => 1,


	        							]);


	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;

	        		default:

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				dump($row);

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {

	        					if(empty(trim($s_park)))
	        						continue;

	        					// dump
	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						dump($row['destination']);	

	        						// $ds = Park::where('state_id', 1)->get();
	        						// dump($ds);
	        						//lagos parks

	        						foreach ($lag_parks as $p_id => $p_name) {
	        							$price = $row['price'];
	        							// dump($d->toArray());

		        						// dump($p->toArray());
		        						dump($d->toArray());

		        						Trip::firstOrCreate([
	        								'name' => 'ANIEKAN'.$p->name.'_'.$d->name.'_'.$price.'_firstbus',
	        								'operator_id' => $operator_id,
	        								'source_park_id' => $p->id,
	        								'dest_park_id' => $d->id,
	        								'departure_time' => '6:00am',
	        								'trip_position'=>'First Bus',
	        								'fare' => $price,
	        								'bus_type_id' => 1,
	        								'ac' => true,
	        								'security' => true,
	        								'active' => 1,


	        							]);

		        						Trip::firstOrCreate([
		        								'name' => 'ANIEKAN'.$p->name.'_'.$d->name.'_'.$price.'_secondbus',
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '7:00am',
		        								'trip_position'=>'Second Bus',
		        								'fare' => $price,
		        								'bus_type_id' => 1,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,


		        							]);
	        						}

	        						


	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;
	        	}

		        


		    }
		});



		dd("All Done.");


    	

    }


    public function getUploadKanta(Request $request){

        

        Excel::filter('chunk')->load('CTS Inventory/CTS_ KANTA Cruise Inventory VALIDATED.xlsx')->chunk(250, function($results){

        	$operator_id = 66;	
        	$lag_parks = [];
		    
		    foreach($results as $sheet){
				
				// dump($sheet);
				switch ($sheet->getTitle()) {
	        		case 'Parks mapped with state':
	        			$state_id = 0;
	        			$state = '';
	        			foreach ($sheet->toArray() as $row) {
	        				
	        				if(!empty($row['states'])){
	        					$state = trim(str_ireplace("state", "", $row['states']));
	        					$db_state = State::where('name', $state)->first();

	        					if(empty($db_state))
	        						dd('state not found');		
	        					else
	        						$state_id = $db_state->id;
	        				}
	        				
	        				if(!empty($row['parks'])){
	        					$parks = explode(",", $row['parks']);
		        				foreach ($parks as $park) {
		        					// dump($park);

		        					$park = Park::firstOrCreate([
		        							'state_id' => $state_id,
		        							'name' => trim($park).' [in '.$state.'] ',
		        							'active' => 1,
		        							'boardable' => 1,


		        						]);	

		        					if($state_id == 1)
		        						$lag_parks[$park->id] = $park->name;


		        				}	
	        				}
	        				
	        			}

	        			// dd('parks');
	        			break;
	        		
	        		

	        		case 'Conditions':
	        			
	        			
	        			break;
	        			
	        		case 'Contacts':
	        			
	        			
	        			break;
	        		
	        		case 'Prices':

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				// dd($row);

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {

	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						dump($row['destination']);	

	        						if(strtolower(trim($row['destination'])) != 'lagos'){

	        							$d = Park::where('name', 'LIKE' ,trim($row['destination']).' %')->first();

		        						$price = $row['price'];

		        						dump($p->toArray());
		        						dump($d->toArray());

		        						Trip::firstOrCreate([
		        								'name' => 'KANTA_'.$p->name.'_'.$d->name.'_'.$price.'_firstbus',
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '6:00am',
		        								'trip_position'=>'First Bus',
		        								'fare' => $price,
		        								'bus_type_id' => 1,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,


		        							]);

		        						Trip::firstOrCreate([
		        								'name' => 'KANTA_'.$p->name.'_'.$d->name.'_'.$price.'_secondbus',
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '7:00am',
		        								'trip_position'=>'Second Bus',
		        								'fare' => $price,
		        								'bus_type_id' => 1,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,


		        							]);

	        						}else{

	        							// $ds = Park::where('state_id', 1)->get();
		        						// dump($ds);
		        						//lagos park


		        						foreach ($lag_parks as $p_id => $p_name) {
		        							$price = $row['price'];
		        							// dump($d->toArray());

			        						// dump($p->toArray());
			        						dump($d->toArray());

			        						Trip::firstOrCreate([
		        								'name' => 'KANTA_'.$p->name.'_'.$d->name.'_'.$price.'_firstbus',
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '6:00am',
		        								'trip_position'=>'First Bus',
		        								'fare' => $price,
		        								'bus_type_id' => 1,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,


		        							]);

			        						Trip::firstOrCreate([
			        								'name' => 'KANTA_'.$p->name.'_'.$d->name.'_'.$price.'_secondbus',
			        								'operator_id' => $operator_id,
			        								'source_park_id' => $p->id,
			        								'dest_park_id' => $d->id,
			        								'departure_time' => '7:00am',
			        								'trip_position'=>'Second Bus',
			        								'fare' => $price,
			        								'bus_type_id' => 1,
			        								'ac' => true,
			        								'security' => true,
			        								'active' => 1,


			        							]);


		        						}
	        						}
	        						


	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;

	        		default:

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				dump($row);

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {

	        					if(empty(trim($s_park)))
	        						continue;

	        					// dump
	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						dump($row['destination']);	

	        						$ds = Park::where('state_id', 1)->get();
	        						// dump($ds);

	        						foreach ($ds as $d) {
	        							$price = $row['price'];
	        							// dump($d->toArray());

		        						// dump($p->toArray());
		        						dump($d->toArray());

		        						Trip::firstOrCreate([
		        								'name' => 'KANTA_'.$p->name.'_'.$d->name.'_'.$price,
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '6:00am',
		        								'duration'=>'',
		        								'fare' => $price,
		        								'bus_type_id' => 1,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,


		        							]);
	        						}

	        						


	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;
	        	}

		        


		    }
		});




		



		dd("All Done.");


    	

    }


    public function getUploadCross(Request $request){

        

        Excel::filter('chunk')->load('CTS Inventory/CTS_Cross Country Inventory_edited.xlsx')->chunk(250, function($results){

        	$operator_id = 15;	
        	$state_parks = [];
        	
		    
		    foreach($results as $sheet){
				
				// dump($sheet);
				// continue;
				switch ($sheet->getTitle()) {
	        		case 'Parks mapped with States':
	        			$state_id = 0;
	        			$state = '';
	        			foreach ($sheet->toArray() as $row) {
	        				
	        				if(!empty($row['states'])){
	        					$state = trim(str_ireplace("state", "", $row['states']));
	        					$db_state = State::where('name', $state)->first();

	        					if(empty($db_state)){
	        						dump($state);	
	        						dd('state not found');		
	        					}
	        					else
	        						$state_id = $db_state->id;
	        				}
	        				
	        				if(!empty($row['parks'])){
	        					$parks = explode(",", $row['parks']);
		        				foreach ($parks as $park) {
		        					dump($park);

		        					$park = Park::firstOrCreate([
		        							'state_id' => $state_id,
		        							'name' => trim($park).' [in '.$state.'] ',
		        							'active' => 1,
		        							'boardable' => 1,


		        						]);	

		        					$state_parks[trim(strtolower($state))][] = $park->name;


		        				}	
	        				}
	        				
	        			}

	        			dump('parks');
	        			break;
	        		
	        		

	        		case 'Conditions':
	        			
	        			
	        			break;
	        			
	        		case 'Contacts':
	        			
	        			
	        			break;
	        		
	        		case 'Prices':

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				// dump($row);
	        				// continue;

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {

	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						dump($row['destination']);	

	        						$d = Park::where('name', 'LIKE' ,trim($row['destination']).' %')->first();

        							$ds = [];

        							if(empty($d)){ // not a park...

        								//check if it is a state
        								$state_ps = $state_parks[trim(strtolower($row['destination']))];
        								if(empty($state_ps)) //none found...
        									dd('stop: '.$row['destination']);
        								else{

        									dump($state_ps);

        									foreach ($state_ps as $p_name) {
        										$d = Park::where('name', 'LIKE' ,$p_name)->first();
        										$ds[] = $d;
        									}

        									

        								}	
        							}else{
        								$ds[] = $d;
        							}

        							// dd($ds);

	        						
	        						foreach ($ds as $key => $d) {

	        							$price = $row['price'];

		        						dump($p->toArray());
		        						dump($d->toArray());

		        						Trip::firstOrCreate([
		        								'name' => 'CROSS_'.$p->name.'_'.$d->name.'_'.$price.'_14-seater',
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '6:00am',
		        								'duration'=>'',
		        								'fare' => $price,
		        								'bus_type_id' => 1,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,


		        							]);


		        						Trip::firstOrCreate([
		        								'name' => 'CROSS_'.$p->name.'_'.$d->name.'_'.$price.'_15-seater',
		        								'operator_id' => $operator_id,
		        								'source_park_id' => $p->id,
		        								'dest_park_id' => $d->id,
		        								'departure_time' => '6:00am',
		        								'duration'=>'',
		        								'fare' => $price,
		        								'bus_type_id' => 2,
		        								'ac' => true,
		        								'security' => true,
		        								'active' => 1,


		        							]);

	        						}

	        						// if(strtolower(trim($row['destination'])) != 'lagos'){

	        						// 	$d = Park::where('name', 'LIKE' ,trim($row['destination']).' %')->first();

		        					// 	$price = $row['price'];

		        					// 	dump($p->toArray());
		        					// 	dump($d->toArray());

		        					// 	Trip::firstOrCreate([
		        					// 			'name' => 'CROSS_'.$p->name.'_'.$d->name.'_'.$price,
		        					// 			'operator_id' => $operator_id,
		        					// 			'source_park_id' => $p->id,
		        					// 			'dest_park_id' => $d->id,
		        					// 			'departure_time' => '6:00am',
		        					// 			'duration'=>'',
		        					// 			'fare' => $price,
		        					// 			'bus_type_id' => 1,
		        					// 			'ac' => true,
		        					// 			'security' => true,
		        					// 			'active' => 1,


		        					// 		]);

	        						// }else{

	        						// 	// $ds = Park::where('state_id', 1)->get();
		        					// 	// dump($ds);
		        					// 	//lagos park


		        					// 	foreach ($lag_parks as $p_id => $p_name) {
		        					// 		$price = $row['price'];
		        					// 		// dump($d->toArray());

			        				// 		// dump($p->toArray());
			        				// 		dump($d->toArray());

			        				// 		Trip::firstOrCreate([
			        				// 				'name' => 'KANTA_'.$p->name.'_'.$p_name.'_'.$price,
			        				// 				'operator_id' => $operator_id,
			        				// 				'source_park_id' => $p->id,
			        				// 				'dest_park_id' => $p_id,
			        				// 				'departure_time' => '6:00am',
			        				// 				'duration'=>'',
			        				// 				'fare' => $price,
			        				// 				'bus_type_id' => 1,
			        				// 				'ac' => true,
			        				// 				'security' => true,
			        				// 				'active' => 1,


			        				// 			]);
		        					// 	}
	        						// }
	        						


	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;

	        		default:

	        			dd('TITLE IS --> '.$sheet->getTitle());

	        			# code...
	        			break;
	        	}

		        


		    }
		});




		



		dd("All Done.");


    	

    }




    public function getUploadEkeson(Request $request){

        

        Excel::filter('chunk')->load('CTS Inventory/CTS_E.Ekeson Inventory_edited.xlsx')->chunk(250, function($results){

        	$operator_id = 19;	
        	$lag_parks = [];
		    
		    foreach($results as $sheet){
				
				// dump($sheet);
				// continue;
				switch ($sheet->getTitle()) {
	        		case 'Parks mapped with state':
	        			$state_id = 0;
	        			$state = '';
	        			foreach ($sheet->toArray() as $row) {
	        				
	        				if(!empty($row['states'])){
	        					$state = trim(str_ireplace("state", "", $row['states']));
	        					$db_state = State::where('name', $state)->first();

	        					if(empty($db_state)){
	        						dump($state);	
	        						dd('state not found');		
	        					}
	        					else
	        						$state_id = $db_state->id;
	        				}
	        				
	        				if(!empty($row['parks'])){
	        					$parks = explode(",", $row['parks']);
		        				foreach ($parks as $park) {
		        					dump($park);

		        					$park = Park::firstOrCreate([
		        							'state_id' => $state_id,
		        							'name' => trim($park).' [in '.$state.'] ',
		        							'active' => 1,
		        							'boardable' => 1,


		        						]);	

		        					if($state_id == 1)
		        						$lag_parks[$park->id] = $park->name;


		        				}	
	        				}
	        				
	        			}

	        			// dd('parks');
	        			break;
	        		
	        		

	        		case 'Conditions':
	        			
	        			
	        			break;
	        			
	        		case 'Contacts':
	        			
	        			
	        			break;
	        		
	        		case 'Prices':

	        			dump('TITLE IS --> '.$sheet->getTitle());

	        			$park = '';
	        			foreach ($sheet->toArray() as $row) {
	        				dump($row);
	        				// continue;

	        				if(!empty($row['park']))
	        					$park = $row['park'];

	        				$s_parks = explode(",", $park);
	        				foreach ($s_parks as $s_park) {



	        					$p = Park::where('name', 'LIKE' ,trim($s_park).' %')->first();

	        					if(!empty($row['destination'])){
	        						dump($row['destination']);	

	        						// if(strtolower(trim($row['destination'])) == 'lagos'){




	        							

		        						

	        						// }else{

	        							dump('I got here');

	        							$d = Park::where('name', 'LIKE' ,trim($row['destination']).' %')->first();

	        							$ds = [];

	        							if(empty($d)){ // not a park...

	        								//check if it is a state
	        								$state = State::where('name', 'LIKE' ,trim($row['destination']).'%')->first();
	        								if(empty($state)) //none found...
	        									dd('stop: '.$row['destination']);
	        								else{

	        									$ds = Park::where('state_id', $state->id)->get();

	        								}	
	        							}else{
	        								$ds[] = $d;
	        							}

	        							// dd($ds);

		        						$morning = $row['morning'];
		        						$night = $row['night'];
		        						$mcv = $row['mcv'];

		        						


		        						foreach ($ds as $key => $d) {

		        							dump($p->toArray());
		        							// dump($ds->toArray());

		        							if(!empty($morning)){

			        						   Trip::firstOrCreate([
			        								'name' => 'EKESON_'.$p->name.'_'.$d->name.'_'.$morning.'_morning_firstbus',
			        								'operator_id' => $operator_id,
			        								'source_park_id' => $p->id,
			        								'dest_park_id' => $d->id,
			        								'departure_time' => '6:00',
			        								'trip_position'=>'First Bus',
			        								'fare' => $morning,
			        								'bus_type_id' => 1,
			        								'ac' => true,
			        								'security' => true,
			        								'active' => 1,


			        							]);		

			        						   Trip::firstOrCreate([
			        								'name' => 'EKESON_'.$p->name.'_'.$d->name.'_'.$morning.'_morning_secondbus',
			        								'operator_id' => $operator_id,
			        								'source_park_id' => $p->id,
			        								'dest_park_id' => $d->id,
			        								'departure_time' => '7:00',
			        								'trip_position'=>'Second Bus',
			        								'fare' => $morning,
			        								'bus_type_id' => 4,
			        								'ac' => true,
			        								'security' => true,
			        								'active' => 1,


			        							]);		

			        						}

			        						if(!empty($night)){

			        							Trip::firstOrCreate([
			        								'name' => 'EKESON_'.$p->name.'_'.$d->name.'_'.$night.'_night_firstbus',
			        								'operator_id' => $operator_id,
			        								'source_park_id' => $p->id,
			        								'dest_park_id' => $d->id,
			        								'departure_time' => '18:00',
			        								'trip_position'=>'First Bus',
			        								'fare' => $night,
			        								'bus_type_id' => 1,
			        								'ac' => true,
			        								'security' => true,
			        								'active' => 1,


			        							]);		

			        						   Trip::firstOrCreate([
			        								'name' => 'EKESON_'.$p->name.'_'.$d->name.'_'.$night.'_night_secondbus',
			        								'operator_id' => $operator_id,
			        								'source_park_id' => $p->id,
			        								'dest_park_id' => $d->id,
			        								'departure_time' => '19:00',
			        								'trip_position'=>'Second Bus',
			        								'fare' => $night,
			        								'bus_type_id' => 4,
			        								'ac' => true,
			        								'security' => true,
			        								'active' => 1,


			        							]);	

			        						  

			        						}


			        						if(!empty($mcv)){


			        							Trip::firstOrCreate([
			        								'name' => 'EKESON_'.$p->name.'_'.$d->name.'_'.$mcv.'_night_firstbus',
			        								'operator_id' => $operator_id,
			        								'source_park_id' => $p->id,
			        								'dest_park_id' => $d->id,
			        								'departure_time' => '18:00',
			        								'trip_position'=>'First Bus',
			        								'fare' => $mcv,
			        								'bus_type_id' => 3,
			        								'ac' => true,
			        								'security' => true,
			        								'active' => 1,


			        							]);		

			        						  


			        						}
		        						}


		        						
	        						// }
	        						


	        					}


	        						
	        				}	



	        				
	        				
	        			}


	        			# code...
	        			break;

	        		default:

	        			dd('TITLE IS --> '.$sheet->getTitle());

	        			# code...
	        			break;
	        	}

		        


		    }
		});




		



		dd("All Done.");


    	

    }
}
