<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Libraries\Utility;
use Jenssegers\Agent\Agent;
use App\Models\Park;
use App\Models\Bank;
use App\Models\Booking;
use App\Models\CharteredBooking;
use App\Models\Passenger;
use App\Models\Transaction;
use App\Models\Trip;
use App\Models\Operator;
use App\Models\BusType;
use App\Models\CashOffice;
use App\Models\OnlineUser;
use App\Models\Customer;
use App\Models\CustomerFeedback;
use App\Models\Seat;
use App\Models\OperatorRule;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Models\BuscomBooking;



 /**
     * Log user in
     * @param  Request $request [the section of post to fetch, 1->original juice, 2->elsewhere, 0->all post]
     * @todo  None for now
     * @return json status,data
     */
class OnlineBookingApiController extends Controller
{
    public function postShow(){
        return 'Yes am here';
    }

    public function postSaveOnlineUser(Request $request){

         Utility::$response = Utility::checkParameters($request, array('name', 'email', 'phone'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);
        $o = OnlineUser::where('email', $request->email)->where('phone', $request->phone)->first();
        if(!empty($o)){ //user already exist...

            Utility::$response['status'] = true;
            Utility::$response['message'] = 'We have your details already. We will be in touch with you soon.';
            return response()->json(Utility::$response);

        }

        OnlineUser::firstOrCreate([
                                    'name'=>$request->name,
                                    'email'=>$request->email,
                                    'phone'=>$request->phone
                                    ]);

        $user = 'buscom_sms';
        $pass = 'bus.com.ng';
        $message = "Hi ".$request->name.". Thank you for contacting Bus.com.ng. We've received your details and we will be in touch with you soon.";
        $sender = "Buscomng";



        $from = rawurlencode($sender); //LOOK MAX 11 CHARs. This is the senderID that will appear on the recipients Phone.
        $msg = rawurlencode($message); // It is important that you use urlencode() here in orde to manage special characters.
        $to = preg_replace("/[^0-9,]/", "", $request->phone);


        // build HTTP URL and query
        $postdata = 'user='.$user.'&pass='.$pass.'&from='.$from.'&to='.$to.'&msg='.$msg; //initialize the request variable

        //echo $postdata;

        $url = 'http://cloud.nuobjects.com/api/send/'; //this is the url of the gateway's interface
        $ch = curl_init(); //initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); //set the url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //return as a variable
        curl_setopt($ch, CURLOPT_POST, 1); //set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata); //set the POST variables
        $response = curl_exec($ch); // grab URL and pass it to the browser. Run the whole process and return the response
        curl_close($ch); //close the curl handle

        // dump($response);


        Utility::$response['status'] = true;
        Utility::$response['message'] = 'We\'ve received your details and we will be in touch.';
        return response()->json(Utility::$response);


    }
	


    public function postSinglePark(Request $request){
         Utility::$response = Utility::checkParameters($request, array('park_id'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

          $response = Park::where('id', $request->park_id)->first(array('name'));
          if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

	public function postGetParks(Request $request){

        if(empty($request->boardable))
		  $response = Park::where('active', 1)->get(array('id', 'name'));
        else
          $response = Park::where('active', 1)->where('boardable', 1)->get(array('id', 'name'));

        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
	}


    public function postGetDests(Request $request){

        $dest_park_ids = Trip::where('active', true)->where('source_park_id', $request->source_park_id)->get(['dest_park_id']);
        // dd($dest_park_ids);

        $response = Park::where('active', 1)->whereIn('id', $dest_park_ids)->orderBy('name', 'asc')->get(array('id', 'name')); 

        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

    public function postGetIntDests(Request $request){

        $dest_park_ids = Trip::where('active', true)
            ->where('international', 1)
            ->where('source_park_id', $request->source_park_id)
            ->get(['dest_park_id']);
        $response = Park::where('active', 1)
            ->whereIn('id', $dest_park_ids)
            ->orderBy('name', 'asc')
            ->get(array('id', 'name'));
        if($response){
            Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

    public function postOperators(Request $request){

        if(isset($request->type))
            $response = Operator::where('active', 1)->get();
        else
            $response = Operator::where('active', 1)->get(array('id', 'name'));

        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

    public function postSearchTrip(Request $request){
        Utility::$response = Utility::checkParameters($request, array('toPark', 'fromPark'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

        $page = isset($request->page)? $request->page: 1;
        $per_page = isset($request->per_page)? $request->per_page: 10;
        $skip = intval($per_page * ($page - 1));
        $query = Trip::with('operator', 'busType', 'destpark', 'sourcepark')
                    ->where('source_park_id', $request->fromPark)
                    ->where('dest_park_id', $request->toPark)
                    ->where('active', 1);
		$current_time = date('H:i:s');
        $current_date = date('Y-m-d');
        if(isset($request->operator_id))
            $query = $query->where('operator_id', $request->operator_id);
		if($request->date == $current_date)
            $query = $query->where("departure_time", '>', $current_time);
        if(isset($request->filter_price)){
            $fpa = explode(":", $request->filter_price);
            $price_min = $fpa[0];
            $price_max = $fpa[1];
            $query = $query->where('fare', '>', $price_min);
            $query = $query->where('fare', '<', $price_max);
        }

        if(isset($request->bus_type) && !empty($request->bus_type))
            $query = $query->where('bus_type_id', $request->bus_type);

        if(isset($request->operator) && !empty($request->operator))
            $query = $query->where('operator_id', $request->operator);


        if($request->ac)
            $query = $query->where('ac', $request->ac);

        if($request->tv)
            $query = $query->where('tv', $request->tv);

        if($request->security)
            $query = $query->where('security', $request->security);

        if($request->passport)
            $query = $query->where('passport', $request->passport);

        if($request->insurance)
            $query = $query->where('insurance', $request->insurance);

        // if($request->ac || $request->tv || $request->security || $request->passport || $request->insurance){

        //     $query = $query->where('ac', $request->ac);
        //     $query = $query->where('tv', $request->tv);
        //     $query = $query->where('security', $request->security);
        //     $query = $query->where('passport', $request->passport);
        //     $query = $query->where('insurance', $request->insurance);

        // }

        // if(time() < strtotime("12pm"))
        //     $response = $response->where("departure_time", '>', date('hA'));
        $trip_count = $query->get()->count();
        $query = $query->searchPaginator($page, $per_page, $skip);

        if(isset($request->sort_by)){
            $s_arr = explode(":", $request->sort_by);
            $query = $query->orderBy($s_arr[0], $s_arr[1]);
        }
        else
            $query = $query->orderBy('departure_time', 'asc');

        $trips = $query->get();





        //getting other filter data...
        $operators = Operator::get(['id','name']);
        $busTypes = BusType::get(['id', 'name']);

        $fromPark = Park::find($request->fromPark);
        $toPark = Park::find($request->toPark);

        // dd($trips);

        if(empty($trips->count())){

            $state_id = $toPark->state_id;
            $park_ids =  Park::where('state_id', $state_id)->lists('id')->toArray();




            $query = Trip::with('operator', 'busType', 'destpark', 'sourcepark')
                    ->where('source_park_id', $request->fromPark)
                    ->whereIn('dest_park_id', $park_ids);

            if(isset($request->operator_id))
                $query = $query->where('operator_id', $request->operator_id);

            if(isset($request->filter_price)){
                $fpa = explode(":", $request->filter_price);
                $price_min = $fpa[0];
                $price_max = $fpa[1];
                $query = $query->where('fare', '>', $price_min);
                $query = $query->where('fare', '<', $price_max);
            }

            if(isset($request->bus_type) && !empty($request->bus_type))
                $query = $query->where('bus_type_id', $request->bus_type);

            if(isset($request->operator) && !empty($request->operator))
                $query = $query->where('operator_id', $request->operator);


            if($request->ac)
                $query = $query->where('ac', $request->ac);

            if($request->tv)
                $query = $query->where('tv', $request->tv);

            if($request->security)
                $query = $query->where('security', $request->security);

            if($request->passport)
                $query = $query->where('passport', $request->passport);

            if($request->insurance)
                $query = $query->where('insurance', $request->insurance);

            // if($request->ac || $request->tv || $request->security || $request->passport || $request->insurance){

            //     $query = $query->where('ac', $request->ac);
            //     $query = $query->where('tv', $request->tv);
            //     $query = $query->where('security', $request->security);
            //     $query = $query->where('passport', $request->passport);
            //     $query = $query->where('insurance', $request->insurance);

            // }

            // if(time() < strtotime("12pm"))
            //     $response = $response->where("departure_time", '>', date('hA'));
            $trip_count = $query->get()->count();
            $query = $query->searchPaginator($page, $per_page, $skip);

            if(isset($request->sort_by)){
                $s_arr = explode(":", $request->sort_by);
                $query = $query->orderBy($s_arr[0], $s_arr[1]);
            }
            else
                $query = $query->orderBy('departure_time', 'asc');

            $altTrips = $query->get();



        }

        $response = compact('trips', 'operators', 'busTypes', 'trip_count', 'fromPark', 'toPark', 'altTrips');
        if($trip_count !== 0){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

    public function postGetTrip(Request $request){
         Utility::$response = Utility::checkParameters($request, array('trip_id'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

        $departure_date = isset($request->departure_date)? $request->departure_date: "";

        $response = Trip::with(['operator', 'busType', 'destpark', 'sourcepark'=>function($q){
                                    $q->select('id', 'name', 'state_id');
        }, 'seats' => function($qq) use($departure_date, $request){
            $qq->where("departure_date", $departure_date)->where("trip_id", $request->trip_id);
        }])->where('id', $request->trip_id)->first();

        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }


   public function postGetTripFromBookingId(Request $request){
         Utility::$response = Utility::checkParameters($request, array('booking_code'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

        $response = BuscomBooking::with(['trip.operator', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'passengers','seats','parent','child.trip.sourcepark', 'child.trip.destpark', 'child.trip.busType'])->where('booking_code',$request->booking_code)->first();



       if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
   }

   public function postGetTripFromCharteredBookingId(Request $request){
         Utility::$response = Utility::checkParameters($request, array('charter_booking_id'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

       $response = CharteredBooking::find($request->charter_booking_id);

       if($response){
            Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
   }



   public function postSaveBooking(Request $request){
        Utility::$response = Utility::checkParameters($request, array('trip_id', 'passenger_count', 'final_cost', 'contact_phone'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);
        $trip = Trip::find($request->trip_id);
		$parent_trip = $trip->parent_trip_id;
        $booking_code = $trip->operator->code.rand(100000,999999);
           if($trip->international == 1){
               switch($request->passport_type){
                   case($request->passport_type == 'virgin_passport');
                       $passport_cost = $trip->virgin_passport;
                       $final_cost = $request->final_cost + $passport_cost  ;
                       break;
                   case($request->passport_type == 'regular_passport');
                       $passport_cost = $trip->regular_passport;
                       $final_cost = $request->final_cost + $passport_cost  ;
                       break;
                   case($request->passport_type == 'no_passport');
                       $passport_cost = $trip->no_passport;
                       $final_cost = $request->final_cost + $passport_cost  ;
                       break;
               }
           }else{
               $final_cost = $request->final_cost;
           }
		   $customer = BuscomBooking::where('contact_phone', $request->contact_phone)->first();
        if(!empty($customer)){
            BuscomBooking::where('id', $customer->id)
                ->update([
                    'contact_name' => $request->contact_name,
                    'contact_email' => $request->contact_email,
                    'contact_phone' => $request->contact_phone,
                    'contact_gender' => $request->gender
                ]);
        }
        $booking =  BuscomBooking::firstOrCreate([
            'trip_id'=>$request->trip_id,
            'date'=>$request->date,
            'passenger_count'=>$request->passenger_count,
            'departure_date'=>$request->departure_date,
            'unit_cost'=>$request->unit_cost,
            'final_cost'=>$final_cost,
            'paid_date'=>$request->paid_date,
            'contact_name'=>$request->contact_name,
            'contact_phone'=>$request->contact_phone,
            'contact_email'=>$request->contact_email,
            'contact_gender'=>$request->gender,
            'booking_code'=>$booking_code,
            'next_of_kin'=>$request->next_of_kin_name,
            'next_of_kin_phone'=>$request->next_of_kin_phone,
            'customer_id' => $request->customer_id,
            'passport_type' => $request->passport_type,
            'passport_number' => $request->passport_number,
            'passport_date_of_issue' => $request->passport_date_of_issue,
            'passport_place_of_issue' => $request->passport_place_of_issue,
            'passport_expiry_date' => $request->passport_expiry_date,
            'passport_cost' => $request->passport_cost,
            'departure_date' => $request->departure_date
        ]);
		 if(!empty($parent_trip)){
            $trip = $parent_trip;
        }else{
            $trip = $request->trip_id;
        }
        // saving first passenger...
        Passenger::firstOrCreate([
                'booking_id'=>$booking->id,
                'name'=>$request->contact_name,
                'seat'=>$request->seat,
                'gender'=>$request->gender,
                'trip_id' => $trip,
                'sub_trip_id' => $request->trip_id
                ]);


        if(!empty($request->return_trip_id)){
            
            $trip = Trip::find($request->return_trip_id);
            $booking_code = $trip->operator->code.rand(100000,999999);
            $booking =  BuscomBooking::firstOrCreate([
                'trip_id'=>$request->return_trip_id,
                'date'=>date('Y-m-d', strtotime($request->return_date)),
                'passenger_count'=>$request->passenger_count,
                'unit_cost'=>$trip->fare,
                'final_cost'=>$request->final_cost,
                'paid_date'=>$request->paid_date,
                'contact_name'=>$request->contact_name,
                'contact_phone'=>$request->contact_phone,
                'contact_email'=>$request->contact_email,
                'contact_gender'=>$request->gender,
                'booking_code'=>$booking_code,
                'next_of_kin'=>$request->next_of_kin_name,
                'next_of_kin_phone'=>$request->next_of_kin_phone,
                'customer_id' => $request->customer_id,
                'parent_booking_id' => $booking->id,
                'passport_cost' => $request->passport_cost,
                'departure_date' => $request->departure_date
            ]);
        }

        $booking = BuscomBooking::with(['trip','trip.sourcepark', 'trip.destpark', 'parent', 'child.trip.sourcepark', 'child.trip.destpark'])->find($booking->id);
        if($booking){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $booking;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }

    }

    public function postSaveCharter(Request $request){
      Utility::$response = Utility::checkParameters($request, array('operator', 'start'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);
        $response =  CharteredBooking::firstOrCreate([
            'pickup_location' => $request->pickup,
            'destination' => $request->dropoff,
            'contact_name' => $request->contact_name,
            'travel_date' => date('Y-m-d', strtotime($request->start)),
            'operator_id' => $request->operator,
            'contact_mobile' => $request->contact_phone,
            'next_of_kin_mobile' => $request->phone_kin,
            'next_of_kin' => $request->kin,
            'no_of_passengers' => $request->passenger,
            'budget' => $request->budget,
            'payment_method_id' => $request->payment_method_id,
            'pick_up_address' => $request->pick_up_address,
            'drop_off_address' => $request->drop_off_address
        ]);

         if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }

    }

     public function postSavePassengers(Request $request){
        Utility::$response = Utility::checkParameters($request, array('booking_id', 'name'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

        $response =  Passenger::firstOrCreate([
                'booking_id'=>$request->booking_id,
                'name'=>$request->name,
                'seat'=>$request->seat,
                'gender'=>$request->gender,
                'age' => isset($request->age)? $request->age: null,
                'age_group' => $request->age_group,
                'trip_id' => $request->trip_id,
                
                ]);

        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

    public function postSaveTransactions(Request $request){
        Utility::$response = Utility::checkParameters($request, array('status', 'response', 'booking_id'));
        if(!Utility::$response['status'])
            return response()->json(Utility::$response);

        // dd($request->request);

        $trans = new Transaction;
        $trans->booking_id = $request->booking_id;
        $trans->booking_code = $request->booking_code;
        $trans->status = $request->response;
        $trans->response = $request->status;
        $response = $trans->save();

        if(isset($request->payment_method) && trim($request->payment_method) == "PayStack"){
            BuscomBooking::where('id', $request->booking_id)
              ->update(['status' => 'PAID', 'paid_date'=>date('Y-m-d H:i:s'), 'payment_method_id'=>2]);
        }

        if(isset($request->cancelled)){
            BuscomBooking::where('id', $request->booking_id)
              ->update(['status' => 'CANCELLED']);
        }


        if($response){
             Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        }else{
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }


    public function getBanks(Request $request){

        $banks =  Bank::where('active',1)->get();
        $cos =  CashOffice::where('is_active',1)->get();



        Utility::$response['status'] = true;
        Utility::$response['data'] = ['banks'=>$banks, 'cash_offices'=>$cos];
        return response()->json(Utility::$response);

    }
	
	public function getFeedbacks(Request $request)
    {
        $feedbacks = CustomerFeedback::where('active', 1)->get();
        //$cos = CashOffice::where('is_active', 1)->get();
		if($feedbacks){
			$result = $feedbacks;
			$status = true;
		}else{
			$result = null;
			$status = false;
			}
        Utility::$response['status'] = $status;
        Utility::$response['data'] = ['feedbacks' => $result];
        return response()->json(Utility::$response);
    }


    public function postSaveCustomerFeedBack(Request $request)
    {
         Utility::$response = Utility::checkParameters($request, array('feedback', 'name', 'email', 'operator'));
        if (!Utility::$response['status'])
            return response()->json(Utility::$response);
        $response = CustomerFeedback::firstOrCreate([
            'name' => $request->name,
            'email' => $request->email,
            'feedback' => $request->feedback,
            'operator' => $request->operator,
            'active' => 0
        ]);
        if ($response) {
            Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        } else {
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

    public function postGetCustomerFeedBack(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, ['operator_id']);

        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $feedback = CustomerFeedback::where('operator_id', $request->input('operator_id'))->get();

        if($feedback && $request->input('rating') < 11){

            Utility::$response['status'] = 200;
            Utility::$response['data'] = $feedback;
            return response()->json(Utility::$response);
        }

        Utility::$response['status'] = false;
        return response()->json(Utility::$response);
    }

    public function postGetCustomerBookings(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, ['customer_id']);

        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $limit = isset($request->limit)? $request->limit: null;
        $page = isset($request->page)? $request->page: "";
        $per_page = isset($request->per_page)? $request->per_page: "";
        $skip = $per_page * ($page - 1);

        $customer_bookings['results'] = BuscomBooking::with('trip')
                                    ->whereCustomer($request->input('customer_id'))
                                    ->whereStatus($request->input('status'))
                                    ->whereLimit($limit)
                                    ->Paginatorx($page, $per_page, $skip)
                                    ->orderBy('id', 'desc')
                                    ->get();
        $customer_bookings['count'] = BuscomBooking::whereCustomer($request->input('customer_id'))
                                                ->whereStatus($request->input('status'))
                                                ->get()
                                                ->count();

        if($customer_bookings){
            Utility::$response['status'] = 200;
            Utility::$response['data'] = $customer_bookings;
            return response()->json(Utility::$response);
        }

        Utility::$response['status'] = false;
        return response()->json(Utility::$response);
    }

    public function postSaveCustomer(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, ['name', 'email', 'phone', 'password']);

        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $customer = Customer::where('email', $request->email)->orwhere('phone', $request->phone)->first();

        if(!$customer){
            $customer = Customer::create([
                                            'name' => $request->name,
                                            'email' => $request->email,
                                            'phone' => $request->phone,
                                            'password' => bcrypt($request->password)
                                        ]);
        }

        Utility::$response['data'] = $customer;
        Utility::$response['status'] = 200;
        return response()->json(Utility::$response);
    }


    public function postGetCustomerStats(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, ['customer_id']);

        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        Booking::where('customer_id', $request->customer_id);

        $paid_bookings = BuscomBooking::where('status', 'PAID')->whereCustomer($request->customer_id)->get()->count();

        $pending_bookings = BuscomBooking::where('status', 'PENDING')->whereCustomer($request->customer_id)->get()->count();
        $cancelled_bookings = BuscomBooking::where('status', 'CANCELLED')->whereCustomer($request->customer_id)->get()->count();

        $total_bookings = BuscomBooking::whereCustomer($request->customer_id)->get()->count();

        Utility::$response['data'] = ['pending' => $pending_bookings, 'paid' => $paid_bookings, 'total' => $total_bookings, 'cancelled' => $cancelled_bookings];
        Utility::$response['status'] = 200;
        return response()->json(Utility::$response);

    }

    public function postGetCustomerData(Request $request)
    {

        Utility::$response = Utility::checkParameters($request, ['phone']);

        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $customer = Booking::where('contact_phone', $request->phone)
                            ->first();

        Utility::$response['data'] = $customer;
        Utility::$response['status'] = 200;
        return response()->json(Utility::$response);
    }

    public function postGetOperatorBookingRule(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, ['operator_id']);

        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $rule = Operator::where('operator_id', $request->operator_id)
                            ->first();

        if(is_null($rule))
            $rule = Operator::where('operator_id', 0)
                                ->first();

        Utility::$response['data'] = $rule;
        Utility::$response['status'] = 200;
        return response()->json(Utility::$response);
    }

    public function postSaveBookedSeat(Request $request)
    {
         Utility::$response = Utility::checkParameters($request, ['trip_id', 'seat_no', 'departure_date']);
        $trip = Trip::find($request->trip_id);
        $parent_trip = $trip->parent_trip_id;
        if (Utility::$response['status'] == 404)
            return response()->json(Utility::$response);
        if(!empty($parent_trip)){
            $trip = $parent_trip;
        }else{
            $trip = $request->trip_id;
        }
        $seat = Seat::create([
            'trip_id' => $trip,
            'seat_no' => $request->seat_no,
            'departure_date' => $request->departure_date,
            'booking_id' => $request->booking_id
        ]);
        Utility::$response['data'] = $seat;
        Utility::$response['status'] = 200;
        return response()->json(Utility::$response);

    }
	
	public function postSaveFeedback(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, array('feedback', 'name', 'email', 'operator'));
        if (!Utility::$response['status'])
            return response()->json(Utility::$response);
        $response = CustomerFeedback::firstOrCreate([
            'name' => $request->name,
            'email' => $request->email,
            'feedback' => $request->feedback,
            'operator' => $request->operator,
            'active' => 0
        ]);
        if ($response) {
            Utility::$response['status'] = true;
            Utility::$response['data'] = $response;
            return response()->json(Utility::$response);
        } else {
            Utility::$response['status'] = false;
            return response()->json(Utility::$response);
        }
    }

    public function postGetCustomerBookingById(Request $request)
    {
        Utility::$response = Utility::checkParameters($request, ['booking_id']);


        if(Utility::$response['status'] == 404)
            return response()->json(Utility::$response);

        $booking = BuscomBooking::with(['trip.operator', 'trip.sourcepark', 'trip.destpark', 'trip.busType', 'passengers','seats'])
                            ->where('id', $request->booking_id)
                            ->first();

        if($booking){
            Utility::$response['status'] = 200;
            Utility::$response['data'] = $booking;
            return response()->json(Utility::$response);
        }

        Utility::$response['status'] = false;
        return response()->json(Utility::$response);
    }
}