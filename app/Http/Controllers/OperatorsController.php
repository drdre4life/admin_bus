<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Operator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class OperatorsController extends Controller
{   

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Select The Active Operator
     */
    public function Select(Request $request, $id){
        
        $request->session()->put('operator_id', $id);
        
        $name = Operator::find($id)->toArray();
        $request->session()->put('operator_name', $name['name']);
        $request->session()->put('operator_logo', $name['img']);
       
        Session::flash('flash_message', 'You have successfully selected <b>'.$name['name'].'</b> Operator');
        return redirect('bookings');

    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $status = 'active')
    {

        if(Auth::user()->role->name == 'Operator'){

            $request->session()->put('operator_id', Auth::user()->operator_id);
        
            $op = Operator::find(Auth::user()->operator_id);
            $request->session()->put('operator_name', $op->name);
            $request->session()->put('operator_logo', $op->img);

            return back()->withInput();


        }

        if($status != 'all'){
            $operators = Operator::active()->paginate(100000);
            $page_title = 'Active operators';
        }
        else{
            $operators = Operator::paginate(100000);
            $page_title = 'All operators';

        }
            
        

        return view('operators.index', compact('operators', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add operator';

        return view('operators.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
        Operator::create($request->all());

        Session::flash('flash_message', 'Operator added!');

        return redirect('operators');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $operator = Operator::findOrFail($id);

        $page_title = 'View operator';
        return view('operators.show', compact('operator', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $operator = Operator::findOrFail($id);

        $page_title = 'Edit operator';
        return view('operators.edit', compact('operator', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        
        $operator = Operator::findOrFail($id);
        $operator->update($request->all());

        Session::flash('flash_message', 'Operator updated!');

        return redirect('operators');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Operator::destroy($id);

        Session::flash('flash_message', 'Operator deleted!');

        return redirect('operators');
    }

}
