<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Park;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class ParksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $parks = Park::paginate(100000);

        $page_title = 'parks';

        return view('parks.index', compact('parks', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add park';

        return view('parks.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'state_id' => 'required', ]);

        Park::create($request->all());

        Session::flash('flash_message', 'Park added!');

        return redirect('parks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $park = Park::findOrFail($id);

        $page_title = 'View park';
        return view('parks.show', compact('park', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $park = Park::findOrFail($id);

        $page_title = 'Edit park';
        return view('parks.edit', compact('park', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'state_id' => 'required', ]);

        $park = Park::findOrFail($id);
        $park->update($request->all());

        Session::flash('flash_message', 'Park updated!');

        return redirect('parks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Park::destroy($id);

        Session::flash('flash_message', 'Park deleted!');

        return redirect('parks');
    }

}
