<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Operator;
use App\Models\State;
use App\Models\Park;
use App\Models\Trip;
use App\Models\Booking;
use Curl;
use Auth;

class ScheduleController extends Controller
{


    
    public function getSync(Request $request)
    {
       
       $ops = Operator::where('should_sync',true)->get();
       foreach ($ops as $op) {
           
            $this->handle($op);

       }
        
        
    }

    private function handle($op){

        $last_sync_time = '';
        $sync_time = date('Y-m-d H:i:s');

        //getting sync data...
        $url = $op->portal_link.'sync/status';
        $response = json_decode( Curl::to($url)
                        ->withData( array( 'access_code' => $op->access_code ) )
                        ->post() )->data;

        // dd($response);

        if($response->sync_status != 'IDLE')          
            return;


        if(!empty($response->last_sync_time))
            $last_sync_time = $response->last_sync_time;
        else
            $last_sync_time = '2010-01-01';

        echo 'Sycing from: '.$last_sync_time.'!<Br/>';

        $this->sync_states($last_sync_time, $op);
        $this->sync_parks($last_sync_time, $op);
        $this->sync_trips($last_sync_time, $op);
        $this->sync_bookings($last_sync_time, $op);


        //update sync time...
        $url = $op->portal_link.'sync/update-status';
        $response = json_decode( Curl::to($url)
                        ->withData( array( 'access_code' => $op->access_code, 'sync_time'=>$sync_time ) )
                        ->post() )->message;

    }

    private function sync_states($last_sync_time, $op){

        $states = State::where('updated_at', '>', $last_sync_time)->get()->toArray();

        if(empty($states)){
            echo 'State up-to-date!<Br/>';
            return;

        }

        $url = $op->portal_link.'sync/update-states';
        $response = json_decode( Curl::to($url)
                        ->withData( array( 'access_code' => $op->access_code, 'states'=>($states) ) )
                        ->post() );

        echo count($states).' state(s) updated!<Br/>';
        return;


    }


    private function sync_parks($last_sync_time, $op){

        $parks = Park::where('updated_at', '>', $last_sync_time)->get()->toArray();

        if(empty($parks)){
            echo 'Parks up-to-date!<Br/>';
            return;

        }

        $url = $op->portal_link.'sync/update-parks';
        $response = json_decode( Curl::to($url)
                        ->withData( array( 'access_code' => $op->access_code, 'parks'=>($parks) ) )
                        ->post() );

        echo count($parks).' parks(s) updated!<Br/>';
        return;


    }


    private function sync_trips($last_sync_time, $op){

        $url = $op->portal_link.'sync/fetch-trips';
        $response = json_decode( Curl::to($url)
                        ->withData( array( 'access_code' => $op->access_code, 'last_sync_time'=>$last_sync_time ) )
                        ->post(), true );

        $trips = $response['data'];

        if(empty($trips)){
            echo 'Trips up-to-date!<Br/>';
            return;

        }

        foreach ($trips as $trip) {
            // dd($trips);
            unset($trip['id']);
            Trip::updateOrCreate(array('operator_trip_id'=>$trip['operator_trip_id']), $trip);
        }

        echo count($trips).' trip(s) updated!<Br/>';
        return;


    }


    private function sync_bookings($last_sync_time, $op){

        $bookings = Booking::with('trip', 'passengers', 'paymentmthd')->where('updated_at', '>', $last_sync_time)->get()->toArray();

        if(empty($bookings)){
            echo 'Bookings up-to-date!<Br/>';
            return;

        }

        // dd($bookings);

        foreach ($bookings as $booking) {
            $url = $op->portal_link.'sync/update-bookings';
            $response = json_decode( Curl::to($url)
                            ->withData( array( 'access_code' => $op->access_code, 'booking'=>($booking), 'last_sync_time'=>$last_sync_time  ) )
                            ->post() );

            // dd($response);

            Booking::where('id', $booking['id'])->update(['operator_booking_id'=>$response->data->id]);
            
        }

        



        echo count($bookings).' bookings(s) updated!<Br/>';
        return;


    }

   
    
    

}
