<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Setting;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class SettingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $settings = Setting::paginate(15);

        $page_title = 'settings';

        return view('settings.index', compact('settings', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add setting';

        return view('settings.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['key' => 'required', 'value' => 'required', ]);

        Setting::create($request->all());

        Session::flash('flash_message', 'Setting added!');

        return redirect('settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $setting = Setting::findOrFail($id);

        $page_title = 'View setting';
        return view('settings.show', compact('setting', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);

        $page_title = 'Edit setting';
        return view('settings.edit', compact('setting', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['key' => 'required', 'value' => 'required', ]);

        $setting = Setting::findOrFail($id);
        $setting->update($request->all());

        Session::flash('flash_message', 'Setting updated!');

        return redirect('settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Setting::destroy($id);

        Session::flash('flash_message', 'Setting deleted!');

        return redirect('settings');
    }

}
