<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\State;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Auth;

class StatesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $states = State::paginate(100000);

        $page_title = 'states';

        return view('states.index', compact('states', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add state';

        return view('states.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        State::create($request->all());

        Session::flash('flash_message', 'State added!');

        return redirect('states');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $state = State::findOrFail($id);

        $page_title = 'View state';
        return view('states.show', compact('state', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $state = State::findOrFail($id);

        $page_title = 'Edit state';
        return view('states.edit', compact('state', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $state = State::findOrFail($id);
        $state->update($request->all());

        Session::flash('flash_message', 'State updated!');

        return redirect('states');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        State::destroy($id);

        Session::flash('flash_message', 'State deleted!');

        return redirect('states');
    }

}
