<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Archive;
use App\Models\Booking;
use App\Models\Trip;
use App\Models\Park;
use App\Models\Operator;
use App\Models\BusType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Excel;
use Auth;
use Illuminate\Support\Facades\Input;

class TripsController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $op_id = $request->session()->get('operator_id');
        if(empty($op_id)){
            $request->session()->flash('select', 'Please select an Operator');
            return redirect('operators');
        }

        $page_title = 'trips';

        $trips = Trip::with(['operator','sourcepark', 'destpark', 'bustype'])->where('operator_id', $op_id)->paginate(1000)->toArray();
        // dd($trips);
        return view('trips.index', compact('trips', 'page_title', 'opr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $page_title = 'Add trip';
        $op_id = $request->session()->get('operator_id');
        $boardable_parks = Park::where('active', 1)->where('boardable', 1)->get();
        $parent_trips = Trip::where('active', 1)->where('operator_id', $op_id)->get();
        $all_parks = Park::where('active', 1)->get();
        $bus_types = BusType::where('show', 1)->get();
        return view('trips.create', compact('page_title', 'boardable_parks', 'all_parks', 'bus_types','parent_trips'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
        public function store(Request $request)
    {

        // Trip::create($request->all());
        $this->validate($request, [
        'source_park' => 'required',
        'dest_park' => 'required',
        'duration' => 'required',
        'fare' => 'required',
        'departure_time' => 'required',
        'bus_type' => 'required',
        'international' => 'required',
        ]);
        //Generating name for Trip
        $s = Park::find($request->source_park);
        $d = Park::find($request->dest_park);
        $b = BusType::find($request->bus_type);
        if(!empty($request->parent_trip_id)){
            $p = Trip::find($request->parent_trip_id);
            $pti = $request->parent_trip_id;
            $ptn = $p->name;
            $ptc = $p->trip_code;
        }else{
            $pti = null;
            $ptn = null;
            $ptc = null;
        }
        $op_id = $request->session()->get('operator_id');
        $trip_name = $b->name.'_'.$s->name.'__'.$d->name.'->'.$request->fare;
        $trip_code = $this->generateTripCode();
        if(!empty($op_id)){
            // Trip::create($request->all());
            $details = array(
                'name' => $trip_name,
                'trip_code' => $trip_code,
                'source_park_id' => $request->source_park,
                'parent_trip_id' => $pti,
                'parent_trip_code' => $ptc,
                'parent_trip_name' => $ptn,
                'dest_park_id' => $request->dest_park,
                'duration' => $request->duration,
                'departure_time' => $request->departure_time,
                'fare' => $request->fare,
                'bus_type_id' => $request->bus_type,
                'operator_id' => $op_id,
                'ac' => $request->ac,
                'tv' => $request->tv,
                'insurance' => $request->insurance,
                'international' => $request->international,
                'virgin_passport' => $request->virgin_passport,
                'regular_passport' => $request->regular_passport,
                'no_passport' => $request->no_passport,
                'passport' => $request->passport,
                'security' => $request->security,);
            //dd($details); exit;
            Trip::firstOrCreate($details);
            Session::flash('flash_message', 'Trip added!');
            return redirect('trips');
        }else{
            Session::flash('flash_message', 'Operator is missing!');
            return redirect('create');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $trip = Trip::findOrFail($id);

        $page_title = 'View trip';
        return view('trips.show', compact('trip', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $trip = Trip::findOrFail($id);

        $boardable_parks = Park::where('active', 1)->where('boardable', 1)->get();
        $all_parks = Park::where('active', 1)->get();
        $bus_types = BusType::where('show', 1)->get();

        $page_title = 'Edit trip';
        return view('trips.edit', compact('trip', 'page_title', 'boardable_parks', 'all_parks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        $trip = Trip::findOrFail($id);
        $trip->update($request->all());

        Session::flash('flash_message', 'Trip updated!');

        return redirect('trips');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Trip::destroy($id);

        Session::flash('flash_message', 'Trip deleted!');

        return redirect('trips');
    }

    public function Search(Request $request){

        $page_title = 'Search trip';
        $op_id = $request->session()->get('operator_id');
        $boardable_parks = Park::where('active', 1)->where('boardable', 1)->get();
        $all_parks = Park::where('active', 1)->get();
        if(time() > strtotime("12:00pm"))
            $date = (date('d F, Y', strtotime('+1day')));
        else
            $date = (date('d F, Y'));

        if ($request->isMethod('post')) {
            // dd($request->request);
            $source_park = $request->source_park;
            $dest_park = $request->dest_park;

            $d_date = $request->departure_date;

            // dd($request->source_park);
            if(empty($request->operator_filter)){
                 $trips = Trip::with(['operator', 'sourcepark', 'destpark', 'bustype'])->where('source_park_id', $request->source_park)->where('dest_park_id', $request->dest_park)->paginate(10);
            }else{
                 $trips = Trip::with(['operator'])->where('source_park_id', $request->source_park)->where('dest_park_id', $request->dest_park)->where('operator_id', $request->operator_filter)->paginate(10);
             }
             // dd($trips->toArray());

            $operators = array();
            foreach ($trips as $t) {
                $operator_name = ($t->operator->name);
                $operators[] = ['id'=>$t->operator->id, 'name'=>$operator_name];
            }
            // dd($operators);

        }

        return view('trips.search', compact('trips', 'operators', 'date', 'd_date', 'page_title', 'boardable_parks', 'all_parks', 'source_park', 'dest_park'));
    }

    public function AdjustPrices(Request $request){

        if ($request->isMethod('post')) {

            $res =Trip::where('id', $request->id)
                    ->update(['fare' => $request->fare]);
            if($res)
                echo 'True';
            else
                echo 'False';
        }
    }

    public function uploadfile(Request $Request){
        echo 'Yesssss';
    }

    public function UplodaTrip(Request $request){

        $op_id = $request->operator_id;
        $operators = Operator::all();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        $op_id = $request->operator_id;
        $opertor_code['code']= Operator::where( 'id',$op_id)->first();
//        $opertor_code =  $opertor_code->code;

        if ($request->isMethod('post')) {

            ini_set('memory_limit', '-1');

            $op_id = $request->session()->get('operator_id');
            // dd($op_id);
            $operators = Operator::all();

            $opertor_code = Operator::where( 'id',$op_id)->first();
            //dd($opertor_code->name);
            $file_name = ($request->userfile->getClientOriginalName());
            $file_extension = $request->file('userfile')->getClientOriginalExtension();
            $hash = hash('ripemd160', $file_name);
            if ($file_extension == 'xlsx' || $file_extension == 'xls') {

                $excel_data = Excel::load($request->userfile, function ($reader) {
                })->get();
                foreach ($excel_data as $data) {

                    $source_park = $data['source_park'];
                    $dest_park = $data['destination_park'];
                    $s_prk = Park::where('name', 'like', '%' . $source_park . '%')->where('active', 1)->first();
                    $d_prk = Park::where('name', 'like', '%' . $dest_park . '%')->where('active', 1)->first();

                    if (empty($d_prk)) {
                        $flash[] = $dest_park . " does not match park name in previous record on line " . $data['id'] . " " . "Create park and try again";
                        return view('trips.bulk_upload', compact('flash', 'operators'));


                    }

                    if (empty($s_prk)) {

                        $flash[] = $source_park . " does not match park name in previous record on line " . $data['id'] . " " . "Create park and try again";
                        return view('trips.bulk_upload', compact('flash', 'operators'));

                    }

                }

                // $excel_data = Excel::load('html/uploads/' . $hash . '.' . $file_extension)->toArray();
                $total = count($excel_data);
                //dd($excel_data);
                $updated = 0;
                foreach ($excel_data as $data) {

                    $source_park = $data['source_park'];
                    $dest_park = $data['destination_park'];
                    $s_prk = Park::where('name', 'like', '%' . $source_park . '%')->where('active', 1)->first();
                    $d_prk = Park::where('name', 'like', '%' . $dest_park . '%')->where('active', 1)->first();
                    // dd($s_prk->name);

                    if( empty($d_prk)){
                        $flash[] = $dest_park . " does not match park name in previous record on line ".$data['id'] ." ". "Create park and try again";
                        return view('trips.bulk_upload', compact('flash', 'operators'));

                        continue;
                    }

                    if(empty($s_prk) ){

                        $flash[] =  $source_park . " does not match park name in previous record on line ".$data['id'] ." ". "Create park and try again";
                        return view('trips.bulk_upload', compact('flash', 'operators'));

                        continue;
                    }



                    if (!empty($s_prk) && !empty($d_prk)) {

                        $bus_type = BusType::where('name', $data['bus_type'])->first();

                        $trip_name = $bus_type->name . '_' . $s_prk->name . '_' . $d_prk->name . '_' . $data['fare'];

                        $trip_code = $this->generateTripCode();
                        $trip_code = $opertor_code['code'].'-'.$trip_code;

                                $trip = Trip::firstOrCreate([
                                    'name' => $trip_name,
                                    'source_park_id' => $s_prk->id,
                                    'dest_park_id' => $d_prk->id,
                                    'departure_time' => $data['departure_time'],
                                    'trip_code' => $trip_code,
                                    'fare' => $data['fare'],
                                    'bus_type_id' => $bus_type->id,
                                    'international'=>$data['international'],
                                    'virgin_passport'=>$data['virgin_passport'],
                                    'regular_passport'=>$data['regular_passport'],
                                    'operator_id' => $op_id,
                                    'active' => 1

                                ]);


                            if ($trip) {
                                // dd($trip);
                                $updated++;
                                //dd($updated);
                            }
                    }

                }

                $flash[] = $updated . " Trips uploaded successfully! out of". " ".$total;
            } else {
                $flash[] = "File format not supported! Please upload Excel only!";
            }
        }

        return view('trips.bulk_upload', compact('flash', 'operators'));

    }

	public function UploadTrips(Request $request){

        $op_id = $request->operator_id;
        $operators = Operator::all();

        if ($request->isMethod('post')) {

            ini_set('memory_limit', '-1');

            $op_id = $request->session()->get('operator_id');
            // dd($op_id);
            $operators = Operator::all();

            $opertor_code = Operator::where( 'id',$op_id)->first();
            //dd($opertor_code->name);
            $file_name = ($request->userfile->getClientOriginalName());
            $file_extension = $request->file('userfile')->getClientOriginalExtension();
            $hash = hash('ripemd160', $file_name);
            if ($file_extension == 'xlsx' || $file_extension == 'xls') {

                $excel_data = Excel::load($request->userfile, function ($reader) {
                })->get();

               // $excel_data = Excel::load('html/uploads/' . $hash . '.' . $file_extension)->toArray();
                $total = count($excel_data);
                //dd($excel_data);
                $updated = 1;
                foreach ($excel_data as $data) {
                 //  dd($data);
                    $source_park = $data['source_park'];
                    $dest_park = $data['destination_park'];
                    $s_prk = Park::where('name', 'like', '%' . $source_park . '%')->where('active', 1)->first();
                    $d_prk = Park::where('name', 'like', '%' . $dest_park . '%')->where('active', 1)->first();
                    // dd($d_prk);

                    if( empty($d_prk)){
                        $flash[] = $dest_park . " does not match park name in previous record on line ".$data['id'] ." ". "Correct and try again";
                        continue;
                        //return view('trips.bulk_upload', compact('flash', 'operators'));i
                    }

                    if(empty($s_prk) ){

                        $flash[] =  $source_park . " does not match park name in previous record on line ".$data['id'] ." ". "Correct and try again";
                        continue;
                        //return view('trips.bulk_upload', compact('flash', 'operators'));i
                    }
                    if (!empty($s_prk) && !empty($d_prk)) {
                        $trip = Trip::where('source_park_id', $s_prk->id)->where('dest_park_id', $d_prk->id)->first();

                        if (!empty($trip)) {

                            $trip_code = $this->generateTripCode();
                            // dd($booking);

                            $archive = new Archive();
                            $archive->trip_id = $trip->id;
                            $archive->price = $trip->fare;
                            $archive->is_intl_trip = $trip->is_intl_trip;
                            $archive->virgin_passport = $trip->virgin_passport;
                            $archive->regular_passport = $trip->regular_passport;
                            $archive->save();
                   }


                        $trip_code = !empty($trip) ? $trip->trip_code : null;

                        //dd($op_id);

                        $update_record = Trip::where('trip_code', $trip_code)
                            //  ->where('operator_id', $op_id)

                            ->update(
                                ['fare' => $data['fare']],
                                ['international' => $data['international']],
                                ['virgin_passport' => $data['virgin_passport']],
                                ['regular_passport' => $data['regular_passport']]
                            );

                        if ($update_record) {
                            $updated++;
                            //dd($updated);
                        }
                    }

                }

                $flash[] = $updated . " Trips updated successfully! out of". " ".$total;
            } else {
                $flash[] = "File format not supported! Please upload Excel only!";
            }
        }

        return view('trips.bulk_upload', compact('flash', 'operators'));
    }

    public function DownloadTrips(Request $request){
        $op_id = $request->session()->get('operator_id');
        if(empty($op_id)){
            $request->session()->flash('select', 'Please select an Operator');
            return redirect('operators');
        }
        //$page_title = 'trips';
        $trips = Trip::where('operator_id', $op_id)->get(['name', 'trip_code', 'departure_time',
            'duration', 'fare', 'new_fare', 'international', 'virgin_passport',
            'regular_passport','no_passport','updated_at' ])->toArray();
        //dd($trips);
        $file_name = 'Trips_'.date('Y-m-d');
        // Initialize the array which will be passed into the Excel
        // generator.
        $tripsArray = [];

        // Define the Excel spreadsheet headers
        $tripsArray[] = ['id', 'trip_code','name','departure','fare','new_fare','fare_expiry_date'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($trips as $trip) {
            $tripsArray[] = $trip;
        }
        Excel::create($file_name, function($excel) use($trips) {
            $excel->sheet('Trips Export', function($sheet) use($trips) {
                $sheet->freezeFirstRowAndColumn();
                //$sheet->protectCells('A1:A2000');
                $sheet->getProtection()->setPassword('bus.com.ng');
                $sheet->getProtection()->setSheet(true);
                $sheet->getStyle('F2:F1000')->getProtection()->setLocked
                    ('unprotected');
                $sheet->getStyle('G2:G1000')->getProtection()->setLocked
                    ('unprotected');
                $sheet->getStyle('H2:H1000')->getProtection()->setLocked
                    ('unprotected');
                $sheet->getStyle('I2:I1000')->getProtection()->setLocked
                    ('unprotected');
                $sheet->getStyle('J2:J1000')->getProtection()->setLocked
                    ('unprotected');
                $sheet->fromArray($trips);
            });
        })->export('xls');
        return back();
    }

    public function confirmTripsData(Request $request){
        if ($request->isMethod('post')) {
            if ($request->isMethod('post')) {
                $file_name  = ($request->userfile->getClientOriginalName());
                $file_extension =  $request->file('userfile')->getClientOriginalExtension();
                $hash = hash('ripemd160', $file_name);
                if($file_extension == 'xlsx' || $file_extension == 'xls'){
                    $upload_file = $request->file('userfile')->move(
                        'uploads', $hash.'.'.$file_extension
                    );
                    $excel_data = Excel::load('html/uploads/'.$hash.'.'.$file_extension)->toArray();
                    $updated = 1;
                    foreach($excel_data as $data){
                        // dd($data); exit;

                        $update_record = Trip::where('trip_code', $data['trip_code'])
                            ->update(
                                ['new_fare' => $data['new_fare']],
                                ['fare_expiry_date' => $data['fare_expires']]
                            );
                        if($update_record){
                            $updated++;
                        }
                    }
                    //unlink('html/uploads/'.$hash.'.'.$file_extension);
                    $flash[] = $updated." Trips updated successfully!";
                }else{
                    $flash[] = "File format not supported! Please upload Excel only!";
                    return view('trips.bulk_upload', compact('flash'));
                }
            }

            return view('trips.bulk_upload', compact('flash'));
        }
    }

    public function generateTripCodes(){
        $trips = Trip::where('active', 1)->get();
        $updated = 1;
        $total = count($trips);
        foreach($trips as $trip){
            $trip_code = $this->generateTripCode();
            Trip::where('id', $trip->id)
                ->update(['trip_code' => $trip_code]);
            $updated++;
        }
        $flash ="Updated successfully- ----- - Updated ".$updated." out of ".$total;
        return back()->with('status', $flash);
    }

    public function generateRandomNumber($length)
    {
        $result = '';
        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(1, 9);
        }
        return $result;
    }

    public function generateRandomAlphabets($length) {
        //$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function generateTripCode(){
        $random_no = $this->generateRandomNumber(15);
        //$random_alphabet = $this->generateRandomAlphabets(2);
        $trip_code = $random_no;
        return $trip_code;
    }





}
