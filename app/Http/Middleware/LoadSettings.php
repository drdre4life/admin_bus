<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Operator;

class LoadSettings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!$request->session()->has('operator')){
            //load operator
            $op = Operator::first();
            session(['operator' => $op]);
        }

        return $next($request);
            
    }
}
