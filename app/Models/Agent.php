<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agents';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'username', 'password', 'email', 'phone', 'last_login', 'active', 'address', 'notes', 'balance', 'percentage'];

    public function transactions(){
        return $this->hasMany('App\Models\AgentTransaction');
    }


    public function bookings(){
        return $this->hasMany('App\Models\Booking');
    }

}
