<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Archive extends Model
{
    protected $table = 'archive';

    protected $fillable = ['trip_id', 'price', 'is_intl_trip','virgin_passport', 'regular_passport'];

}
