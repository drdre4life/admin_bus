<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    protected $fillable = ['trip_id', 'date', 'passenger_count','passport_cost' ,'passport_number','passport_date_of_issue','passport_place_of_issue','passport_expiry_date','unit_cost', 'final_cost','status', 'paid_date','booking_code', 'contact_phone', 'contact_name', 'contact_email', 'next_of_kin', 'next_of_kin_phone','customer_id', 'parent_booking_id', 'contact_gender'];

    public function trip(){
        return $this->belongsTo('App\Models\Trip');
    }
    public function paymentmthd(){
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id');
    }

    public function transactions(){
        return $this->hasMany('App\Models\Transaction');
    }

    public function passengers(){
        return $this->hasMany('App\Models\Passenger');
    }

    public function seats()
    {
        return $this->hasMany("App\Models\Seat");
    }

    public function scopeWhereCustomer($query, $customer_id)
    {
        return $query->where('customer_id', $customer_id);
    }

    public function scopeWhereStatus($query, $status)
    {
        if($status)
            return $query->where('status', $status);
        return $query;

    }

    public function scopewhereLimit($query, $limit)
    {
        if($limit != null)
            return $query->limit($limit);
        return $query;
    }

    public function scopePaginatorx($query, $page, $per_page, $skip)
    {
        if($page > 0)
            return $query->skip($skip)->take($per_page);
        return $query;
    }
}
