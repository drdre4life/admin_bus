<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingNote extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'booking_notes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['booking_id', 'user_id', 'notes',];
    
     public function user(){
        return $this->belongsTo('App\Models\User');
    }
   
}
