<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashOffice extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cash_offices';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'contact_name', 'address', 'phone', 'is_active'];

}
