<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class CharteredBooking extends Model
{
    protected $fillable = ['trip_id', 'operator_id', 'passenger_count', 'payment_method_id', 'travel_date', 'pickup_location', 'destination', 'contact_name', 'contact_mobile', 'status', 'next_of_kin', 'next_of_kin_mobile', 'pick_up_address', 'drop_off_address', 'budget'];

    public function trip(){
        return $this->belongsTo('App\Models\Trip');
    }

    public function operator(){
        return $this->belongsTo('App\Models\Operator');
    }


}
