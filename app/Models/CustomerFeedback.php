<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerFeedback extends Model
{
	protected $table = "feedbacks";

    protected $fillable = ['name', 'email', 'operator', 'feedback', 'rating'];

    public function operator()
    {
    	return $this->belongsTo(Operator::class);
    }
}
