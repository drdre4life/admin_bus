<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'operators';

    protected $fillable = ['name', 'access_code', 'address', 'contact_person', 'phone', 'email', 'details', 'website', 'img', 'commission', 'portal_link', 'active', 'booking_rules', 'local_terms', 'intl_terms' ];


    public function trip(){
        
        return $this->hasMany('App\Models\Trip');
    }



    public function scopeActive($query)
    {
        
        return $query->where('active', 1);
    }

}

