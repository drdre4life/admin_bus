<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class OperatorRule extends Model
{
	protected $table = "operator_rules";
    protected $guarded = ['id'];
}
