<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Passenger extends Model
{
    protected $fillable = ['name', 'booking_id', 'gender', 'age', 'seat', 'age_group', 'trip_id', 'sub_trip_id'];

    public function booking()
    {
    	return $this->belongsTo('App\Models\Booking');
    }
}
