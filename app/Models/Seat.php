<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    protected $fillable = ['seat_no','booking_id','departure_date','trip_id','sub_trip_id'];

    public function trip()
    {
    	return $this->belongsTo('App\Models\Trip');
    }

    public function scopeTripSeats($query, $departure_date, $trip_id)
    {
    	if($departure_date != "" && $trip_id != "")
    		return $query->where('trip_id', $trip_id)->where('departure_date', $departure_date);
    	return $query;
    }

    public function booking()
    {
        return $this->belongsTo("App\Models\Bookings");
    }
}
