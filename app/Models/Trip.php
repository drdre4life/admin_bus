<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Trip extends Model
{

    protected $table = 'trips';

    protected $fillable = ['name', 'source_park_id', 'dest_park_id', 'parent_trip_id', 'operator_id',
        'parent_trip_code','parent_trip_name','trip_code',
        'departure_time', 'duration',
        'fare', 'bus_type_id', 'no_of_seats', 'ac', 'security', 'tv', 'insurance', 'international','virgin_passport',
        'regular_passport','no_passport','passport', 'active','trip_position'];


    public function operator(){
        return $this->belongsTo('App\Models\Operator');

    }

    public function busType(){
        return $this->belongsTo('App\Models\BusType');

    }

    public function sourcepark(){
        return $this->belongsTo('App\Models\Park', 'source_park_id');
    }

    public function destpark(){
        return $this->belongsTo('App\Models\Park', 'dest_park_id');
    }

    public function booking(){
        return $this->hasMany('App\Models\Booking');
    }

    public function seats()
    {
        return $this->hasMany('App\Models\Seat');
    }

    public function scopeSearchPaginator($query, $page, $per_page, $skip)
    {
        if($page > 0)
            return $query->skip($skip)->take($per_page);
        return $query;
    }
}