<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_logs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'log_type', 'details'];

}
