<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingRefundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_refunds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('booking_id')->unsigned()->index();
			$table->integer('user_id')->unsigned()->index();
			$table->float('booking_amount', 10, 0);
			$table->float('refunded_amount', 10, 0);
			$table->text('comment', 65535);
			$table->boolean('approved')->default(0);
			$table->integer('approved_user_id')->unsigned()->index();
			$table->dateTime('approved_date')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_refunds');
	}

}
