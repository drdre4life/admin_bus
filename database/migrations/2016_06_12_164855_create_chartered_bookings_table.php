<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharteredBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chartered_bookings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('operator_id')->unsigned()->index();
			$table->integer('trip_id')->unsigned()->nullable()->index();
			$table->dateTime('travel_date');
			$table->string('pickup_location')->nullable();
			$table->string('destination')->nullable();
			$table->integer('park_id')->unsigned()->nullable()->index();
			$table->float('agreed_price', 10, 0);
			$table->integer('bus_type_id')->unsigned()->nullable()->index();
			$table->text('bus_features', 65535);
			$table->string('contact_name');
			$table->string('contact_mobile');
			$table->string('contact_email');
			$table->string('next_of_kin');
			$table->string('next_of_kin_mobile');
			$table->integer('no_of_passengers');
			$table->string('status')->default('PENDING');
			$table->dateTime('paid_date');
			$table->text('comments', 65535);
			$table->integer('payment_method_id')->unsigned()->nullable()->index('chartered_bookings_payment_method_id_foreign');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chartered_bookings');
	}

}
