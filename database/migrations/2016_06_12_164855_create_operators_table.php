<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOperatorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('operators', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('access_code');
			$table->string('code', 5)->nullable();
			$table->text('address', 65535);
			$table->string('contact_person');
			$table->string('phone');
			$table->string('email');
			$table->text('details', 65535)->nullable();
			$table->string('website')->nullable()->default('');
			$table->float('commission');
			$table->string('img');
			$table->string('portal_link');
			$table->boolean('active')->default(1);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('operators');
	}

}
