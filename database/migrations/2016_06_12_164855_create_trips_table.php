<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTripsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trips', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable()->default('');
			$table->integer('operator_id')->unsigned()->index();
			$table->integer('source_park_id')->unsigned()->index();
			$table->integer('dest_park_id')->unsigned()->index();
			$table->time('departure_time');
			$table->string('duration');
			$table->float('fare');
			$table->integer('bus_type_id')->unsigned()->index();
			$table->integer('no_of_seats')->nullable();
			$table->integer('parent_trip_id')->unsigned()->nullable()->index();
			$table->boolean('ac')->default(0);
			$table->boolean('security')->default(0);
			$table->boolean('tv')->default(0);
			$table->boolean('insurance')->default(0);
			$table->boolean('passport')->default(0);
			$table->boolean('active')->default(1);
			$table->integer('operator_trip_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trips');
	}

}
