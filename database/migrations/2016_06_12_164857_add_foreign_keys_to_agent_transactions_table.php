<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAgentTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('agent_transactions', function(Blueprint $table)
		{
			$table->foreign('agent_id', 'agent_purchases_agent_id_foreign')->references('id')->on('agents')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('agent_transactions', function(Blueprint $table)
		{
			$table->dropForeign('agent_purchases_agent_id_foreign');
		});
	}

}
