<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCharteredBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('chartered_bookings', function(Blueprint $table)
		{
			$table->foreign('bus_type_id')->references('id')->on('bus_types')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('operator_id')->references('id')->on('operators')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('park_id')->references('id')->on('parks')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('payment_method_id')->references('id')->on('payment_methods')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('trip_id')->references('id')->on('trips')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('chartered_bookings', function(Blueprint $table)
		{
			$table->dropForeign('chartered_bookings_bus_type_id_foreign');
			$table->dropForeign('chartered_bookings_operator_id_foreign');
			$table->dropForeign('chartered_bookings_park_id_foreign');
			$table->dropForeign('chartered_bookings_payment_method_id_foreign');
			$table->dropForeign('chartered_bookings_trip_id_foreign');
		});
	}

}
