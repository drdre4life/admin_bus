<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTripsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trips', function(Blueprint $table)
		{
			$table->foreign('bus_type_id')->references('id')->on('bus_types')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('dest_park_id')->references('id')->on('parks')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('operator_id')->references('id')->on('operators')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('parent_trip_id')->references('id')->on('trips')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('source_park_id')->references('id')->on('parks')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trips', function(Blueprint $table)
		{
			$table->dropForeign('trips_bus_type_id_foreign');
			$table->dropForeign('trips_dest_park_id_foreign');
			$table->dropForeign('trips_operator_id_foreign');
			$table->dropForeign('trips_parent_trip_id_foreign');
			$table->dropForeign('trips_source_park_id_foreign');
		});
	}

}
