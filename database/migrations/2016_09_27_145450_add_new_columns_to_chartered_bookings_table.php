<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToCharteredBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chartered_bookings', function (Blueprint $table) {
            $table->string('pick_up_address')->after('payment_method_id');
            $table->string('drop_off_address')->after('pick_up_address');
            $table->string('budget')->after('pick_up_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chartered_bookings', function (Blueprint $table) {
            $table->dropColumn(['pick_up_address','drop_off_address']);
        });
    }
}
