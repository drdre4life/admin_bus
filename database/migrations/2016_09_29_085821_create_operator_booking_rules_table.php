<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorBookingRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operator_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('operator_id');
            $table->text('rules');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operator_rules', function (Blueprint $table) {
            Schema::drop('operator_rules');

        });
    }
}
