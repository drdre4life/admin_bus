<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('bus_types', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name', 255);
        //     $table->tinyInteger('show');
        //     $table->integer('no_of_seats');
        //     $table->integer('columns');
        //     $table->integer('rows');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bus_types');
    }
}
