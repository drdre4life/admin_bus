<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToBusTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('bus_types', function (Blueprint $table) {

            $table->dropColumn('no_of_seats');

        });
            
        Schema::table('bus_types', function (Blueprint $table) {
            
            

            $table->integer('no_of_seats')->default(15)->after('show');
            $table->integer('no_of_columns')->default(4)->after('no_of_seats');
            $table->integer('no_of_rows')->default(4)->after('no_of_columns');
            $table->integer('seat_width')->default(125)->after('no_of_rows');
            $table->integer('seat_height')->default(45)->after('seat_width');
            $table->integer('holder_width')->default(573)->after('seat_height');
            $table->integer('holder_height')->default(317)->after('holder_width');
            $table->string('no_seats')->default('-1, 0')->after('holder_height');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bus_types', function (Blueprint $table) {
            
            $table->dropColumn('no_of_columns');
            $table->dropColumn('no_of_rows');
            $table->dropColumn('seat_width');
            $table->dropColumn('seat_height');
            $table->dropColumn('holder_width');
            $table->dropColumn('holder_height');
            $table->dropColumn('no_seats');
        });
    }
}
