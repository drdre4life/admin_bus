<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeatsAndAgeGroupToPassengers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('passengers', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('age');

        });

        Schema::table('passengers', function (Blueprint $table) {
            $table->integer('seat')->nullable()->after('name');
            $table->string('gender')->after('seat');
            $table->integer('age')->nullable()->after('gender');
            $table->string('age_group')->default('Adult')->after('age');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('passengers', function (Blueprint $table) {
            
            $table->dropColumn("seat");
            $table->dropColumn("age_group");

        });
    }
}
