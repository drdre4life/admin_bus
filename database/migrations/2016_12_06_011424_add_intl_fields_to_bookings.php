<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntlFieldsToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            
            $table->boolean('is_intl_trip')->default(FALSE)->after('next_of_kin_phone');
            $table->string('passport_type')->nullable()->after('is_intl_trip');
            $table->string('passport_no')->nullable()->after('passport_type');
            $table->string('passport_expiry')->nullable()->after('passport_no');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {

            $table->dropColumn('passport_expiry');
            $table->dropColumn('passport_no');
            $table->dropColumn('passport_type');
            $table->dropColumn('is_intl_trip');

        });
    }
}
