<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInternationalColumnToTrips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->integer('international')->unsigned();;
            $table->double('regular_passport', 5, 2)->nullable();;
            $table->double('virgin_passport', 5, 2)->nullable();;
            $table->double('no_passport', 5, 5)->nullable();;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->dropColumn(['international', 'regular_passport', 'virgin_passport', 'no_passport']);
        });
    }
}
