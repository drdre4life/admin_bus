<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        //
    	$now = date('Y-m-d H:i:s');

        DB::table('states')->insert([
            'name' => 'Lagos',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);

        DB::table('states')->insert([
            'name' => 'Abia',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('states')->insert([
            'name' => 'Adamawa',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('states')->insert([
            'name' => 'Akwa-Ibom',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('states')->insert([
            'name' => 'Anambra',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('states')->insert([
            'name' => 'Bauchi',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


         DB::table('states')->insert([
            'name' => 'Bayelsa',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


          DB::table('states')->insert([
            'name' => 'Benue',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


           DB::table('states')->insert([
            'name' => 'Borno',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


            DB::table('states')->insert([
            'name' => 'Cross-River',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

             DB::table('states')->insert([
            'name' => 'Delta',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


              DB::table('states')->insert([
            'name' => 'Edo',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


               DB::table('states')->insert([
            'name' => 'Enugu',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                DB::table('states')->insert([
            'name' => 'Gombe',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                 DB::table('states')->insert([
            'name' => 'Imo',
            'region' => 'Ghana',
            'boardable' => 0,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                  DB::table('states')->insert([
            'name' => 'Jigawa',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                   DB::table('states')->insert([
            'name' => 'Kaduna',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                    DB::table('states')->insert([
            'name' => 'Kano',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                     DB::table('states')->insert([
            'name' => 'Katsina',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                      DB::table('states')->insert([
            'name' => 'Kebbi',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

                       DB::table('states')->insert([
            'name' => 'Kogi',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

                        DB::table('states')->insert([
            'name' => 'Kwara',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                         DB::table('states')->insert([
            'name' => 'Nasarawa',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                          DB::table('states')->insert([
            'name' => 'Niger',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                           DB::table('states')->insert([
            'name' => 'Ogun',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

                            DB::table('states')->insert([
            'name' => 'Osun',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                             DB::table('states')->insert([
            'name' => 'Oyo',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                              DB::table('states')->insert([
            'name' => 'Plateau',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                               DB::table('states')->insert([
            'name' => 'Rivers',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                                DB::table('states')->insert([
            'name' => 'Sokoto',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                                 DB::table('states')->insert([
            'name' => 'Taraba',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                                  DB::table('states')->insert([
            'name' => 'Yobe',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                                   DB::table('states')->insert([
            'name' => 'Zamfara',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                                    DB::table('states')->insert([
            'name' => 'Abuja',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                                     DB::table('states')->insert([
            'name' => 'Ebonyi',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
                                      DB::table('states')->insert([
            'name' => 'Ekiti',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);
           
           DB::table('states')->insert([
            'name' => 'Ondo',
            'region' => 'Nigeria',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


           DB::table('states')->insert([
            'name' => 'Cotonou',
            'region' => 'Republic of Benin',
            'boardable' => 1,
            'active' => 0,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

           DB::table('states')->insert([
            'name' => 'Accra',
            'region' => 'Ghana',
            'boardable' => 0,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

            DB::table('parks')->insert([
            'name' => 'Aba [in Abia]',
            'state_id' => 2,
            'address' => '30 / 32 Milverton Avenue, Abia State',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	  DB::table('parks')->insert([
            'name' => 'Maraba [in Abuja]',
            'state_id' => 34,
            'address' => 'Maraba Bus Park',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	  DB::table('parks')->insert([
            'name' => 'Agege - Oke Okoto [in Lagos]',
            'state_id' => 1,
            'address' => 'Agege Motor Park',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

    	   DB::table('parks')->insert([
            'name' => 'Cele [in Lagos]',
            'state_id' => 1,
            'address' => 'Lagos',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	    DB::table('parks')->insert([
            'name' => 'Calabar [in Cross-River]',
            'state_id' => 10,
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	     DB::table('parks')->insert([
            'name' => 'Ebonyi',
            'state_id' => 35,
            'address' => '',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	      DB::table('parks')->insert([
            'name' => 'Gwagwalada [in Abuja]',
            'state_id' => 34,
            'address' => 'Abuja',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	       DB::table('parks')->insert([
            'name' => 'Ilorin [in Kwara]',
            'state_id' => 22,
            'address' => 'ilorin Bug Motor Park',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	        DB::table('parks')->insert([
            'name' => 'Jalingo [in Taraba]',
            'state_id' => 31,
            'address' => 'Jags Motor Park',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	         DB::table('parks')->insert([
            'name' => 'Isofia [in Anambra]',
            'state_id' => 5,
            'address' => 'Isoks Motor Park',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

    	          DB::table('parks')->insert([
            'name' => 'Lafia [in Nasarawa]',
            'state_id' => 23,
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

    	           DB::table('parks')->insert([
            'name' => 'Minna [in Niger]',
            'state_id' => 24,
            'address' => 'Minna',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	            DB::table('parks')->insert([
            'name' => 5,
            'state_id' => 1,
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


    	             DB::table('parks')->insert([
            'name' => 'Ohafia [in Abia]',
            'state_id' => 2,
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

    	              DB::table('parks')->insert([
            'name' => 'Warri [in Delta]',
            'state_id' => 11,
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

    	DB::table('parks')->insert([
            'name' => 'Zaki-Biam [in Benue]',
            'state_id' => 8,
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('parks')->insert([
            'name' => 'Ibadan [in Oyo]',
            'state_id' => 27,
            'address' => 'Agege Motor Park',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('parks')->insert([
            'name' => 'Ramat Park [in Benin]',
            'state_id' => 12,
            'address' => 'Agege Motor Park',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('parks')->insert([
            'name' => 'Holy Ghost [in Enugu]',
            'state_id' => 13,
            'address' => 'Holy Ghost Opposite Main market, Enugu',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('parks')->insert([
            'name' => 'Ring Road [in Accra]',
            'state_id' => 38,
            'address' => 'Ring Road',
            'boardable' => 1,
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);

    }
}
