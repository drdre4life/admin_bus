<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class TripsTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
    	$now = date('Y-m-d H:i:s');
    	$faker = Faker::create();

    	/**
    	 * Operators
    	 */
    	DB::table('operators')->insert([
            'name' => 'GOD is GOOD',
            'access_code' => $faker->uuid,
            'address' => $faker->address,
            'contact_person' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'details' => $faker->text,
            
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


        DB::table('operators')->insert([
            'name' => 'EZEwanchukwu',
            'access_code' => $faker->uuid,
            'address' => $faker->address,
            'contact_person' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'details' => $faker->text,
            
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


         DB::table('operators')->insert([
            'name' => 'Young Shall Grow Transport Ltd',
            'access_code' => $faker->uuid,
            'address' => $faker->address,
            'contact_person' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'details' => $faker->text,
            
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


         DB::table('operators')->insert([
            'name' => 'Chisco',
            'access_code' => $faker->uuid,
            'address' => $faker->address,
            'contact_person' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'details' => $faker->text,
            
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


         DB::table('operators')->insert([
            'name' => 'ABC transport',
            'access_code' => $faker->uuid,
            'address' => $faker->address,
            'contact_person' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'details' => $faker->text,
            
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


          DB::table('operators')->insert([
            'name' => 'GUO Motors',
            'access_code' => $faker->uuid,
            'address' => $faker->address,
            'contact_person' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'details' => $faker->text,
            
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


           DB::table('operators')->insert([
            'name' => 'Cross Country',
            'access_code' => $faker->uuid,
            'address' => $faker->address,
            'contact_person' => $faker->name,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'details' => $faker->text,
            
            'active' => 1,
           
            'created_at' => $now,
                'updated_at' => $now,
        ]);


            DB::table('bus_types')->insert([
            'name' => 'Hiace',
            'show' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


             DB::table('bus_types')->insert([
            'name' => 'Luzury',
            'show' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


              DB::table('bus_types')->insert([
            'name' => 'G-Wagon',
            'show' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


               DB::table('bus_types')->insert([
            'name' => 'Toyota',
            'show' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


                DB::table('bus_types')->insert([
            'name' => 'Coaster',
            'show' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);





        /**
         * TRIPS
         */

         DB::table('trips')->insert([
            'name' => 'Hiace_Asaba[in Lagos]_Asaba [in Delta]_3000',
            'operator_id' => 1,
            'source_park_id' => 1,
            'bus_type_id' => 1,
            'dest_park_id' => 7,
            'departure_time' => '7am',
            'fare' => 5000,
            'no_of_seats' => 20,
            'ac' => 1,
            'tv' => 1,
            'insurance' => 1,
            'security' => 1,
            'passport' => 0,
            'active' => 1,
            'operator_trip_id' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);

         DB::table('trips')->insert([
            'name' => 'Venza_Oke Okoto [in Lagos]_Calabar [in Cross-River]_15000',
            'operator_id' => 1,
            'source_park_id' => 3,
            'bus_type_id' => 1,
            'dest_park_id' => 5,
            'departure_time' => '7am',
            'fare' => 9000,
            'no_of_seats' => 20,
            'ac' => 1,
            'tv' => 1,
            'insurance' => 1,
            'security' => 1,
            'passport' => 0,
            'active' => 1,
            'operator_trip_id' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


          DB::table('trips')->insert([
            'name' => 'Coaster_Oke Okoto [in Lagos]_Calabar [in Cross-River]_15000',
            'operator_id' => 2,
            'source_park_id' => 3,
            'bus_type_id' => 1,
            'dest_park_id' => 5,
            'departure_time' => '7am',
            'fare' => 9800,
            'no_of_seats' => 20,
            'ac' => 1,
            'tv' => 1,
            'insurance' => 1,
            'security' => 1,
            'passport' => 0,
            'active' => 1,
            'operator_trip_id' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


 DB::table('trips')->insert([
            'name' => 'Coaster_Oke Okoto [in Lagos]_Calabar [in Cross-River]_15000',
            'operator_id' => 3,
            'source_park_id' => 3,
            'bus_type_id' => 1,
            'dest_park_id' => 5,
            'departure_time' => '11am',
            'fare' => 5750,
            'no_of_seats' => 20,
            'ac' => 1,
            'tv' => 0,
            'insurance' => 0,
            'security' => 0,
            'passport' => 0,
            'active' => 1,
            'operator_trip_id' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


 DB::table('trips')->insert([
            'name' => 'Venza_Oke Okoto [in Lagos]_BCalabar [in Cross-River]_9000',
            'operator_id' => 1,
            'source_park_id' => 3,
            'bus_type_id' => 1,
            'dest_park_id' => 5,
            'departure_time' => '8pm',
            'fare' => 9000,
            'no_of_seats' => 20,
            'ac' => 1,
            'tv' => 1,
            'insurance' => 1,
            'security' => 1,
            'passport' => 0,
            'active' => 1,
            'operator_trip_id' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);

 DB::table('trips')->insert([
            'name' => 'Venza_Ibadan [in Oyo]_Ohafia [in Abia]_15000',
            'operator_id' => 4,
            'source_park_id' => 17,
            'bus_type_id' => 1,
            'dest_park_id' => 14,
            'departure_time' => '10am',
            'fare' => 15000,
            'no_of_seats' => 20,
            'ac' => 1,
            'tv' => 1,
            'insurance' => 1,
            'security' => 1,
            'passport' => 0,
            'active' => 1,
            'operator_trip_id' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);


 DB::table('trips')->insert([
            'name' => 'Lorry_Ibadan [in Oyo]_Ohafia [in Abia]_15000',
            'operator_id' => 4,
            'source_park_id' => 17,
            'bus_type_id' => 1,
            'dest_park_id' => 14,
            'departure_time' => '6am',
            'fare' => 3000,
            'no_of_seats' => 20,
            'ac' => 0,
            'tv' => 0,
            'insurance' => 0,
            'security' => 0,
            'passport' => 0,
            'active' => 1,
            'operator_trip_id' => 1,
            'created_at' => $now,
                'updated_at' => $now,
        ]);





    }
}
