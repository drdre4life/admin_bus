# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.26)
# Database: bustickets
# Generation Time: 2016-03-23 12:49:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table agents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `agents`;

CREATE TABLE `agents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(11) DEFAULT NULL,
  `acount_details` text,
  `percentage` int(11) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table banks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banks`;

CREATE TABLE `banks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `acount_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;

INSERT INTO `banks` (`id`, `name`, `acount_number`, `account_name`, `rank`, `active`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'GTBank','0127613144','spark.com.ng Ltd-Bus Booking',1,1,NULL,NULL,NULL),
	(2,'Zenith Bank','1013496374','Bus Booking Limited',1,1,NULL,NULL,NULL),
	(3,'UBA','1017438497','Bus Booking Ltd',1,1,NULL,NULL,NULL);

/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table booking_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `booking_notes`;

CREATE TABLE `booking_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_notes_booking_id_index` (`booking_id`),
  KEY `booking_notes_user_id_index` (`user_id`),
  CONSTRAINT `booking_notes_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `booking_notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `booking_notes` WRITE;
/*!40000 ALTER TABLE `booking_notes` DISABLE KEYS */;

INSERT INTO `booking_notes` (`id`, `booking_id`, `user_id`, `notes`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(3,1,6,'bksjabkjsnkanlka',NULL,NULL,NULL),
	(4,1,6,'bksjabkjsnkanlka',NULL,'2016-03-19 16:48:10',NULL),
	(5,1,6,'This is so lovely',NULL,'2016-03-19 16:50:44',NULL);

/*!40000 ALTER TABLE `booking_notes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table booking_refunds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `booking_refunds`;

CREATE TABLE `booking_refunds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `booking_amount` double NOT NULL,
  `refunded_amount` double NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `approved_user_id` int(10) unsigned NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_refunds_booking_id_index` (`booking_id`),
  KEY `booking_refunds_user_id_index` (`user_id`),
  KEY `booking_refunds_approved_user_id_index` (`approved_user_id`),
  CONSTRAINT `booking_refunds_approved_user_id_foreign` FOREIGN KEY (`approved_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `booking_refunds_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `booking_refunds_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table bookings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bookings`;

CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trip_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `passenger_count` int(11) NOT NULL,
  `unit_cost` double NOT NULL,
  `final_cost` double NOT NULL,
  `payment_method_id` int(10) unsigned DEFAULT NULL,
  `bank_id` int(10) unsigned DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `paid_date` datetime DEFAULT NULL,
  `booking_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next_of_kin` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `next_of_kin_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_booking_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bookings_trip_id_index` (`trip_id`),
  KEY `bookings_payment_method_id_index` (`payment_method_id`),
  KEY `bookings_bank_id_index` (`bank_id`),
  KEY `bookings_parent_booking_id_index` (`parent_booking_id`),
  KEY `bookings_user_id_index` (`user_id`),
  CONSTRAINT `bookings_parent_booking_id_foreign` FOREIGN KEY (`parent_booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bookings_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bookings_trip_id_foreign` FOREIGN KEY (`trip_id`) REFERENCES `trips` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bookings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;

INSERT INTO `bookings` (`id`, `trip_id`, `date`, `passenger_count`, `unit_cost`, `final_cost`, `payment_method_id`, `bank_id`, `status`, `paid_date`, `booking_code`, `contact_name`, `contact_phone`, `next_of_kin`, `next_of_kin_phone`, `parent_booking_id`, `user_id`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,10,'2016-03-24',1,4500,4500,2,NULL,'PENDING','2016-03-01 09:00:00','OGD-10ZEDC7G','Spec test','01374729','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-23 07:31:09'),
	(2,8,'2016-03-09',2,6700,13400,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Olatuji','0892621','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(21,11,'2016-03-07',1,3900,3900,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Victor','0901736362','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(22,18,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Bolaji','0813738829','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(23,9,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Tolulope','033678829','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(24,12,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Atinuke','+238 8746782','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(25,8,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Sophie','08022363617','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(26,10,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Abuodun','08011987372','Bode','09027348243',NULL,7,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(27,8,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Temitope','07467829','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(28,8,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','tosin','0701367892','Bode','09027348243',NULL,7,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(29,9,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Omolola','08031736789','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(30,10,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Emmanuel','0477483892','Bode','09027348243',NULL,7,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(31,11,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Kayode','0806374889','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(32,18,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Ifetuni','08063535962','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(33,11,'2016-03-08',1,4000,4000,2,NULL,'PAID','2016-03-01 09:00:00','OGD-10ZEDC7G','Akeem','08036637374','Bode','09027348243',NULL,NULL,NULL,'2016-03-10 00:00:00','2016-03-10 00:00:00'),
	(45,10,'2016-03-13',3,9000,27000,NULL,NULL,'PENDING',NULL,'','Akeem','0801128327','0107367','080',NULL,NULL,NULL,'2016-03-12 17:37:38','2016-03-12 17:37:38'),
	(46,8,'2016-03-22',1,9800,9800,NULL,NULL,'PENDING',NULL,'','Deji Lana','0808084','Wale OgundoAyus','080808',NULL,NULL,NULL,'2016-03-22 11:56:00','2016-03-22 11:56:00'),
	(50,10,'2016-03-22',1,5500,5500,NULL,NULL,'PENDING',NULL,'','Akeem Lana','0808084','Ayodele Oshunlana','080808',NULL,NULL,NULL,'2016-03-22 22:45:55','2016-03-22 22:45:55'),
	(51,9,'2016-03-22',1,5750,5750,2,NULL,'PAID','2016-03-22 23:50:50','','Omoyele','0801628368','kins name','01873728',NULL,NULL,NULL,'2016-03-22 23:47:16','2016-03-22 23:50:50');

/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bus_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bus_types`;

CREATE TABLE `bus_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `bus_types` WRITE;
/*!40000 ALTER TABLE `bus_types` DISABLE KEYS */;

INSERT INTO `bus_types` (`id`, `name`, `show`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'Hiace',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(2,'Luzury',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(3,'G-Wagon',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(4,'Toyota',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(5,'Coaster',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(6,'Hiace',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(7,'Luzury',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(8,'G-Wagon',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(9,'Toyota',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(10,'Coaster',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(11,'Hiace',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(12,'Luzury',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(13,'G-Wagon',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(14,'Toyota',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(15,'Coaster',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(16,'Hiace',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(17,'Luzury',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(18,'G-Wagon',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(19,'Toyota',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(20,'Coaster',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(21,'Hiace',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(22,'Luzury',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(23,'G-Wagon',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(24,'Toyota',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(25,'Coaster',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(26,'Hiace',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(27,'Luzury',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(28,'G-Wagon',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(29,'Toyota',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(30,'Coaster',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(31,'Hiace',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(32,'Luzury',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(33,'G-Wagon',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(34,'Toyota',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(35,'Coaster',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(36,'Hiace',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(37,'Luzury',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(38,'G-Wagon',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(39,'Toyota',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(40,'Coaster',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07');

/*!40000 ALTER TABLE `bus_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table call_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `call_logs`;

CREATE TABLE `call_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `call_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `call_logs_booking_id_index` (`booking_id`),
  KEY `call_logs_user_id_index` (`user_id`),
  CONSTRAINT `call_logs_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
  CONSTRAINT `call_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table chartered_bookings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chartered_bookings`;

CREATE TABLE `chartered_bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `operator_id` int(10) unsigned NOT NULL,
  `trip_id` int(10) unsigned DEFAULT NULL,
  `travel_date` datetime NOT NULL,
  `pickup_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `park_id` int(10) unsigned DEFAULT NULL,
  `agreed_price` double NOT NULL,
  `bus_type_id` int(10) unsigned DEFAULT NULL,
  `bus_features` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next_of_kin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `next_of_kin_mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_passengers` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `paid_date` datetime NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `payment_method_id` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chartered_bookings_payment_method_id_foreign` (`payment_method_id`),
  KEY `chartered_bookings_operator_id_index` (`operator_id`),
  KEY `chartered_bookings_trip_id_index` (`trip_id`),
  KEY `chartered_bookings_park_id_index` (`park_id`),
  KEY `chartered_bookings_bus_type_id_index` (`bus_type_id`),
  CONSTRAINT `chartered_bookings_bus_type_id_foreign` FOREIGN KEY (`bus_type_id`) REFERENCES `bus_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chartered_bookings_operator_id_foreign` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chartered_bookings_park_id_foreign` FOREIGN KEY (`park_id`) REFERENCES `parks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chartered_bookings_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chartered_bookings_trip_id_foreign` FOREIGN KEY (`trip_id`) REFERENCES `trips` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `chartered_bookings` WRITE;
/*!40000 ALTER TABLE `chartered_bookings` DISABLE KEYS */;

INSERT INTO `chartered_bookings` (`id`, `operator_id`, `trip_id`, `travel_date`, `pickup_location`, `destination`, `park_id`, `agreed_price`, `bus_type_id`, `bus_features`, `contact_name`, `contact_mobile`, `contact_email`, `next_of_kin`, `next_of_kin_mobile`, `no_of_passengers`, `status`, `paid_date`, `comments`, `payment_method_id`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(31,2,NULL,'2016-03-10 00:00:00','Abia','Lagos',NULL,0,NULL,'','Ayula','080','','','',0,'PENDING','0000-00-00 00:00:00','',NULL,NULL,'2016-03-06 17:24:07','2016-03-06 17:24:07'),
	(37,7,NULL,'2016-03-06 00:00:00','Ohafia, abia','Agege, Lagos',NULL,0,NULL,'','Ayula','0801628368','','','',0,'PENDING','0000-00-00 00:00:00','',NULL,NULL,'2016-03-06 17:52:29','2016-03-06 17:52:29'),
	(38,7,NULL,'2016-03-06 00:00:00','Ohafia, abia','Agege, Lagos',NULL,0,NULL,'','Ayula','0801628368','','','',0,'PENDING','0000-00-00 00:00:00','',NULL,NULL,'2016-03-06 17:56:31','2016-03-06 17:56:31');

/*!40000 ALTER TABLE `chartered_bookings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table devices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `devices`;

CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assigned_to` int(10) unsigned NOT NULL,
  `operator_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `devices_assigned_to_index` (`assigned_to`),
  KEY `devices_operator_id_index` (`operator_id`),
  CONSTRAINT `devices_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `agents` (`id`) ON DELETE CASCADE,
  CONSTRAINT `devices_operator_id_foreign` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table failed_queue_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_queue_jobs`;

CREATE TABLE `failed_queue_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `message_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `read_date` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_sender_id_index` (`sender_id`),
  KEY `messages_recipient_id_index` (`recipient_id`),
  CONSTRAINT `messages_recipient_id_foreign` FOREIGN KEY (`recipient_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `messages_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2014_10_12_000000_create_users_table',1),
	('2014_10_12_100000_create_password_resets_table',1),
	('2016_03_01_083203_init_schema',1),
	('2016_03_02_001352_create_queue_jobs_table',1),
	('2016_03_02_001356_create_failed_queue_jobs_table',1),
	('2016_03_05_165443_create_transactions_table',2),
	('2016_03_19_112704_create_devices_table',3);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table operators
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operators`;

CREATE TABLE `operators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commission` double(8,2) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `portal_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `operators` WRITE;
/*!40000 ALTER TABLE `operators` DISABLE KEYS */;

INSERT INTO `operators` (`id`, `name`, `access_code`, `address`, `contact_person`, `phone`, `email`, `details`, `website`, `commission`, `img`, `portal_link`, `active`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'GOD is GOOD','f2e0b70a-3762-3c51-a434-97dbb3cfce59','92851 Mayert Groves Apt. 195\nSouth Moniquebury, MN 85984-7944','Noemi Kuhlman','666-264-6621x117','Adalberto28@Cruickshank.com','Minus dolores dicta magni quia culpa odit fuga. Est similique unde est fuga rerum ipsam. Explicabo atque nesciunt corrupti voluptatum vero id. Totam dolores aliquid reiciendis accusamus praesentium.','',0.00,'godisgood.jpg','',1,NULL,'2016-03-02 09:10:05','2016-03-02 09:10:05'),
	(2,'EZEwanchukwu','049288e6-311b-3a75-adc0-62a6e8fe9cf9','01566 Florencio Parks\nDelphineview, IN 91447-4665','Edgardo O\'Reilly','(254)763-4705','uFranecki@hotmail.com','Omnis asperiores occaecati rerum dolorem nulla ad minus. Eum qui magnam qui.','',0.00,'','',1,NULL,'2016-03-02 09:10:05','2016-03-02 09:10:05'),
	(3,'Young Shall Grow Transport Ltd','a6cdd5e5-d4a6-3ec0-be51-9d582d3e2d33','856 Gutkowski Locks\nWest Eldaborough, VT 87867-0366','Mina Keeling DDS','989.236.0678x0910','Kolby04@gmail.com','Distinctio molestiae error quia. Soluta porro est distinctio deleniti laudantium quia. Delectus quod fuga modi. Et et ea suscipit quia.','',0.00,'','',1,NULL,'2016-03-02 09:10:05','2016-03-02 09:10:05'),
	(4,'Chisco','c4fb20a1-8f63-3675-ad15-2d5068073a12','02347 Gleason Shores Apt. 869\nEast Presley, NC 49256','Tessie Cole','(165)464-7106x82883','Aliza02@hotmail.com','Doloribus quasi ipsa inventore nemo at rerum aliquid. Nemo accusantium quo quia similique ullam debitis molestiae. Quaerat velit inventore quo quam ipsam nostrum.','',0.00,'three.jpg','',1,NULL,'2016-03-02 09:10:05','2016-03-02 09:10:05'),
	(5,'ABC transport','ffadf0dd-dc50-3857-9fec-b8c0ac235b6f','417 Lorenzo Viaduct\nNorth Fermin, NE 14297','Bobby Smith','085-564-9674','Keshawn.Kuhic@gmail.com','Dolorem necessitatibus accusantium aliquam est et quia et eveniet. Est animi deserunt aspernatur eaque in dicta ullam. Autem facilis sit et ea.','',0.00,'','',1,NULL,'2016-03-02 09:10:05','2016-03-02 09:10:05'),
	(6,'GUO Motors','425858ef-4fb5-3327-9222-425d84df6c08','306 Lind Pass Suite 822\nRogerstown, CT 40392','Grayson Torphy','02277392566','uGleichner@gmail.com','Ea repellat esse ut est non quisquam doloremque. Corrupti inventore non facilis voluptatibus dolorem maiores nostrum. Necessitatibus est voluptatem dolor totam. Magni ad eos sequi aut voluptatum aut.','',0.00,'','',1,NULL,'2016-03-02 09:10:05','2016-03-02 09:10:05'),
	(7,'Cross Country','9822b742-1cc4-3035-9bcd-77fa14bcf224','4664 Herman Port\nPort Danialport, KY 94720','Cristobal Carter','184-370-7969x20513','uStanton@hotmail.com','Dolorem consequatur magnam dolores ut amet animi enim. Eaque cupiditate inventore veniam voluptatem. Accusantium dignissimos optio est perferendis illo similique veniam aspernatur.','',0.00,'','',1,NULL,'2016-03-02 09:10:05','2016-03-02 09:10:05'),
	(8,'GOD is GOOD','e4ba860c-3f5a-3837-a969-581329f13a32','671 Lance Islands Suite 111\nNorth Tyshawn, FL 64119','Mohammad Veum','917.291.6518','Dare.Lindsey@Herzog.biz','Debitis sed laudantium alias totam quis. Corporis non fuga amet possimus est fuga. Ipsam reiciendis fuga totam praesentium.','',0.00,'godisgood.jpg','',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(9,'EZEwanchukwu','e36a18b4-7bf7-3a99-b1f0-7f40fe60d86c','7082 Beier Plains\nLucilebury, MA 04922-9876','Mr. Ephraim Gusikowski','(881)451-9113x33179','Jermain30@hotmail.com','Quae id saepe est reiciendis. Nesciunt rem est fugit esse rerum minima numquam. Corrupti voluptatem atque quidem perferendis voluptas.','',0.00,'','',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(10,'Young Shall Grow Transport Ltd','149c33d6-e09d-3a24-b00b-89fc0059e00f','97809 Vandervort Islands Apt. 621\nLake Keith, GA 95294','Greyson Heaney III','+98(2)4274537090','Brittany.Grady@Lang.info','Est rem quia voluptatem ipsa facilis natus. Et at consequatur atque numquam et.','',0.00,'','',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(11,'Chisco','882f83f2-4c61-3d81-8d11-982c0b8b6998','9028 Herman Plain Apt. 354\nMichaleville, VA 02461','Kelly Crona','1-721-372-7588x36226','Windler.Mavis@hotmail.com','Vitae accusantium dolorum amet aut qui labore quo. Tempora ut nostrum sit ut dolorem corrupti. Delectus molestiae et asperiores officia voluptatem et non. Officia non suscipit ipsam.','',0.00,'','',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(12,'ABC transport','96912283-51c0-3e8e-87b5-f6984d4dcef8','8649 Sanford Fall\nSouth Shayna, OK 26584-1421','Prof. Chesley Mayer II','1-128-624-9238x199','Kozey.Shaylee@Heaney.com','Laboriosam quos veniam quasi sunt. Et qui totam illo unde. Ea rem beatae quia officia consequatur. Sed quos voluptatem quia praesentium.','',0.00,'','',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(13,'GUO Motors','ce242228-b6fc-3ece-bd9f-1414b57effe1','839 Joelle Isle\nSouth Candaceshire, MS 01433','Mr. Madison Hermann III','430-822-0998','Jovani92@Howell.info','Atque recusandae qui maiores et ipsam dolores. Cum labore enim provident qui amet. Autem corrupti aut dolor quia unde quibusdam velit.','',0.00,'','',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(14,'Cross Country','74c47774-bd30-312f-8aa1-58cd7e0d40db','89947 Rodriguez Green Apt. 011\nLednerville, SC 98796','Prof. Ruby Lehner','471-884-7237x58273','Sylvan98@Douglas.info','Ratione qui facilis quia nisi. Iure quia ut et qui nam dolorum repellendus.','',0.00,'','',1,NULL,'2016-03-02 09:10:38','2016-03-02 09:10:38'),
	(15,'GOD is GOOD','6aaa3277-5448-33e7-8668-00430f568c75','40778 Silas Via Apt. 220\nWest Sincereside, NC 55588','Imelda Jacobson','(468)879-6359x38025','Kihn.Loyce@Klocko.com','Non velit voluptatem ab vel quod. Saepe omnis ut occaecati maiores voluptate. Repellat rerum consectetur non alias. Tempore sed voluptas officiis blanditiis.','',0.00,'','',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(16,'EZEwanchukwu','96e3567b-d142-36ad-ab19-bf37634984bb','218 Bridie Island\nNew Ulises, MO 79518','Ms. Sarai Baumbach PhD','1-574-108-6930x862','Joana08@gmail.com','Et quia et tempore aut facere qui in dolorem. Occaecati sapiente deleniti soluta ut voluptatem numquam.','',0.00,'','',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(17,'Young Shall Grow Transport Ltd','2c507810-6808-3d6c-9967-6de36b32d7cc','954 Conroy Gateway Suite 198\nKosston, NC 59497-4538','Alexanne Rice II','858.842.4623x61393','Vida.Bartell@Greenfelder.com','Dolorum quia nihil voluptatem aspernatur accusamus officia odit atque. Sunt odit iste libero. Amet soluta reiciendis sequi non.','',0.00,'','',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(18,'Chisco','21f08a1f-16a3-3ab2-a24e-b36c1aee07d7','14493 Tristin Track Apt. 909\nPort Deondre, NC 94148-3494','Burnice Reynolds','103-737-1586x7065','Leda59@gmail.com','Error molestiae exercitationem illo. Dolorem dolorem veniam dolorum voluptatum et. Itaque nulla cupiditate quo facilis nobis iste reiciendis.','',0.00,'three.jpg','',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(19,'ABC transport','18edb007-31be-3dad-b1c4-7a7c4839bbfe','65146 Emiliano Greens\nLake Lauryn, NM 21578','Mr. Arnold Cummings','(636)582-9955x608','Barton32@hotmail.com','Non voluptas saepe dolores non aliquam deleniti corporis. Saepe a consequatur ducimus quia explicabo aut id. Dolorem nobis nulla alias.','',0.00,'','',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(20,'GUO Motors','81bbc68d-add2-3acb-abae-b0f5c9ef76ad','2760 Kayla Drives\nNorth Alexandrefurt, TX 38679','Bradley Howell','1-350-790-1781','Reva85@Leuschke.net','Optio totam odio perspiciatis nihil labore pariatur et. Nostrum qui labore ut non. Occaecati commodi voluptas harum sit. Expedita tenetur perferendis beatae iusto et.','',0.00,'','',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(21,'Cross Country','64e6068d-fa64-33d5-b1be-42c7ab77ac32','08623 Wilfrid Glen\nLeslyland, RI 35303-9055','Dr. Nannie Runolfsson','679-339-5168','Quigley.Morris@Funk.com','Quo laboriosam porro dolor autem quasi. Quam non perspiciatis suscipit eligendi dolor qui. Reprehenderit minus impedit qui maiores adipisci voluptas similique consequatur.','',0.00,'','',1,NULL,'2016-03-02 09:11:01','2016-03-02 09:11:01'),
	(22,'GOD is GOOD','a14a594e-d325-3a76-9c0c-41d7eee6144f','1649 Spinka Club Apt. 078\nVivienneborough, OK 18195-1911','Susie Grimes','084.661.6320','Genoveva.Hermann@Osinski.com','Facilis earum exercitationem fugiat aliquam vero deserunt soluta. Sequi at voluptatem vitae animi corporis qui aliquid amet. Voluptas non est quae omnis.','',0.00,'','',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(23,'EZEwanchukwu','9562d5f2-790d-3934-8c15-6eaff52cb071','0750 Dach Mountains Apt. 837\nDonnellyfort, WY 65110','Sven Veum','(715)738-5966x412','pHuel@hotmail.com','Non eaque ut reprehenderit autem. Quibusdam aut dolorem recusandae corrupti qui. Occaecati ratione aut et ab enim expedita.','',0.00,'','',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(24,'Young Shall Grow Transport Ltd','5e1ef3f1-2cac-3305-abdd-0a466d11bc5f','7252 Vandervort Path\nJaronside, AK 10992-0229','Mrs. Rose Bradtke','998.168.5792x464','jMurphy@Osinski.info','Doloribus qui perspiciatis ducimus qui. Et quo qui aut placeat nobis. Voluptas nam eos aut cumque maiores sunt. Deleniti praesentium ad et magnam praesentium cumque accusantium eligendi.','',0.00,'','',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(25,'Chisco','9ccb4ef1-e7d3-3bc2-a288-a5558e8eae57','92423 Taurean Circles\nEffertzstad, MS 10866-5152','Marcella Haag','053.820.9001','pKeeling@gmail.com','Quod nulla qui id esse. Aperiam cum odio sit eos enim tempore. Odio non pariatur error est praesentium corporis.','',0.00,'','',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(26,'ABC transport','103701ae-b4ee-3af4-b0ba-de28e6b13cc8','72367 Thiel Glen\nWest Emmie, DE 48228','Prof. Rico Johnston Jr.','09784215836','Conor34@Hyatt.com','Modi et pariatur sed nihil nihil qui ut est. Quia qui eius cum id porro doloribus. Sit tempore est suscipit vel exercitationem aut in. Officiis nihil facere eos ea voluptatem velit eum.','',0.00,'','',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(27,'GUO Motors','42b51a33-e71d-3531-bf40-ad3fb9892476','495 Kunze Heights Apt. 230\nEast Gussie, SD 44467','Nikki Predovic','(339)762-3263','Braun.Odell@Erdman.org','Magni unde id repellat eum atque. Excepturi repudiandae rerum et dolorem odit. Asperiores expedita aut magnam. Voluptas quis exercitationem voluptas nesciunt repellat tempora.','',0.00,'','',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(28,'Cross Country','bdc5b71f-22f5-3087-8403-5686442a03f5','78673 Cruickshank Fork\nPort Brenden, VT 67156','Yolanda Leannon Sr.','144-192-9975','pSchoen@yahoo.com','Non voluptas commodi dicta quis. Mollitia laudantium et odit unde.','',0.00,'','',1,NULL,'2016-03-02 09:11:20','2016-03-02 09:11:20'),
	(29,'GOD is GOOD','247c0f53-2aa3-3efa-b15c-9ca2f405c060','892 Douglas Stravenue Apt. 986\nAmayaville, MT 18748','Maxine Stamm','727-729-4837','Keeling.Jettie@Borer.net','Illum sapiente consequatur iusto nemo omnis id corporis. Reprehenderit explicabo molestias quia tenetur enim reprehenderit mollitia qui. Cupiditate culpa vitae eaque sunt corporis.','',0.00,'','',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(30,'EZEwanchukwu','d7e5a207-7e4c-321a-a38d-99949a05ec2f','93738 Anabelle Stravenue\nNorth Wadeberg, KY 95347','Dashawn Farrell','845.414.5152','Hyatt.Saige@Schamberger.com','Dolorum quas ducimus amet est unde consequatur. Ea laborum totam delectus sed officia. Ipsum rerum in magnam nihil.','',0.00,'','',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(31,'Young Shall Grow Transport Ltd','d1b78bd7-e81a-3608-bf70-c49b13a4a95e','841 Larson Way\nParkershire, ME 42793','Edison Senger DDS','462-933-2600x5105','Joseph.Purdy@hotmail.com','Voluptas repellendus rerum atque voluptates. Omnis aut esse consequatur ipsa. Illo et sequi vero enim.','',0.00,'','',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(32,'Chisco','ff87165d-9e5d-310d-a305-3c15869a8e0a','021 Howe Run Suite 282\nBeckerstad, CT 80039','Dr. Louie Gorczany','02980511774','Bednar.Russell@yahoo.com','Eos ut sed aspernatur fugit voluptas doloremque non sit. Provident aut repellendus cum ut qui animi provident. Ducimus pariatur qui corrupti quibusdam. Qui id id animi dolores sed est animi.','',0.00,'','',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(33,'ABC transport','5255bc54-f22a-3813-b1a8-f6fdd55d355f','218 Caitlyn Wells Apt. 830\nBorertown, ME 36123-6208','Eldred Friesen','+28(8)1993468805','Antonina.Franecki@hotmail.com','Laboriosam aliquid minus amet quia porro neque aut. Voluptate eum ipsum sint saepe aut voluptas ducimus. Ut sed rerum molestiae voluptatibus et. Error quia voluptate qui aut.','',0.00,'','',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(34,'GUO Motors','a1c82895-983a-34bc-b369-5ef3f76a1d2d','0159 Green Walks Suite 669\nNew Lonzo, HI 46195-4107','Jodie Kub V','(253)113-4019','Vivienne69@hotmail.com','Tempore veritatis ut dolores iure. Est modi et iure optio odit adipisci. Mollitia ratione et eaque est quae.','',0.00,'','',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(35,'Cross Country','e63c1489-6f00-30b8-94e8-905e488945ef','6069 Donnell Village\nSouth Conner, NY 76015-2261','Mr. Hunter Boehm DVM','239-141-0653','kHarris@Cruickshank.com','Est dolorem et beatae eum quod distinctio. Et veritatis delectus delectus modi dolores. Omnis aperiam consequatur alias iusto. Accusantium ea rerum incidunt repellendus.','',0.00,'','',1,NULL,'2016-03-02 09:12:12','2016-03-02 09:12:12'),
	(36,'GOD is GOOD','98fa0c46-5fa7-3b31-a692-648220b75909','25480 McGlynn Drive\nGaylordmouth, ME 77723','Jewel Jones','(193)281-5837x258','Mike.Bashirian@Moen.com','Voluptates iusto expedita repellat occaecati. Animi voluptates qui adipisci ea deleniti quibusdam. Repellendus a alias nihil quia voluptatibus. Odit soluta rerum beatae atque aut.','',0.00,'','',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(37,'EZEwanchukwu','34e7f46a-5126-3bd6-b0c7-0846b08c9ac0','0314 Schaden Overpass\nSouth Robbiehaven, MD 87678-4230','Furman Little','(037)513-2453','lEmmerich@Howell.com','Aut facere veritatis quia voluptatem recusandae et. Aut et et deleniti magni autem tenetur. Consequatur odio enim nobis quis consequatur est eius cumque. Eligendi est officia molestias voluptatibus.','',0.00,'','',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(38,'Young Shall Grow Transport Ltd','0f260d82-69b5-3bed-9359-8a02a6df6400','7048 Batz Union Apt. 648\nSouth Hankmouth, SC 43327','Arlie Rath','1-251-184-4887','Okey.Mante@Stoltenberg.com','Consequatur ipsum sit aut et tenetur numquam. Corrupti architecto asperiores neque voluptatem placeat reiciendis. Sint cupiditate aliquam enim. Dolores est rerum cupiditate aut aut.','',0.00,'','',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(39,'Chisco','5a2996a3-5b20-308d-a162-04d1d366413f','0105 Skiles Stravenue\nNew Ladarius, NM 16905','Emmanuelle Beahan','880-314-1018x8588','uWilderman@Hermann.com','Consequatur itaque totam nostrum quaerat consequatur officia odit perferendis. Et consequuntur corporis minima id quae. Et quibusdam rerum necessitatibus modi minima et.','',0.00,'','',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(40,'ABC transport','c4708e5e-17a7-362b-8ad1-42ef127617b0','933 Trace Square\nEast Adrian, DC 39497','Merritt Jenkins','681-751-1223','jMante@Quitzon.org','Aut ab enim possimus voluptatem. Odit reprehenderit ut iusto debitis neque. Distinctio rerum ducimus mollitia facere.','',0.00,'','',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(41,'GUO Motors','edd44b05-6c9f-3725-ad92-40a77d6e7140','444 O\'Connell Village\nNew Rodrigofurt, AK 89646','Dillon Dach','+04(1)3675337450','Sofia86@yahoo.com','Ut aut ea ab et hic omnis voluptas. Alias praesentium repellendus inventore fuga necessitatibus aut velit. Accusantium doloremque iste ratione voluptatem iste modi.','',0.00,'','',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(42,'Cross Country','eec5245e-dc57-3c5f-a6d9-e95cfd95225e','055 Bogisich Wall\nWest Alfonzo, NV 99993','Major Feeney','1-509-211-9262x15676','Tremblay.Elsa@hotmail.com','Est fugit necessitatibus corrupti eaque commodi nam nam. Quia cum iusto quidem aut. Nihil occaecati dolore porro aut est quibusdam.','',0.00,'','',1,NULL,'2016-03-02 09:15:14','2016-03-02 09:15:14'),
	(43,'GOD is GOOD','22a24426-0620-3799-9d54-8bb1717fd9ff','667 Beier View Apt. 117\nEast Jevon, MS 24293','Dr. Deondre Hoeger DVM','188-794-2524','gDaugherty@Welch.com','At reiciendis et et consequatur sapiente. Quas sint earum et. Enim sint molestiae quod ut cupiditate voluptas.','',0.00,'','',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(44,'EZEwanchukwu','d4c9668f-27cb-3897-8118-3747afb98e8a','590 Stoltenberg Junction Suite 692\nGideonstad, NY 66015-8652','Emiliano Stracke','1-577-104-1633','nPredovic@Marks.biz','Ipsa vel animi quo modi. Et culpa ab dolorem rerum sed quisquam quod. Minus et impedit doloremque qui ut qui architecto.','',0.00,'','',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(45,'Young Shall Grow Transport Ltd','fae20738-5cdd-353f-bb6a-300d76b76186','131 Silas Avenue\nEast Betty, IA 09914-1524','Prof. Kattie Waters III','(875)423-6831','lDoyle@Kilback.com','Consequatur blanditiis officiis voluptas eius aspernatur aut. Ut velit culpa aut. Sed aut et autem velit. Labore inventore quo modi aspernatur.','',0.00,'','',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(46,'Chisco','79b6fe74-0a4e-3212-90dd-3746c6063355','8867 Kris Courts\nLake Friedaburgh, VT 01638-6738','Mr. Gregory Borer DVM','282.946.4291x32909','lKerluke@yahoo.com','Est expedita delectus voluptas quas impedit hic. Laboriosam illum nesciunt officiis aliquam. Error aliquid quo in est expedita.','',0.00,'','',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(47,'ABC transport','a5890569-0f8f-3958-a777-742e79930b0f','8496 Kautzer Cliff\nEast Winona, MI 56028','Alysha Wehner','1-430-327-4679x91684','Linnie.Harris@Bergnaum.com','Ipsum totam rerum voluptatem minima nemo ipsum quidem. Sint sit ex saepe dolorem officiis rem. Cum velit vel consequatur earum quasi aspernatur voluptates.','',0.00,'','',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(48,'GUO Motors','0315c2e9-6908-311f-8d42-28b93ad36ec4','628 Drake Mews\nJoefort, OR 03480','Matilda Franecki','113-408-9115','Kelsie89@OHara.com','Quidem enim vero et praesentium quam reprehenderit. Ut qui tenetur perspiciatis voluptatem natus. Ut expedita aut laudantium perferendis velit temporibus.','',0.00,'','',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(49,'Cross Country','e2cba6a8-07c0-3d9a-a517-cb73f3e52042','1292 Maximo Radial\nMarilieview, OH 02171-2534','Alexandre Ebert','597-539-8366x4928','kEffertz@yahoo.com','Qui minima omnis et nam et minus. Eius labore illo et voluptatem quas aliquid saepe expedita. Numquam corrupti illum ducimus neque rerum. Enim asperiores quia voluptatem sint omnis.','',0.00,'','',1,NULL,'2016-03-02 09:16:10','2016-03-02 09:16:10'),
	(50,'GOD is GOOD','39f19927-42cb-3320-ae9a-83e72909e1b3','483 Barrows Meadow Apt. 468\nJammieland, NC 74567-1129','Dr. Maximus Satterfield DDS','125.387.4792x60839','Turner.Efren@Gulgowski.com','Vel temporibus maxime veritatis. Explicabo enim impedit maxime voluptas iusto porro. Iste vitae maxime tempora cupiditate eum.','',0.00,'','',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(51,'EZEwanchukwu','96c44d78-1c02-3edf-b38f-5d4e865ce74b','8849 Satterfield Rapid Apt. 099\nEloyport, MO 66360','Lazaro Goyette','375-219-1459x25730','oGerlach@gmail.com','Quia vel in deserunt nulla vel non minima dolores. Ducimus aut repellendus et cum.','',0.00,'','',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(52,'Young Shall Grow Transport Ltd','402e8bd9-c6eb-357f-a122-1c53da9105a6','732 Sasha Flats Apt. 769\nHelenton, MA 44773','Sadye Wisozk','813-663-3052','Freeda.Weimann@Kuhlman.com','Tempora voluptas eum saepe tempora molestiae. Facilis molestiae et aut cumque explicabo et. Autem mollitia officiis sit in consequatur. Dolorum rerum aut maiores exercitationem.','',0.00,'','',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(53,'Chisco','88e1b566-9d85-38e2-a3ba-1b1d9e6ee7a0','0075 Tracey Station\nWelchchester, SC 77729-9820','Westley Jacobi','(587)664-5328x73376','Zula62@gmail.com','Dolorem quod ex porro excepturi minima odit. Quidem quidem cumque nam quia eius sapiente maiores repudiandae. Dolorum qui cupiditate et voluptatem consequatur.','',0.00,'','',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(54,'ABC transport','d8105654-4ce9-3b09-a5ca-925593d855ca','8273 Schneider Circle Apt. 987\nNorth Cathryn, CA 19208','Walter Wisozk MD','09245253668','Berge.Maudie@Keeling.com','Alias velit repudiandae omnis sint aspernatur quaerat optio suscipit. Eius reprehenderit qui odit necessitatibus quibusdam. Nesciunt necessitatibus aut sit aut et voluptatem.','',0.00,'','',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(55,'GUO Motors','b9dcc1e0-8a8a-3746-873c-1cf31fa55e00','2859 Roderick Track Suite 836\nEast Annamariestad, MA 40522-7363','Art Lubowitz II','258-853-6477x3300','Koby63@Hamill.info','Numquam non magni voluptatem corporis facilis. Aut accusantium voluptates eum aut voluptatem debitis et. Corrupti dolor aut voluptates numquam.','',0.00,'','',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(56,'Cross Country','b5dbbb1b-198f-3438-a98f-b941a76a6775','369 Zemlak Avenue\nElvaborough, OR 40818-7455','Prof. Garfield Bahringer I','1-729-905-2956','Rosie75@yahoo.com','Beatae praesentium qui aut minima. Ea sed eum totam animi. Unde aut ut quasi quidem. Sint earum saepe quaerat esse voluptatem.','',0.00,'','',1,NULL,'2016-03-02 09:17:20','2016-03-02 09:17:20'),
	(57,'GOD is GOOD','92cf18f3-07e4-3a0f-a116-e90aafc44442','676 Trinity Cape Suite 496\nSouth Giuseppe, KS 12414-5193','Verdie Blanda','(392)044-9047x08263','Lindsay.Jacobson@Kreiger.com','Aspernatur quaerat dolorem laborum sint aut in ipsa esse. Dolore ipsum eaque qui qui sint dignissimos. Voluptatem itaque illum recusandae ut consequatur.','',0.00,'','',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(58,'EZEwanchukwu','53fd6fc8-5865-38fc-99b7-c1589299c52b','291 Ewell Ranch\nNorth Gunner, MS 77686-6614','Dr. Tracey Hilll','1-927-279-9055','Reyes.Cummings@yahoo.com','Dolor repudiandae id voluptatem inventore optio impedit. Aut veniam temporibus odit aut rerum. Dolorem et et architecto sit dolorum. Magni nobis veniam nisi doloribus facilis qui consequatur.','',0.00,'','',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(59,'Young Shall Grow Transport Ltd','348d00fa-907d-388f-bf5f-0340ad28becb','25948 Mosciski Port\nSouth Pierce, AZ 36392-7886','Horacio Kihn','(039)318-6882x5762','bTrantow@Nader.net','Ut omnis officia et voluptatibus aliquid voluptatem id. Quo quia et error perspiciatis aut esse et perspiciatis. Accusantium at voluptas illum eaque porro debitis at ab.','',0.00,'','',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(60,'Chisco','4b0dbc1b-cbf7-3ba6-b743-cd6d567a6b2e','28548 Emerson Roads\nCummeratatown, GA 24179-1817','Trudie Schmeler I','1-419-481-0780x132','Moen.Domenica@Marks.org','Illo odio dolorem non commodi. Earum id velit maiores exercitationem. Ut et suscipit officiis enim. Exercitationem alias commodi neque et earum nihil et sunt.','',0.00,'','',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(61,'ABC transport','0dda5999-c7b9-3f96-ab63-e28b342030c0','696 Sammie Turnpike Apt. 754\nAntonettefurt, HI 43656','Bettie Gaylord','(464)052-4048x62228','Conner77@hotmail.com','Excepturi consequuntur facere necessitatibus ipsam voluptas omnis veritatis omnis. Et vitae ut autem. Necessitatibus qui consequatur quisquam nihil voluptate. Aut qui architecto eligendi ab.','',0.00,'','',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(62,'GUO Motors','7195b1a2-b303-358f-b866-aabb0cbcff4f','800 Crona Extension Apt. 033\nSouth Nevamouth, NH 04165','Dr. Dorothea Kris I','1-757-777-9564','Missouri07@hotmail.com','Enim sunt quod et deserunt quasi et architecto. Accusantium ipsa unde dolorem exercitationem cumque facere natus.','',0.00,'','',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(63,'Cross Country','699a38a9-7e8b-3694-a480-c1fe8104e64b','11266 Farrell Mall\nEast Bernieceburgh, TN 17569-1644','Rahul Schmitt','1-270-492-5820x555','nMcClure@Macejkovic.com','Veritatis expedita vel eos quis. Veritatis et et voluptatibus quisquam laborum quia. Iste repudiandae accusantium ut aut id quia quia.','',0.00,'','',1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07');

/*!40000 ALTER TABLE `operators` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table parks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parks`;

CREATE TABLE `parks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `boardable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parks_state_id_index` (`state_id`),
  CONSTRAINT `parks_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `parks` WRITE;
/*!40000 ALTER TABLE `parks` DISABLE KEYS */;

INSERT INTO `parks` (`id`, `state_id`, `name`, `address`, `active`, `boardable`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,2,'Aba [in Abia]','30 / 32 Milverton Avenue, Abia State',1,0,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(2,34,'Maraba [in Abuja]','Maraba Bus Park',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(3,1,'Agege - Oke Okoto [in Lagos]','Agege Motor Park',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(4,1,'Cele [in Lagos]','Lagos',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(5,10,'Calabar [in Cross-River]','',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(6,35,'Ebonyi','',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(7,34,'Gwagwalada [in Abuja]','Abuja',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(8,22,'Ilorin [in Kwara]','ilorin Bug Motor Park',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(9,31,'Jalingo [in Taraba]','Jags Motor Park',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(10,5,'Isofia [in Anambra]','Isoks Motor Park',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(11,23,'Lafia [in Nasarawa]','',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(12,24,'Minna [in Niger]','Minna',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(14,2,'Ohafia [in Abia]','',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(15,11,'Warri [in Delta]','',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(16,8,'Zaki-Biam [in Benue]','',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(17,27,'Ibadan [in Oyo]','Agege Motor Park',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(18,12,'Ramat Park [in Benin]','Agege Motor Park',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(19,13,'Holy Ghost [in Enugu]','Holy Ghost Opposite Main market, Enugu',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(20,38,'Ring Road [in Accra]','Ring Road',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03');

/*!40000 ALTER TABLE `parks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table passengers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `passengers`;

CREATE TABLE `passengers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `passengers_booking_id_index` (`booking_id`),
  CONSTRAINT `passengers_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `passengers` WRITE;
/*!40000 ALTER TABLE `passengers` DISABLE KEYS */;

INSERT INTO `passengers` (`id`, `booking_id`, `name`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(7,45,'Ogede master',NULL,'2016-03-12 17:38:03','2016-03-12 17:38:03'),
	(8,45,'Adeola',NULL,'2016-03-12 17:38:03','2016-03-12 17:38:03');

/*!40000 ALTER TABLE `passengers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table payment_methods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_methods`;

CREATE TABLE `payment_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `bank_id` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_methods_bank_id_index` (`bank_id`),
  CONSTRAINT `payment_methods_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;

INSERT INTO `payment_methods` (`id`, `name`, `description`, `active`, `bank_id`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'Bank','Bank Deposit',1,NULL,NULL,NULL,NULL),
	(2,'PayStack','Online Payment',1,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table queue_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `queue_jobs`;

CREATE TABLE `queue_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `queue_jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `permissions`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'Super User','','2016-02-09 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `exposed` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table states
# ------------------------------------------------------------

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `boardable` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;

INSERT INTO `states` (`id`, `name`, `region`, `active`, `boardable`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'Lagos','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(2,'Abia','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(3,'Adamawa','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(4,'Akwa-Ibom','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(5,'Anambra','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(6,'Bauchi','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(7,'Bayelsa','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(8,'Benue','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(9,'Borno','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(10,'Cross-River','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(11,'Delta','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(12,'Edo','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(13,'Enugu','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(14,'Gombe','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(15,'Imo','Ghana',1,0,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(16,'Jigawa','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(17,'Kaduna','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(18,'Kano','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(19,'Katsina','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(20,'Kebbi','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(21,'Kogi','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(22,'Kwara','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(23,'Nasarawa','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(24,'Niger','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(25,'Ogun','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(26,'Osun','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(27,'Oyo','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(28,'Plateau','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(29,'Rivers','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(30,'Sokoto','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(31,'Taraba','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(32,'Yobe','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(33,'Zamfara','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(34,'Abuja','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(35,'Ebonyi','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(36,'Ekiti','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(37,'Ondo','Nigeria',1,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(38,'Cotonou','Republic of Benin',0,1,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03'),
	(39,'Accra','Ghana',1,0,NULL,'2016-03-02 08:53:03','2016-03-02 08:53:03');

/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table transactions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_booking_id_index` (`booking_id`),
  CONSTRAINT `transactions_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;

INSERT INTO `transactions` (`id`, `booking_id`, `status`, `response`, `created_at`, `updated_at`)
VALUES
	(14,1,'Transaction Not Found','Failed','2016-03-01 00:00:00',NULL),
	(15,1,'Transaction Successful','Success','2016-03-04 00:00:00',NULL),
	(25,51,'Transaction Not Found','Failed','2016-03-22 23:50:32','2016-03-22 23:50:32'),
	(26,51,'Transaction Successful','Success','2016-03-22 23:50:50','2016-03-22 23:50:50');

/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trips
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trips`;

CREATE TABLE `trips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `operator_id` int(10) unsigned NOT NULL,
  `source_park_id` int(10) unsigned NOT NULL,
  `dest_park_id` int(10) unsigned NOT NULL,
  `departure_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fare` double(8,2) NOT NULL,
  `bus_type_id` int(10) unsigned NOT NULL,
  `no_of_seats` int(11) NOT NULL,
  `parent_trip_id` int(10) unsigned DEFAULT NULL,
  `ac` tinyint(1) NOT NULL DEFAULT '0',
  `security` tinyint(1) NOT NULL DEFAULT '0',
  `tv` tinyint(1) NOT NULL DEFAULT '0',
  `insurance` tinyint(1) NOT NULL DEFAULT '0',
  `passport` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `operator_trip_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trips_operator_id_index` (`operator_id`),
  KEY `trips_source_park_id_index` (`source_park_id`),
  KEY `trips_dest_park_id_index` (`dest_park_id`),
  KEY `trips_bus_type_id_index` (`bus_type_id`),
  KEY `trips_parent_trip_id_index` (`parent_trip_id`),
  CONSTRAINT `trips_bus_type_id_foreign` FOREIGN KEY (`bus_type_id`) REFERENCES `bus_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `trips_dest_park_id_foreign` FOREIGN KEY (`dest_park_id`) REFERENCES `parks` (`id`) ON DELETE CASCADE,
  CONSTRAINT `trips_operator_id_foreign` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`) ON DELETE CASCADE,
  CONSTRAINT `trips_parent_trip_id_foreign` FOREIGN KEY (`parent_trip_id`) REFERENCES `trips` (`id`) ON DELETE CASCADE,
  CONSTRAINT `trips_source_park_id_foreign` FOREIGN KEY (`source_park_id`) REFERENCES `parks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `trips` WRITE;
/*!40000 ALTER TABLE `trips` DISABLE KEYS */;

INSERT INTO `trips` (`id`, `name`, `operator_id`, `source_park_id`, `dest_park_id`, `departure_time`, `duration`, `fare`, `bus_type_id`, `no_of_seats`, `parent_trip_id`, `ac`, `security`, `tv`, `insurance`, `passport`, `active`, `operator_trip_id`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(8,'Coaster_Oke Okoto [in Lagos]_Calabar [in Cross-River]_15000',2,3,5,'7am','',9800.00,1,20,NULL,1,1,1,1,0,1,1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(9,'Coaster_Oke Okoto [in Lagos]_Calabar [in Cross-River]_15000',3,3,5,'11am','',5750.00,1,20,NULL,1,0,0,0,0,1,1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(10,'Venza_Oke Okoto [in Lagos]_BCalabar [in Cross-River]_9000',1,3,5,'8pm','',5500.00,1,20,NULL,1,1,1,1,0,1,1,NULL,'2016-03-02 09:24:07','2016-03-19 11:24:35'),
	(11,'Venza_Ibadan [in Oyo]_Ohafia [in Abia]_15000',4,17,14,'10am','',15000.00,1,20,NULL,1,1,1,1,0,1,1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(12,'Lorry_Ibadan [in Oyo]_Ohafia [in Abia]_15000',4,17,14,'6am','',3000.00,1,20,NULL,0,0,0,0,0,1,1,NULL,'2016-03-02 09:24:07','2016-03-02 09:24:07'),
	(18,'G-Wagon_Jalingo [in Taraba]__Warri [in Delta]->10000',1,9,15,'7am','7hrs',10000.00,8,10,NULL,1,0,1,1,0,1,0,NULL,'2016-03-09 16:51:58','2016-03-09 16:51:58'),
	(28,'Coaster_Maraba [in Abuja]_Minna [in Niger]_3000',1,2,12,'5am','20',3000.00,5,0,NULL,1,0,1,0,1,1,0,NULL,'2016-03-23 13:44:53','2016-03-23 13:44:53'),
	(29,'Hiace_Ohafia [in Abia]_Holy Ghost [in Enugu]_7380',1,14,19,'10am','5',7380.00,1,0,NULL,1,1,0,0,1,1,0,NULL,'2016-03-23 13:44:53','2016-03-23 13:44:53');

/*!40000 ALTER TABLE `trips` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_logs`;

CREATE TABLE `user_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `log_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_logs_user_id_index` (`user_id`),
  CONSTRAINT `user_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `role_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_index` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `active`, `role_id`, `remember_token`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(6,'Ayodele','Oshunlana','sophie@ayolana.com','$2y$10$60tqlsORxMEblkri.8a.zub6lP3xNnWeJ0JardkPbx/kVFHyQuASC','+2348038953794','13, Arigbanla street,',1,1,'n9pdvTEzmF4vb5ymSxgjOaL11yzho1J7NkUBLyaHQNnF47hbC4nCLaS56ykA',NULL,'2016-03-09 09:43:42','2016-03-22 12:33:12'),
	(7,'Ayula','Bosede','me@ayolana.com','$2y$10$0XqVj6YGiRVezmMr3DDwLuOpfLf5gy/ZJujl0SG7XblosMe6/yF96','08133772946','',1,1,'y8cChupAkTRchuTVnhfP9lgJpqOOko7nxI10RKH8tplGG5ph7xrdaG00pTSN',NULL,'2016-03-12 18:58:29','2016-03-12 19:11:49');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
