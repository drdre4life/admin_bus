<?php

namespace DummyNamespace;

use DummyRootNamespaceHttp\Requests;
use DummyRootNamespaceHttp\Controllers\Controller;

use DummyRootNamespaceModels\{{modelName}};
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class DummyClass extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        ${{crudName}} = {{modelName}}::paginate(15);

        $page_title = '{{crudName}}';

        return view('{{viewPath}}{{crudName}}.index', compact('{{crudName}}', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Add {{crudNameSingular}}';

        return view('{{viewPath}}{{crudName}}.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        {{validationRules}}
        {{modelName}}::create($request->all());

        Session::flash('flash_message', '{{modelName}} added!');

        return redirect('{{routeGroup}}{{crudName}}');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        ${{crudNameSingular}} = {{modelName}}::findOrFail($id);

        $page_title = 'View {{crudNameSingular}}';
        return view('{{viewPath}}{{crudName}}.show', compact('{{crudNameSingular}}', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        ${{crudNameSingular}} = {{modelName}}::findOrFail($id);

        $page_title = 'Edit {{crudNameSingular}}';
        return view('{{viewPath}}{{crudName}}.edit', compact('{{crudNameSingular}}', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        {{validationRules}}
        ${{crudNameSingular}} = {{modelName}}::findOrFail($id);
        ${{crudNameSingular}}->update($request->all());

        Session::flash('flash_message', '{{modelName}} updated!');

        return redirect('{{routeGroup}}{{crudName}}');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        {{modelName}}::destroy($id);

        Session::flash('flash_message', '{{modelName}} deleted!');

        return redirect('{{routeGroup}}{{crudName}}');
    }

}
