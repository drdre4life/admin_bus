<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'success' => 'You have successfully logged in',
    'failed' => 'These credentials do not match our records.(password error)',
    'no_account' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email_taken' => 'This email :email, has already been taken',
    'phone_taken' => 'This phone :phone, has already been taken',
    'system_error' => 'You network seems to be faulty. Please Try Again',
    'no_data_change' => 'Your form appears the same',
    'no_email_in_db' => 'You dont\'t have an account with us',
    'forgot_password_sent' => 'Your password has been sent to your account',
    'wrong_reset_code' => 'Reset code is wrong.',
    'password_change_success' => 'Your password has been changed successfully',
    'user-not-found' => 'User not found!',
    'user-no-specs' => 'Hi :name, You have not filled in your job settings. Please take a few seconds to fill it :spec_link',
    'user-no-specs-no-link' => 'Hi :name, You have not filled in your job settings.',
    'matching-jobs-found' => 'Hi :name. Here are the jobs that match you across the internet based on your selections: :selections. Modify :spec_link',
    'matching-jobs-found-no-link' => 'Hi :name. Here are the jobs that match you across the internet based on your selections: :selections.',
    'get-saved-jobs'  =>    'Hi :name. Here are your saved jobs ',
    'get-viewed-jobs'  =>    'Hi :name. Here are the jobs you have viewed',
    'get-applied-jobs'  =>    'Hi :name. Here are the jobs you\'ve applied for.',
    'saved_career_summary' =>  'Career summary has been saved.'

];
