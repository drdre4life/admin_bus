@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Edit Agent </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('agents') }}">Agents </a></li>
          <li class="active">Edit Agent </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">
           @if(Session::has('flash_message'))
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                <strong>Oh snap!</strong> {{Session::get('flash_message')}}
              </div>
            @endif

            <div class="portlet-content">

                {!! Form::model($agent, [
                    'method' => 'PATCH',
                    'url' => ['agents', $agent->id],
                    'class' => 'form-horizontal'
                ]) !!}


                             <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Username: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                {!! $errors->first('username', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Password: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('password', null, ['class' => 'form-control']) !!}
                {!! $errors->first('password', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Phone: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('phone', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                {!! Form::label('address', 'Address: ', ['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-6">
                    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
                {!! $errors->first('address', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('percentage') ? 'has-error' : ''}}">
                {!! Form::label('percentage', 'Percentage: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('percentage', null, ['class' => 'form-control']) !!}
                {!! $errors->first('percentage', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


               <div class="form-group {{ $errors->has('device') ? 'has-error' : ''}}">
                {!! Form::label('device', 'Device: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    <select name="device" class="form-control select22">
                        <option value="">Choose Device</option>
                        @foreach($devices as $device)
                        <option value="{{ $device->id }}" @if($device->assigned_to == $agent->id) selected=selected @endif >{{ $device->name }}</option>
                        @endforeach
                    </select>
                {!! $errors->first('device', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['agents', $agent->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-times"></i> Delete Agent', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                {!! Form::close() !!}
            </li>
            <li>
              <a href="{{ url('agents') }}">
                <i class="fa fa-bars"></i> 
                List Agents
              </a>
            </li>
            <li class="">
              <a href="{{ url('agents/create') }}">
                <i class="fa fa-plus"></i> 
                Add Agent
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection