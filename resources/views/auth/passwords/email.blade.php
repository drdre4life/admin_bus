@extends('layouts.auth')

<!-- Main Content -->
@section('content')

<div class="account-body">

      <h3 class="account-body-title">Password Reset</h3>

      <h5 class="account-body-subtitle">We'll email you instructions on how to reset your password.</h5>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

      <form class="form account-form" role="form" method="POST" action="{{ url('/password/email') }}" >
        {!! csrf_field() !!}

        <div class="form-group">
          <label for="forgot-email" class="placeholder-hidden">Your Email</label>
          <input type="email" class="form-control" name="email"  id="forgot-email" placeholder="Your Email" tabindex="1" value="{{ old('email') }}">
            @if ($errors->has('email'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div> <!-- /.form-group -->

        
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="2">
            Reset Password &nbsp; <i class="fa fa-refresh"></i>
          </button>
        </div> <!-- /.form-group -->

        <div class="form-group">
          <a href="{{ url('/login') }}"><i class="fa fa-angle-double-left"></i> &nbsp;Back to Login</a>
        </div> <!-- /.form-group -->

      </form>


    </div> <!-- /.account-body -->


@endsection
