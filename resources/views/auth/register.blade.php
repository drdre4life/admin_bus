@extends('layouts.auth')

@section('content')


<div class="account-body">

      <h3 class="account-body-title">Register.</h3>

      <h5 class="account-body-subtitle">Please sign in to get access.</h5>

      <form class="form account-form" role="form" method="POST" action="{{ url('/register') }}">
        {!! csrf_field() !!}

        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">First Name</label>
          <input type="text" class="form-control" name="first_name" id="login-username" value="{{ old('first_name') }}" placeholder="First Name" tabindex="1">
            @if ($errors->has('first_name'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif

        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Last Name</label>
          <input type="text" class="form-control" name="last_name" id="login-username" value="{{ old('last_name') }}" placeholder="Last Name" tabindex="1">
            @if ($errors->has('last_name'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif

        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Email Address</label>
          <input type="email" class="form-control" name="email" id="login-username" value="{{ old('email') }}" placeholder="Email Address" tabindex="1">
            @if ($errors->has('email'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Phone</label>
          <input type="text" class="form-control" name="phone" id="login-username" value="{{ old('phone') }}" placeholder="Phone" tabindex="1">
            @if ($errors->has('phone'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
            @endif

        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Password</label>
          <input type="password" class="form-control" name="password" id="login-password" placeholder="Password" tabindex="2">
            @if ($errors->has('password'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div> <!-- /.form-group -->

        <div class="form-group">
          <label for="login-password" class="placeholder-hidden">Confirm Password</label>
          <input type="password" class="form-control" name="password_confirmation" id="login-password2" placeholder="Confirm Password" tabindex="2">
            @if ($errors->has('password_confirmation'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div> <!-- /.form-group -->


        <div class="form-group">
          <label for="login-username" class="placeholder-hidden">Address</label>
          <textarea class="form-control"  rows="5" name="address" id="login-username" placeholder="Address" tabindex="1">{{ old('address') }}</textarea>
            @if ($errors->has('address'))
                <span class="parsley-error-list">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif

        </div> <!-- /.form-group -->


        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
            Register &nbsp; <i class="fa fa-play-circle"></i>
          </button>
        </div> <!-- /.form-group -->

      </form>


    </div> <!-- /.account-body -->

@endsection
