@extends('layouts.master')

@section('content')

<div class="content-header">
    <h2 class="content-header-title">Financial Summary </h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="active">Bookings</li>
    </ol>
</div> <!-- /.content-header -->


<div class="row">

    <div class="col-md-12 col-sm-12">

        <br/><br/>

        <div class="well">
            <h4><span class="text-primary">Summary</span></h4>
            <?php $avg = $paidSum - ((5 / 100) * $paidSum) ?>
            <?php $bal = $paidSum - $avg ?>
            <?php $tot = $paidSum - $bal ?>
            <p>
                <span class="text-primary">Total Guarantee Fee Paid: </span> <strong id="totalPending">&#8358;{{
                    number_format($paidSum,2) }}</strong> |
                <span class="text-primary">Total Paid Booking: </span> <strong id="totalPaid">&#8358;{{
                    number_format($tot,2) }}</strong> |
                <span class="text-primary">Guarantee fee left: </span> <strong id="totalCancelled">&#8358;{{
                    number_format($bal,2) }}</strong>


            </p>

        </div>


        <div class="btn-group pull-right">
            <div id="reportrange" class="pull-right"
                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
        </div>
        <h4 class="heading-inline pull-right">
            &nbsp;&nbsp;
            <small>Choose date</small>
            &nbsp;&nbsp;</h4>

        <br/><br/><br/>


        <div class="portlet">

            @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                <strong>Well done!</strong> {!! session('flash_message') !!}
            </div>

            @endif

            <div class="portlet-content">


                <div class="table-responsive">

                    <table
                        class="table table-striped table-bordered table-hover table-highlight table-checkable"
                        data-provide="datatable1"
                        data-display-rows="10"
                        data-info="true"
                        data-search="true"
                        data-length-change="true"
                        data-paginate="true"
                        >
                        <thead>
                        <tr>
                            <th>Number of Bookings</th>
                            <th>Number of passengers</th>
                            <th>Revenue</th>
                            <th>Average Cost</th>
                            <!-- <th>Commision (10%)</th>
                            <th>Net revenue</th> -->

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ number_format($paidCount) }}</td>
                            <td>{{ number_format($psgCount) }}</td>
                            <td style="color:green">&#8358;{{ number_format($paidSum) }}</td>
                            <td style="color:black">&#8358;{{ number_format($avg, 2) }}</td>
                            <!-- <td style="color:red;">&#8358;{{ number_format(0) }}</td>
                            <td style="color:green;">&#8358;{{ number_format(0) }}</td> -->
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->


            </div>
            <!-- /.portlet-content -->

        </div>
        <!-- /.portlet -->


    </div>
    <!-- /.col -->


</div> <!-- /.row -->

<script type="text/javascript">

    function updateUrl(name, value) {
        window.location.search = jQuery.query.set(name, value);

        return false;
    }

    function refreshReport() {

        console.log($('#select-input').val());
        window.location = '{{ url("trip-schedules") }}/' + $('#park-input').val();


    }


    $(function () {

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        cb(moment("{{ $start_date }}"), moment("{{ $end_date }}"));

        $('#reportrange').daterangepicker({
            ranges: {
                // 'Next Tomorrow': [moment().add(2, 'days'), moment().add(2, 'days')],
                // 'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));

            updateUrl('daterange', picker.startDate.format('YYYY-MM-DD') + ' ' + picker.endDate.format('YYYY-MM-DD'));
        });

    });

</script>

@endsection
