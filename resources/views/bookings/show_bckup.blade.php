@extends('layouts.master')

@section('content')

<div class="container">
    <h1>Booking</h1>

<br><br>
<div class="col-md-8 col-sm-8">
    

    <div id="flash"></div>
    <div class="portlet">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Booking Details
              </h3>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">

              <dl>
                <dt>Name</dt>
                <dd>{{ $booking['booking_code'] }}</dd>
                <dt>Date</dt>
                <dd> {{ $booking['date'] }} </dd>

                <dt>STATUS</dt>
                <dd id='statusField'> {{ $booking['status'] }} </dd>

                <dt>Passengers Count </dt>
                <dd>{{ $booking['passenger_count'] }}</dd>
                <dt>Unit Cost</dt>
                <dd>{{ $booking['unit_cost'] }}</dd>
                <dt>Final Cost</dt>
                <dd>{{ $booking['final_cost'] }}</dd>
                
                <dt>Final Cost</dt>
                <dd>{{ $booking['final_cost'] }}</dd>

                <dt>Payment method</dt>
                <dd>{{ $booking['paymentmthd']['name'] }}</dd>
                <dt>Payment Date</dt>
                <dd> <?php echo date('D m, Y', strtotime($booking['paid_date'])) ?></dd>

                <hr>

                
                <dt>Contact Name</dt>
                <dd>{{ $booking['contact_name'] }}</dd>

                <dt>Contact Phone</dt>
                <dd>{{ $booking['contact_phone'] }}</dd>

                <dt>Next of Kin Name</dt>
                <dd>{{ $booking['next_of_kin'] }}</dd>

                <dt>Next of Kin Name Phone</dt>
                <dd>{{ $booking['next_of_kin_phone'] }}</dd>

              </dl>

              

            </div> <!-- /.portlet-content -->

          </div>
</div>

<div class="col-md-3 col-sm-3">

    <div class="portlet">
        <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Update Status
              </h3>
            </div> <!-- /.portlet-header -->

            <div class="portlet-content">
            
          <!-- AJAX FORM -->
            <form action="{{ route('update-status') }}" id="updateStatus" method="post">

              <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
              <select name="status" id="status" class="form-control">
                <option value="">Choose one</option>
                <option value="NOT">NOT PAID</option>
                <option value="PENDING">PENDING</option>
                <option value="PAID">PAID</option>
              </select><br>

              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">

              <input type="submit" class="btn btn-primary" value="Update">
            </form>
            </div>


    </div>


    @if($booking['status'] == 'PAID')
    <div class="portlet">
        <div class="portlet-header">
              <h3>
                <i class="fa fa-reorder"></i>
                Send Sms
              </h3>
            </div> <!-- /.portlet-header -->

            <div class="portlet-content">
              <button class="btn btn-danger" id="ResendSms" >Resend Sms</button>
            </div>
    </div>
    @else

    <div class="portlet">
        <div class="portlet-header">
              <h3>
                <i class="fa fa-reorder"></i>
                Booking
              </h3>
            </div> <!-- /.portlet-header -->

            <div class="portlet-content">
              <a href="{{ url('bookings/' . $booking['id'] . '/edit') }}" class="btn btn-danger" >Edit Booking</a>
            </div>
    </div>


    @endif

     

  

      
    <div class="portlet">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-reorder"></i>
                Trip Details
              </h3>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">

              <dl>
                <dt>Name</dt>
                <dd><a href="{{ url('trips', $booking['trip']['id']) }}" >{{ $booking['trip']['name'] }}</a></dd>
                <dt>Departure Time</dt>
                <dd> {{ $booking['trip']['departure_time'] }} </dd>
               <dt>Bus Type</dt>
                <dd> {{ $booking['trip']['bus_type']['name'] }} </dd>

              </dl>

              

            </div> <!-- /.portlet-content -->

          </div>


        <div class="portlet">

                <div class="portlet-header">
                  <h3>
                    <i class="fa fa-reorder"></i>
                    Booking Notes
                  </h3>
                </div> <!-- /.portlet-header -->

                <div class="portlet-content">
                  <!-- <a href="#" class="btn btn-danger">ADD Booking Notes</a> -->
                  <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#myModal"> Add Booking Notes </button>
                </div> <!-- /.portlet-content -->

        </div>


</div>
    

    <div class="col-md-10 col-sm-8">
          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th><th>Status</th><th>Response</th><th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($booking['transactions'] as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ url('transactions', $item['id']) }}">{{ $item['status'] }}</a></td>
                            <td>{{ $item['response'] }}</td>
                            <td>{{ $item['created_at'] }}</td>
                            

                          
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->
    </div> <!-- /.col -->


    <div id="showNote">
         <div class="col-md-10 col-sm-8">
          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th><th>User_id</th><th>Notes</th><th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($notes as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td>{{ $item->user->first_name }}</td>
                            <td>{{ $item->notes }}</td>
                            <td>{{ $item->created_at }}</td>
                            

                          
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->
</div> <!-- /.col -->
    </div>

</div>


<div class = "modal fade" id="myModal" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Add Booking note
            </h4>
         </div>
         <form action="{{ route('booking-note') }}" method="post" id="bookingNotes">
         <div class = "modal-body" id="contentArea">
              <input type="hidden" id="SubBtn" name="_token" value="<?php echo csrf_token(); ?>">
          <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
            Notes: <textarea rows="7" cols="50" name="notes" class="form-control"></textarea>
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "submit" id="NoteSub" class = "btn btn-primary">
               Submit changes
            </button>
         </form>

         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  

@endsection