@extends('layouts.master')

@section('content')

    <h1>Cashoffice</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Contact Name</th><th>Address</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $cashoffice->id }}</td> <td> {{ $cashoffice->name }} </td><td> {{ $cashoffice->contact_name }} </td><td> {{ $cashoffice->address }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection