@extends('layouts.master')

@section('content')

<div class="content-header">
    <h2 class="content-header-title">Charteredbooking </h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="active">Charteredbooking</li>
    </ol>
</div> <!-- /.content-header -->


<div class="row">

    <div class="col-md-12 col-sm-8">

        <div class="portlet">

            <div class="portlet-content">

                <div class="table-responsive">

                    <table
                        class="table table-striped table-bordered table-hover table-highlight table-checkable"
                        data-provide="datatable"
                        data-display-rows="10"
                        data-info="true"
                        data-search="true"
                        data-length-change="true"
                        data-paginate="true"
                        >
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th> Contact Name</th>
                            <th> Contact Mobile</th>
                            <th> Travel Date</th>
                            <th> Pickup Location</th>
                            <th> Destination</th>
                            <th> Passengers</th>
                            <th> Budget</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- */$x=0;/* --}}
                        @foreach($charteredbooking as $item)
                        <tr>
                            <td>{{ $x }}</td>
                            <td>{{ $item->contact_name }}</td>
                            <td>{{ $item->contact_mobile }}</td>
                            <td>{{ $item->travel_date }}</td>
                            <td>{{ $item->pickup_location }}</td>
                            <td>{{ $item->destination }}</td>
                            <td>{{ $item->no_of_passengers }}</td>
                            <td>{{ $item->agreed_price }}</td>
                            <td>
                                <a href="{{ url('chartered-bookings/' . $item->id ) }}" title="Edit">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <!-- <a href="{{ url('charteredbooking/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>  -->
                                <!--  {!! Form::open([
                                     'method'=>'DELETE',
                                     'url' => ['charteredbooking', $item->id],
                                     'style' => 'display:inline'
                                 ]) !!}
                                     {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                 {!! Form::close() !!} -->
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->


            </div>
            <!-- /.portlet-content -->

        </div>
        <!-- /.portlet -->


    </div>
    <!-- /.col -->

    <!--  <div class="col-md-2 col-sm-4">

         <ul id="myTab" class="nav nav-pills nav-stacked">
         <li class="active">
           <a href="{{ url('charteredbooking/create') }}">
             <i class="fa fa-plus"></i>
             Add New CharteredBooking
           </a>
         </li>
       </ul>

     </div> -->

</div> <!-- /.row -->


@endsection
