@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Device </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('devices') }}">Devices </a></li>
          <li class="active">Add Device </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'devices', 'class' => 'form-horizontal']) !!}

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('name', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


                            <div class="form-group {{ $errors->has('serial_number') ? 'has-error' : ''}}">
                {!! Form::label('serial_number', 'Serial Number: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('serial_number', null, ['class' => 'form-control']) !!}
                {!! $errors->first('serial_number', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


            <div class="form-group {{ $errors->has('serial_number') ? 'has-error' : ''}}">
                {!! Form::label('serial_number', 'Operator: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                  <input class="form-control" type="text" name="operator_id" value="{{ $operator_name }}" disabled> <small>TO change this, select another operator</small>
                    <!-- {!! Form::text('serial_number', null, ['class' => 'form-control']) !!} -->
                {!! $errors->first('serial_number', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


          

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('devices') }}">
                <i class="fa fa-bars"></i> 
                List Devices
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection