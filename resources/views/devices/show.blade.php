@extends('layouts.master')

@section('content')

    <h1>Device</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Assigned To</th><th>Operator Id</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $device->id }}</td> <td> {{ $device->name }} </td><td> {{ $device->assigned_to }} </td><td> {{ $device->operator_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection