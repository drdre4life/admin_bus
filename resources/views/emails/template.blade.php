<table cellpadding="2" cellspacing="0" border="0" width="600">
	<tbody>
		<tr>
			<td width="600"><table cellpadding="2" cellspacing="0" border="0" width="600">
				<tbody>
					<tr>
						<td width="600"><font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif ">
							<table cellpadding="2" cellspacing="0" border="0" width="580">
								<tbody>
									<tr>
										<td width="100" align="center">
											<img border="0" src="https://ci5.googleusercontent.com/proxy/kbEY4b3KoVa2P5Vt8-UOYZxjwQaQyjf9KG78EmqyF2PvCp8xzSRVGNaQSov2PqK2mKYTURcsGJUcDkdytA3nzqEJEaCZZ-4_CYg4DDyMS0WI5g65Jsp2neo0=s0-d-e1-ft#http://customer3.videcom.com/AzmanAir/AzmanAirgraphics/emaillogo.png" class="CToWUd">
										</td>
										<td width="300" align="center">
											<b>
												<font color="#6495ed" size="4" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">e-Ticket Receipt &amp; Itinerary</font></b>
										</td>
										<td width="200" align="center">
											<img border="0" src="https://ci6.googleusercontent.com/proxy/mbX45jmRD0ISmmMNFDNz7h-TzQ62TdcnQ0y-VXz5BE0N7fOqjSkKSBhmwtSehY9IFUBadj8WpZSwLnnLSBqQEHmbzhfoN1MLhbtIojd6KznMi0hMk-Uchypxkyt80A=s0-d-e1-ft#http://customer3.videcom.com/AzmanAir/barcodes%5C30Aug2016%5CRAARC37.gif" class="CToWUd">
										</td>
									</tr>
								</tbody>
							</table>
							<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif ">We have made the following booking for you:</font>
							<br>
							<table cellpadding="2" cellspacing="0" border="0" width="592">
								<tbody>
									<tr>
										<td width="594">
											<table cellpadding="2" cellspacing="1" border="0" width="592">
												<tbody>
													<tr>
														<td width="296">
															<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Booking Reference: </font>
															<font size="3" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
															<b>AARC37      </b>
															</font>
														</td>
														<td width="296">
															<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Date of Issue: </font>
															<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
																<b>30 Aug 2016</b>
															</font>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</font>
					</td>
				</tr>
			<tr>
				<td>
					<br>
					<table cellpadding="2" cellspacing="0" border="0" width="600">
						<tbody>
							<tr>
								<td width="150" bgcolor="#000000">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>CONTACT INFORMATION</b>
									</font>
								</td>
								<td width="200" bgcolor="#000000">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>PASSENGER INFORMATION</b>
									</font>
								</td>
								<td width="200" bgcolor="#000000">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>FLIGHT TICKET NUMBERS</b>
									</font>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<table cellpadding="2" cellspacing="1" width="150">
										<tbody>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">MS IYANU AKINREMI</font>
												</td>
											</tr>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Mob: +2347051116841</font>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td valign="top">
									<table cellpadding="2" cellspacing="1" width="200">
										<tbody>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
														<b>MS IYANU AKINREMI</b>
													</font>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td valign="top">
									<table cellpadding="2" cellspacing="1" width="160">
										<tbody>
											<tr>
												<td width="280">
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
														<b>ETKT000 2300471854/01</b>
														<br>
													</font>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
						<b>DEPARTING:</b>
					</font>
					<br>
					<table cellpadding="2" cellspacing="0" border="0" width="600">
						<tbody>
							<tr>
								<td bgcolor="#000000" width="100">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Date</font>
								</td>
								<td bgcolor="#000000" width="175">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>Wed 31 Aug 16 </b>
									</font>
								</td>
								<td bgcolor="#000000" width="100">&nbsp;</td>
								<td bgcolor="#000000" width="175">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">All Times Local</font>
								</td>
								<td bgcolor="#000000" width="150">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Flight-Number
									</font>
								</td>
								<td bgcolor="#000000" width="150">
									<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Cabin (Book&nbsp;Class)
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">From:</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
									<b>LAGOS</b>
									</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Departure
									</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>
											<span class="aBn" data-term="goog_194066611" tabindex="0">
												<span class="aQJ">10:00</span>
											</span>
										</b>
									</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>AZM2311 (HK)</b>
									</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>Y</b> (D)
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">To:</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>KANO</b>
									</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Arrive</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
										<b>
											<span class="aBn" data-term="goog_194066612" tabindex="0">
												<span class="aQJ">11:15</span>
											</span>
										</b>
									</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">&nbsp;</font>
								</td>
								<td>
									<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">&nbsp;</font>
								</td>
							</tr>
						</tbody>
					</table>
					<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
						<b>Info Flight 1: </b>
						<br>
						<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
							<b>Rules Flight 1: </b>
							<br>Tickets are non-refundable except on flight cancellation by carrier.
							<br> Child discount-75% of Adult fare.<br> No show will attract upgrade to a higher fare.<br> Check-in closes 30mins before departure.<br> Boarding gate closes 15mins before departure.<br> ECO Noshow charge-N5000.<br> Infant-10% of adult fare.<br> Excess Baggage-Charge N300.<br>00 per kilo.<br> Baggage allowance 20kg.<br> Unaccompanied Minor-Full Adult Fare.<br> Validity - 6 months from date of purchase.
							<br><br>
							<font size="3" face="Times New Roman, Geneva, Arial, Helvetica, Sans Serif ">
								<b>ADDITIONAL ITEMS:</b>
								<table cellpadding="2" cellspacing="0" border="0" width="600">
									<tbody>
										<tr>
											<td width="300" bgcolor="#000000">
												<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Item</font>
											</td>
											<td width="300" bgcolor="#000000">
												<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Price</font>
											</td>
										</tr>
										<tr>
											<td>
												<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">300.00+0.00 NGN CREDIT CARD CHARGE</font>
											</td>
											<td>
												<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">NGN 300.00</font>
											</td>
										</tr>
									</tbody>
								</table>
								<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
									<b>Additional Items Total</b> NGN 300.00
								</font>
								<table cellpadding="2" cellspacing="0" border="0" width="600">
									<tbody>
										<tr>
											<td bgcolor="#000000" width="300">
												<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
													<b>PRICING</b>
												</font>
											</td>
											<td bgcolor="#000000" width="300">
												<font color="#FFFFFF" size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
													<b>PAYMENT</b>
												</font>
											</td>
										</tr>
										<tr>
											<td>
												<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif"></font>
											</td>
											<td>
												<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Form of Payment Credit Card</font>
											</td>
										</tr>
										<tr>
											<td>
												<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Price NGN 6855.00</font>
											</td>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Card Number 000XXXXXXXXXX000</font>
												</td>
											</tr>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">Booking Fee  BF         NGN 500.00</font>
												</td>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
														<b>Payment Amount</b> NGN  28800.00
													</font>
												</td>
											</tr>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">VALUE ADDED TAX JY         NGN 327.75</font>
												</td>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">&nbsp;</font>
												</td>
											</tr>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">TICKET SALES CHARGE NG         NGN 327.75</font>
												</td>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">&nbsp;</font>
												</td>
											</tr>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">AIRPORT TAX QT         NGN 2500.00</font>
												</td>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">&nbsp;</font>
												</td>
											</tr>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">FUEL SURCHARGE YQ         NGN 18289.50</font>
												</td>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">&nbsp;</font>
												</td>
											</tr>
											<tr>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
														<b>Total Price NGN 28800.00</b>
													</font>
												</td>
												<td>
													<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">&nbsp;</font>
												</td>
											</tr>
										</tbody>
									</table>
								</font>
							</font>
						</font>
					</td>
				</tr>
			</tbody>
		</table>
		<table cellpadding="2" cellspacing="1" border="0" width="600">
			<tbody>
				<tr>
					<td width="200">
						<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
							<b>Agent Name: VIDWEB</b>
						</font>
					</td>
					<td width="200">
					<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
						<b>Agent ID: WEB04</b>
					</font>
				</td>
				<td width="200">
					<font size="1" face="Verdana, Geneva, Arial, Helvetica, Sans Serif">
						<b>Agent Office: Web</b>
					</font>
				</td>
			</tr>
		</tbody>
	</table>
</td>
</tr>
</tbody>
</table>