@extends('layouts.master')

@section('content')

<div class="content-header">
    <h2 class="content-header-title">Feedbacks </h2>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="active">Feedbacks</li>
    </ol>
</div> <!-- /.content-header -->


<div class="row">

    <div class="col-md-12 col-sm-8">

        <div class="portlet">

            <div class="portlet-content">

                <div class="table-responsive">

                    <table
                        class="table table-striped table-bordered table-hover table-highlight table-checkable"
                        data-provide="datatable"
                        data-display-rows="10"
                        data-info="true"
                        data-search="true"
                        data-length-change="true"
                        data-paginate="true"
                        >
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th> Name</th>
                            <th> E-Mail</th>
                            <th> Operator</th>
                            <th> Feedback</th>
                            <th> Status</th>
                            <th> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- */$x=1;/* --}}
                        @foreach($feedbacks as $item)
                        <tr>
                            <td>{{ $x++ }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->operator }}</td>
                            <td>{{ $item->feedback }}</td>
                            @if($item->active == 0)
                            <td>{{ 'Inactive' }}</td>
                            @else
                            <td>{{ 'Active' }}</td>
                            @endif
                            <td>
                                @if($item->active == 0)
                                <a href="{{ url('change-feedback-status/' . $item->id .'/1' ) }}" title="Activate">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa
                                    fa-check-circle"></i>
                                    </button>
                                </a>
                                @else
                                <a href="{{ url('change-feedback-status/' . $item->id .'/0' ) }}" title="Deactivate">
                                    <button type="submit" class="btn btn-primary btn-xs"><i class="fa
                                    fa-minus-circle"></i>
                                    </button>
                                </a>
                                @endif
                                <!-- <a href="{{ url('feedbacks/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>  -->
                                <!--  {!! Form::open([
                                     'method'=>'DELETE',
                                     'url' => ['feedbacks', $item->id],
                                     'style' => 'display:inline'
                                 ]) !!}
                                     {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                 {!! Form::close() !!} -->
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->


            </div>
            <!-- /.portlet-content -->

        </div>
        <!-- /.portlet -->


    </div>
    <!-- /.col -->

    <!--  <div class="col-md-2 col-sm-4">

         <ul id="myTab" class="nav nav-pills nav-stacked">
         <li class="active">
           <a href="{{ url('feedbacks/create') }}">
             <i class="fa fa-plus"></i>
             Add New CharteredBooking
           </a>
         </li>
       </ul>

     </div> -->

</div> <!-- /.row -->


@endsection
