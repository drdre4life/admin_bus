@extends('layouts.master')

@section('content')


<div class="container">
    <h1>Chartered Booking Details</h1>

    <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('chartered-bookings') }}">Chartered bookings</a></li>
          
        </ol>

<br><br>

<div class="alert alert-success hide" id="paidAlertDiv">
  Booking has been marked as PAID. <!--You can now print the Ticket-->
</div>
@if ($charteredbooking['child'] == 0 )
       <input type ="hidden" id="rt" value="{{'0'}}">
@else
      <input type ="hidden" id="rt" value="{{'1'}}">
@endif
<div class="col-md-11 col-sm-8">
    
    <div class="portlet">

            <div class="portlet-header">

              <h1>
                <i class="fa fa-bus"></i>
                  <!-- <span>
                    Booking Code: 
                    <span id="bookingcode">
                      {{ $charteredbooking['booking_code'] }}
                    </span>
                  </span> -->
              </h1>

            </div> <!-- /.portlet-header -->
            
            <div class="portlet-content" class="col-sm-12">
             <div class="clearfix"></div>
              
              <div class="row">

                <div class="col-md-4" id= "dateDiv">
                <dl>

                <dt>STATUS</dt>
                <dd>  
                  <span id='statusField' name="statusField" class="label label-warning">{{ $charteredbooking['status'] }}</span> 
                </dd>
                <br/>
                

                <!-- change made by laolu -->
                <dt>Booking Date</dt>
               
                <dd name="bookingdate"> {{ date('D, d/m/Y h:iA', strtotime($charteredbooking['created_at'])) }} </dd>
                <!-- change made by laolu -->

          
                <dt>Travel Date</dt>
                <dd name="departureDate"> {{ date('D, d/m/Y', strtotime($charteredbooking['travel_date'])) }} </dd>
                
                @if($charteredbooking['child'] == 1)
                  <dt>Return Date</dt>
                  <dd name="returnDate"> {{ date('D, d/m/Y', strtotime($charteredbooking['return_date']))  }} </dd>
                @endif

                
                </dl>
                </div>

                <div class="col-md-4" id="costDiv">
                
                <dl>
                
                  <dt>Contact name </dt>
                  <dd id="nop" name="nop">
                    {{ $charteredbooking['contact_name'] }}
                  </dd>

                  <dt>Contact phone </dt>
                  <dd id="nop" name="nop">
                    {{ $charteredbooking['contact_mobile'] }}
                  </dd>

                  <dt>Next of kin </dt>
                  <dd id="nop" name="nop">
                    {{ $charteredbooking['next_of_kin'] }}
                  </dd>

                  <dt>Next of kin phone </dt>
                  <dd id="nop" name="nop">
                    {{ $charteredbooking['next_of_kin'] }}
                  </dd>

                   <!-- <dt>Contact email </dt>
                  <dd id="nop" name="nop">
                    {{ $charteredbooking['contact_email'] }}
                  </dd> -->

                  <dt>Num of Passengers </dt>
                  <dd id="nop" name="nop">{{ $charteredbooking['no_of_passengers'] }}</dd>

                  <dt>Budget</dt>
                  <dd>&#8358;{{ number_format($charteredbooking['budget']) }}</dd>
                

                

             
</div>

<div class="col-md-3 col-sm-3">


<h4>Trip Details</h4>
                <dl>
                <dt>From:</dt>
                <dd name="from">{{ $charteredbooking['pickup_location'] }}</dd>

                <dt>To:</dt>
                <dd name="to">{{ $charteredbooking['destination'] }}</dd>

                
                 
                   

                              
                </dl>
                </span<i></dd></dl></div></div>

                <hr>
 

        

</div>
    

</div>
</dl>



@endsection