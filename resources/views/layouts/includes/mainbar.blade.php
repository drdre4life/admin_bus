<div class="mainbar">

  <div class="container">

    <button type="button" class="btn mainbar-toggle" data-toggle="collapse" data-target=".mainbar-collapse">
      <i class="fa fa-bars"></i>
    </button>

    <div class="row mainbar-collapse collapse">

     <div class="col-md-9 col-sm-6">

      <ul class="nav navbar-nav mainbar-nav">

        <li class="">
          <a href="{{ url('/') }}">
            <i class="fa fa-dashboard"></i>
            Dashboard
          </a>
        </li>

        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-desktop"></i>
            Bookings
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">   
            <li><a href="{{ url('bookings') }}"><i class="fa fa-user nav-icon"></i> Bookings</a></li>
            <li><a href="{{ url('bookings/status/PAID') }}"><i class="fa fa-user nav-icon"></i> Paid Bookings</a></li>
            <li><a href="{{ url('bookings/status/PENDING') }}"><i class="fa fa-user nav-icon"></i> Pending Bookings</a></li>
            <li><a href="{{ url('bookings/status/CANCELLED') }}"><i class="fa fa-user nav-icon"></i> Cancelled Bookings</a></li>
          </ul>
        </li>


        <li class="">
          <a href="{{ url('chartered-bookings') }}">
            <i class="fa fa-bus"></i>
            Chartered Bookings
          </a>
        </li>
        @if(Auth::user()->role->name == 'Super User')
        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-user"></i>
            Operators
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">   
            <li><a href="{{ url('operators') }}"><i class="fa fa-user nav-icon"></i> Active Operators</a></li>
            <li><a href="{{ url('operators/index/all') }}"><i class="fa fa-user nav-icon"></i> All Operators</a></li>
            
            
          </ul>
        </li>
        <li class="">
            <a href="{{ url('feedbacks') }}">
                <i class="fa fa-comment-o"></i>
                FeedBacks
            </a>
        </li>
        @endif

        <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-user"></i>
            Trips
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">   
            <li><a href="{{ url('trips') }}"><i class="fa fa-user nav-icon"></i> Manage trips </a></li>
            <li><a href="{{ url('trips/create') }}"><i class="fa fa-user nav-icon"></i> Add a trip</a></li>
            <li><a href="{{ url('trips') }}"><i class="fa fa-user nav-icon"></i> Adjust trip prices </a></li>
            <li><a href="#"><i class="fa fa-user nav-icon"></i> Upload Trips </a>
                <ul>
                    <li> <a href="{{ url('download-trips') }}"><i class="fa fa-user nav-icon"></i></i>
                           Download Template</a></li>
                    <li> <a href="{{ url('upload-trip') }}"><i class="fa fa-user nav-icon"></i>Update
                            Trips</a></li>
                </ul>
            </li>
            
          </ul>
        </li>


         @if(Auth::user()->role->name == 'Super User')
         <li class="dropdown ">
          <a href="#about" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-users"></i>
            Agents
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">   
            <li><a href="{{ url('agents') }}"><i class="fa fa-user nav-icon"></i> List Agents</a></li>
            <li><a href="{{ url('agents/create') }}"><i class="fa fa-user nav-icon"></i> Add Agent</a></li>
            
          </ul>
        </li>



<!-- 
         <li class="">
          <a href="{{ url('operators') }}">
            <i class="fa fa-dashboard"></i>
            Operator
          </a>
        </li> -->

        <!--li class="">
          <a href="#">
            <i class="fa fa-archive"></i>
            Reports
          </a>
        </li-->

        
       <!--  <li class="">
          <a href="{{ url('trips/search') }}">
            <i class="fa fa-search"></i>
            Book a Trip
          </a>
        </li> -->
        @endif


        @if(Auth::user()->role->name == 'Operator')

          <li class="">
            <a href="{{ url('fin-summary') }}">
              <i class="fa fa-money"></i>
                Finance Summary
            </a>
          </li>

        @endif



        <!-- <li class="dropdown ">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-files-o"></i>
            Sample Pages
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">
            <li><a href="./page-profile.html"><i class="fa fa-user nav-icon"></i> Profile</a></li>
            <li><a href="./page-invoice.html"><i class="fa fa-money nav-icon"></i> Invoice</a></li>
            <li><a href="./page-pricing.html"><i class="fa fa-dollar nav-icon"></i> Pricing Plans</a></li>
            <li><a href="./page-support.html"><i class="fa fa-question nav-icon"></i> Support Page</a></li>
            <li><a href="./page-gallery.html"><i class="fa fa-picture-o nav-icon"></i> Gallery</a></li>
            <li><a href="./page-settings.html"><i class="fa fa-cogs nav-icon"></i> Settings</a></li>
            <li><a href="./page-calendar.html"><i class="fa fa-calendar nav-icon"></i> Calendar</a></li>
          </ul>
        </li>   -->

      <!--   <li class="dropdown active">
          <a href="#contact" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            <i class="fa fa-external-link"></i>
            Extra Pages
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu" role="menu">
            <li>
              <a href="./page-notifications.html">
              <i class="fa fa-bell"></i> 
              &nbsp;&nbsp;Notifications
              </a>
            </li>     

            <li>
              <a href="./ui-icons.html">
              <i class="fa fa-smile-o"></i> 
              &nbsp;&nbsp;Font Icons
              </a>
            </li> 

            <li class="dropdown-submenu">
              <a tabindex="-1" href="#">
              <i class="fa fa-ban"></i> 
              &nbsp;&nbsp;Error Pages
              </a>

              <ul class="dropdown-menu">
                <li>
                  <a href="./page-404.html">
                  <i class="fa fa-ban"></i> 
                  &nbsp;&nbsp;404 Error
                  </a>
                </li>

                <li>
                  <a href="./page-500.html">
                  <i class="fa fa-ban"></i> 
                  &nbsp;&nbsp;500 Error
                  </a>
                </li>
              </ul>
            </li>

            <li class="dropdown-submenu">

              <a tabindex="-1" href="#">
              <i class="fa fa-lock"></i> 
              &nbsp;&nbsp;Login Pages
              </a>

              <ul class="dropdown-menu">
                <li>
                  <a href="./account-login.html">
                  <i class="fa fa-unlock"></i> 
                  &nbsp;&nbsp;Login
                  </a>
                </li>

                <li>
                  <a href="./account-login-social.html">
                  <i class="fa fa-unlock"></i> 
                  &nbsp;&nbsp;Login Social
                  </a>
                </li>

                <li>
                  <a href="./account-signup.html">
                  <i class="fa fa-star"></i> 
                  &nbsp;&nbsp;Signup
                  </a>
                  </li>

                <li>
                  <a href="./account-forgot.html">
                  <i class="fa fa-envelope"></i> 
                  &nbsp;&nbsp;Forgot Password
                  </a>
                </li>
              </ul>
            </li> 

            <li class="divider"></li>

            <li>
              <a href="./page-blank.html">
              <i class="fa fa-square-o"></i> 
              &nbsp;&nbsp;Blank Page
              </a>
            </li> 

          </ul>
        </li> -->

       

      </ul>

       </div>
          
        <div class="col-md-3 col-sm-6" style="margin-top: 13px">
          @include('layouts.includes.sel_operator')
        </div>

    </div> <!-- /.navbar-collapse -->   

  </div> <!-- /.container --> 

</div> <!-- /.mainbar -->

