<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <title>@if(isset($page_title)){{ ucfirst($page_title) }} | @endif Bus Tickets</title>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta http-equiv="refresh" content="600">
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">
  <link rel="stylesheet" href="{{ url('bckend/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ url('bckend/js/libs/css/ui-lightness/jquery-ui-1.9.2.custom.min.css') }}">
  <link rel="stylesheet" href="{{ url('bckend/css/bootstrap.min.css') }}">
    <!-- App CSS -->
  <link rel="stylesheet" href="{{ url('bckend/css/target-admin.css') }}">
  <link rel="stylesheet" href="{{ url('bckend/css/custom.css') }}">
  <link rel="stylesheet" href="{{ url('bckend/js/plugins/select2/select2.css') }}">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="{{ url('bckend/js/plugins/timepicker/bootstrap-timepicker.css') }}">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script src="https://use.fontawesome.com/818dbd02b4.js"></script>
  <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83001036-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
  <div class="navbar">
  <div class="container">
  <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <i class="fa fa-cogs"></i>
      </button>
      <a class="navbar-brand navbar-brand-image" href="url('/')">
        <img src="{{ asset('bckend/img/buscomng.jpg') }}" width="80px" alt="Site Logo">
      </a>
    </div> <!-- /.navbar-header -->
    @include('layouts.includes.navbar')
  </div> <!-- /.container -->
</div> <!-- /.navbar -->
  @include('layouts.includes.mainbar')
<div class="container">
  <div class="content">
    <div class="content-container">
      @yield('content')
    </div> <!-- /.content-container -->
  </div> <!-- /.content -->
</div> <!-- /.container -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <h4>Bus Tickets</h4>
        <p>A Mobile Ticketing Platform.</p>  

        <hr>    

        <p>&copy; {{ date('Y') }} BusTickets.ng</p>

      </div> <!-- /.col -->

      <div class="col-sm-3"> 

        <!--h4>Support</h4>

        <br>

        <ul class="icons-list">
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">Frequently Asked Questions</a>
          </li>
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">Ask a Question</a>
          </li>
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">Video Tutorial</a>
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">Feedback</a>
          </li>
        </ul-->          

      </div> <!-- /.col -->

      <div class="col-sm-3">

        <!--h4>Legal</h4>

        <br>

        <ul class="icons-list">
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">License</a>
          </li>
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">Terms of Use</a>
          </li>
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">Privacy Policy</a>
          </li>
          <li>
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="javascript:;">Security</a>
          </li>
        </ul-->          

      </div> <!-- /.col -->

      <div class="col-sm-3">

        <h4>Settings</h4>

        <ul class="icons-list">
          <li style="margin-bottom: 0">
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="http://www.bus.com.ng">Online Platform</a>
          </li>
          <li style="margin-bottom: 0">
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="http://bustickets.ng">Operator Platform </a>
          </li>
          <li style="margin-bottom: 0">
            <i class="fa fa-angle-double-right icon-li"></i>
            <a href="#">Payment Platform</a>
          </li>
          
        </ul>        

      </div> <!-- /.col -->

    </div> <!-- /.row -->

  </div> <!-- /.container -->
  
</footer>

  <script src="{{ url('bckend/js/libs/jquery-ui-1.9.2.custom.min.js') }}"></script>
  <script src="{{ url('bckend/js/libs/bootstrap.min.js') }}"></script>

 
  <!-- <script src="{{ asset('bckend/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('bckend/js/plugins/datatables/DT_bootstrap.js') }}"></script> -->
   <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="{{ asset('bckend/js/plugins/datatables/DT_bootstrap.js') }}"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.flash.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

  <script src="{{ asset('bckend/js/plugins/tableCheckable/jquery.tableCheckable.js') }}"></script>
  <script src="{{ asset('bckend/js/plugins/icheck/jquery.icheck.min.js') }}"></script>

  <script src="{{ asset('bckend/js/plugins/parsley/parsley.js') }}"></script>
  <script src="{{ asset('bckend/js/plugins/select2/select2.js') }}"></script>
  
  <script src="{{ url('bckend/js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
  <script src="{{ url('bckend/js/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
  <script src="{{ asset('bckend/js/demos/form-extended.js') }}"></script>
  <script src="{{ url('bckend/js/plugins/simplecolorpicker/jquery.simplecolorpicker.js') }}"></script>
  <script src="{{ url('bckend/js/plugins/textarea-counter/jquery.textarea-counter.js') }}"></script>
  <script src="{{ url('bckend/js/plugins/autosize/jquery.autosize.min.js') }}"></script>
  <script src="{{ url('bckend/js/target-admin.js') }}"></script>
  <script src="http://malsup.github.com/jquery.form.js"></script> 
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <script src="{{ URL::asset('bckend/js/plugins/jquery.query-object.js') }}" > </script>


    <script>
      // $(".select22").select2({
      //   placeholder: "Select an option",
      //   allowClear: true
      // });

    

      $('.select22').select2({ allowClear: true, placeholder: "Select..." })


      $('#timepicker1').timepicker();


      function ConfirmDelete()
      {
      var x = confirm("Are you sure you want to delete?");
      if (x)
        return true;
      else
        return false;
      }



      function currency_formatf(number){

        return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      }

    </script>
  

    <script>

        $('#updateStatus').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                  console.log(response);
                  $('#statusField').html(response);
                  $('#flash').html('<span class="alert alert-danger"> Booking Status Successfully updated </span>')
                },
                 reset: true
        });

         $('#bookingNotes').ajaxForm({
                beforeSubmit: genPreSubmit,
                success: function(response){
                  console.log(response);
                  $('#myModal').modal('hide')
                  $('#showNote').html(response);
                  $('#flash').html('<span class="alert alert-danger">Note Added Successfully</span>')

                },
                 reset: true
         }); 

        function genPreSubmit(){
          console.log("We are here....");
          $("#SubBtn").html('please wait...');

        }

      $('.select22').select2({ allowClear: true, placeholder: "Select..." })
      $('#timepicker1').timepicker();

      function ConfirmDelete(){
        var x = confirm("Are you sure you want to delete?");
        if (x)
          return true;
        else
          return false;
      }

    </script>



  
</body>
</html>
