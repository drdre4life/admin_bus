@extends('layouts.master')

@section('content')

    <h1>Park</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>State Id</th><th>Address</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $park->id }}</td> <td> {{ $park->name }} </td><td> {{ $park->state_id }} </td><td> {{ $park->address }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection