@extends('layouts.master')

@section('content')

   <div class="content-header">
        <h2 class="content-header-title">Add Setting </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('settings') }}">Settings </a></li>
          <li class="active">Add Setting </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">

                {!! Form::open(['url' => 'settings', 'class' => 'form-horizontal']) !!}

                            <div class="form-group {{ $errors->has('key') ? 'has-error' : ''}}">
                {!! Form::label('key', 'Key: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('key', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('key', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('value') ? 'has-error' : ''}}">
                {!! Form::label('value', 'Value: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('value', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('value', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('exposed') ? 'has-error' : ''}}">
                {!! Form::label('exposed', 'Exposed: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('exposed', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('exposed', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('exposed', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-3">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('settings') }}">
                <i class="fa fa-bars"></i> 
                List Settings
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->   

@endsection