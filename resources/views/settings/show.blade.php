@extends('layouts.master')

@section('content')

    <h1>Setting</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Key</th><th>Value</th><th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $setting->id }}</td> <td> {{ $setting->key }} </td><td> {{ $setting->value }} </td><td> {{ $setting->description }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection