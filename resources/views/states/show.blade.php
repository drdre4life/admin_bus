@extends('layouts.master')

@section('content')

    <h1>State</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Region</th><th>Active</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $state->id }}</td> <td> {{ $state->name }} </td><td> {{ $state->region }} </td><td> {{ $state->active }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection