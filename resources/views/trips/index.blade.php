@extends('layouts.master')

@section('content')
  

    <div class="content-header">
      <div class="row">
          <div class="col-md-9 col-sm-6">
            <h2 class="content-header-title">Trips </h2>
            <ol class="breadcrumb">
              <li><a href="{{ url('/') }}">Dashboard</a></li>
              <li class="active">Trips </li>
            </ol>

          </div>
          <div class="col-md-3 col-sm-6">
             
          </div>
      </div>
      </div> 
      <!-- /.content-header -->


      

      <div class="row">

        <div class="col-md-12 col-sm-12">

          <div class="portlet">

            <div class="portlet-content"> 

              @if (Session::has('message'))
                  <div class="alert alert-info">
                  {{ Session::get('flash') }}</div>
              @endif          

              <div class="table-responsive">

             <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <span id='flash' ></span>
                  <thead>
                    <tr>
                      <th>Operator</th>
                      <th>Trip Code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Departure Time</th>
                      <th>Fare</th>
                      <th>New Fare</th>
                      <th>Fare Expires</th>
                      <th>Bus Type</th>
                      <th>Features</th>
                      <th>Active?</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($trips['data'] as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>
                                <img src="{{ asset('logos/'.$item['operator']['img']) }}" width="80px" /><br/>
                                {{ $item['operator']['name'] }}</td>
                            <td>{{ $item['trip_code'] }}</td>
                            <td>{{ $item['sourcepark']['name'] }}</td>
                            <td>{{ $item['destpark']['name'] }}</td>
                            <td>{{ $item['departure_time'] }}</td>
                            <td>&#8358;{{ number_format($item['fare']) }}</td>
                             <td>{{ number_format($item['new_fare']) }}</td>
                            <td>{{ $item['fare_expiry_date'] }}</td>
                            <td>{{ $item['bustype']['name'] }}</td>
                             <td>
                               @if($item['ac'])
                                <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['security'])
                                    <span><i class="fa fa-check-square-o font-green-jungle"></i>  Security</span>
                                @else
                                <span ><i class="fa fa-times-circle font-red-thunderbird"></i>  Security</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['insurance'])
                                <span ><i class="fa fa-check-square-o font-green-jungle"></i>  Insurance</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Insurance</span>
                                @endif

                                &nbsp;&middot;&nbsp;
        
                                <br/>
                                @if($item['tv'])
                                <span><i class="fa fa-check-square-o font-green-jungle"></i>  TV</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  TV</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['passport'])
                                <span><i class="fa fa-check-square-o font-green-jungle"></i>  Passport</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Passport</span>
                                @endif
                            </td>
                            
                            <td>@if($item['active']) Yes @else No @endif</td>
                            
                            <td>
                                 

                                <a type="button" title="Adjust Price"  class="btn btn-default" data-toggle="modal" data-target="#myModal<?php echo $item['id'] ?>"> <i class="fa fa-money"></i> </a>


                                <a href="{{ url('trips/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                </a> 

                                <!--
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['trips', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'onclick'=>'ConfirmDelete()', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}

                                -->
                            </td>
                        </tr>
                        <!-- Modal -->
<div class = "modal fade" id="myModal<?php echo $item['id'] ?>" tabindex = "-1" role = "dialog" aria-labelledby = "myModalLabel" aria-hidden = "true">
   <div class = "modal-dialog">
      <div class = "modal-content">
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            <h4 class = "modal-title" id = "myModalLabel">
               Adjust {{ $item['name'] }} Price
            </h4>
         </div>
         <form action="" method="post">
         <div class = "modal-body" id="contentArea">
           Fare: <input type="text" id="fare{{ $item['id'] }}" value="{{ $item['fare'] }}">
         </div>
         <div class = "modal-footer">
            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            <button type = "button" id="fareSub{{$item['id'] }}" class = "btn btn-primary">
               Submit changes
            </button>
         </div>
         </form>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <script src="{{ url('bckend/js/libs/jquery-1.10.1.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function(){

    $('#fareSub<?php echo $item["id"] ?>').click(function(){
      var fare = $("#fare<?php echo $item['id'] ?>").val()
      var id = "{{ $item['id'] }}"

        $("#contentArea").html('<img src="{{ asset("bckend/img/facebook.gif") }}" width="100px" /> please wait...');

                                var url = "{{ route('adjust-prices') }}";
                                      $.ajax
                                      ({
                                        type: "POST",
                                        url: url,
                                        data: ({ rnd : Math.random() * 100000, "_token":"{{ csrf_token() }}", fare:fare, id:id}),
                                        success: function(response){
                                          console.log(response);
                                          $('#myModal<?php echo $item["id"] ?>').modal('hide')
                                           location.reload(); 
                                          $('#update<?php echo $item["id"] ?>').text(fare);
                                          $('#flash').text('Fare successfully updated');
                                          
                                        }
                                    });
        })

  })
  
</script>

                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        <!-- Modal -->


        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('trips/create') }}">
                <i class="fa fa-plus"></i> 
                Add New Trip
              </a>

              <br>

             <!--  <div class="form-group">
                <label for="phone">Logo</label>
                            <form id="newHotnessForm" action="{{ route('upload-trip') }}" method="POST" type='file'>
                                
                                <input type="file" size="20" name="userfile" id="imageUpload" class=" ">
                            </form>
                </div> -->
 
            </li>

             <li class="active">
              <a href="{{ route('upload-trip') }}">
                <i class="fa fa-plus"></i> 
                Upload Trip
              </a>

              <br>

            </li>
          </ul>

        </div>

      </div> <!-- /.row -->    





@endsection
