@extends('layouts.master')

@section('content')


  <link rel="stylesheet" href="{{ url('bckend/js/plugins/datepicker/datepicker.css') }}">
 

   <div class="content-header">
        <h2 class="content-header-title">Add Trip </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li><a href="{{ url('trips') }}">Trips </a></li>
          <li class="active">Add Trip </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-12 col-sm-10">

          <div class="portlet">

            <div class="portlet-content">
                
                <form method="post" action="">
                
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


             <div class="col-sm-3">
                <strong>Source Park </strong>
                <div  class="input-group date" data-auto-close="true" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                            <select name="source_park" class="form-control select22" id="select22" style="width:200px">
                                <option value="">Choose Park</option>
                                @foreach($boardable_parks as $park)
                                <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach
                            </select> 
                        {!! $errors->first('source_park', '<span class="parsley-error-list">:message</span>') !!}
                        <!-- <span class="input-group-addon"><i class="fa fa-calendar"></i></span> -->
                </div>
            </div>

            <div class="col-sm-3">
                <strong>Destination Park</strong>
                <div  class="input-group date" data-auto-close="true" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <select name="dest_park" class="form-control select22" style="width:200px">
                                <option value="">Choose Park</option>
                                @foreach($all_parks as $park)
                                <option value="{{ $park->id }}">{{ $park->name }}</option>
                                @endforeach
                    </select>
                    {!! $errors->first('dest_park', '<span class="parsley-error-list">:message</span>') !!}
                </div>
            </div>
                    
            <div class="col-sm-3">
                <strong>Departure Date</strong>
                <div id="dp-ex-3" class="input-group date" data-auto-close="true"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                    <input class="form-control" type="text" name="departure_date" value="{{ $date }}">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <!-- <span class="help-block">dd-mm-yyyy</span> -->
            </div>


<br>
                <input type="submit" value="Search Trip" class="btn btn-danger">               
                    
    
                </form>
             </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->
    
         

        </div> <!-- /.col -->

      

      </div> <!-- /.row -->   

@if(!empty($trips))
 <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>Operator</th>
                      <th>Leaving From</th>
                      <th>Going to</th>
                      <th>Departure time</th>
                      <th>Fare</th>
                      <th>Bus Type</th>
                      <th>Features</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($trips as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <!--td>{{ $x }}</td-->
                            <td>{{ $item['operator']['name'] }}</td>
                            <td>{{ $item['sourcepark']['name'] }}</td>
                            <td>{{ $item['destpark']['name'] }}</td>
                            <td>{{ $item['departure_time'] }}</td>
                            <td> &#8358; {{ number_format($item['fare']) }}</td>
                            <td>{{ $item['bustype']['name'] }}</td>
                            <td>
                               @if($item['ac'])
                                <span<i class="fa fa-check-square-o font-green-jungle"></i>  AC</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  AC</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['security'])
                                    <span><i class="fa fa-check-square-o font-green-jungle"></i>  Security</span>
                                @else
                                <span ><i class="fa fa-times-circle font-red-thunderbird"></i>  Security</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['insurance'])
                                <span ><i class="fa fa-check-square-o font-green-jungle"></i>  Insurance</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Insurance</span>
                                @endif

                                &nbsp;&middot;&nbsp;
        
                                <br/>
                                @if($item['tv'])
                                <span><i class="fa fa-check-square-o font-green-jungle"></i>  TV</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  TV</span>
                                @endif
                                
                                &nbsp;&middot;&nbsp;

                                @if($item['passport'])
                                <span><i class="fa fa-check-square-o font-green-jungle"></i>  Passport</span>
                                @else
                                <span><i class="fa fa-times-circle font-red-thunderbird"></i>  Passport</span>
                                @endif
                            </td>
                            
                            

                            <td>
                                <!--
                                <a href="{{ url('trips/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['trips', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'onclick'=>'ConfirmDelete()', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                                -->

                                <a href="{{ route('admin-book', [$item['id'], $d_date]) }}" class="btn btn-primary btn-large">BOOK</a> 

                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

           <div class="portlet">

            <div class="portlet-content"> 

                FILTER SECTION  
                
                <strong> </strong>
                
                <form method="post" action="">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="source_park" value="<?php echo $source_park ?>">
                <input type="hidden" name="dest_park" value="<?php echo $dest_park; ?>">

                     <select name="operator_filter" class="form-control">
                        <option value="" >Choose operator</option>
                         @foreach($operators as $p)
                         <option value="{{ $p['id'] }}">{{ $p['name'] }}</option>       
                         @endforeach
                     </select>
                    <br>
                    <input type="submit" value="Filter" class="btn btn-danger">                    
                 </form>

            </div>
           </div>

        </div>

      </div> <!-- /.row -->  
@endif
  
 
  

  
@endsection