@extends('layouts.master')

@section('content')

    <h1>Trip</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Duration</th><th>Fare</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $trip->id }}</td> <td> {{ $trip->name }} </td><td> {{ $trip->duration }} </td><td> {{ $trip->fare }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection