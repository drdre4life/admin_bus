@extends('layouts.master')

@section('content')

	<link rel="stylesheet" href="{{ asset('bckend/js/plugins/morris/morris.css') }}">
  <link rel="stylesheet" href="{{ asset('bckend/js/plugins/icheck/skins/minimal/blue.css') }}">
  <link rel="stylesheet" href="{{ asset('bckend/js/plugins/select2/select2.css') }}">
  <link rel="stylesheet" href="{{ asset('bckend/js/plugins/fullcalendar/fullcalendar.css') }}">

      <div class="content-header">
        <h2 class="content-header-title">Welcome {{ Auth::user()->first_name }} </h2>

        <div style="margin-top: -25px;">

          <h4 class="heading-inline pull-right">
                 &nbsp;&nbsp;<small>Showing data from <strong><?php echo date('M d, Y', strtotime($start_date)); ?></strong> to <strong><?php echo date('M d, Y') ?></strong></small>

          &nbsp;&nbsp;</h4>
      </div>

	    <br><br>
        
      </div> <!-- /.content-header -->


      

      

      <div class="row">

        <div class="col-sm-6 col-md-2">
          <div class="row-stat">
            <p class="row-stat-label">Paid Bookings</p>
            <h3 class="row-stat-value">&#8358; {{ number_format(((Auth::user()->role->name != 'Operator')?$paidSum:($paidSum - (150*$paidCount)))) }} </h3>
            <!--span class="label label-success row-stat-badge">+43%</span-->
          </div> <!-- /.row-stat -->
        </div> <!-- /.col -->

        <div class="col-sm-6 col-md-2">
          <div class="row-stat">
            <p class="row-stat-label">Pending Bookings</p>
            <h3 class="row-stat-value">&#8358; {{ number_format(((Auth::user()->role->name != 'Operator')?$pendSum:($pendSum - (150*$pendCount)))) }}</h3>
            <!--span class="label label-success row-stat-badge">+17%</span-->
          </div> <!-- /.row-stat -->
        </div> <!-- /.col -->

        <div class="col-sm-6 col-md-2">
          <div class="row-stat">
            <p class="row-stat-label">Total Bookings</p>
            <h3 class="row-stat-value">{{ number_format($totalCount) }}</h3>
            <!--span class="label label-success row-stat-badge">+26%</span-->
          </div> <!-- /.row-stat -->
        </div> <!-- /.col -->

        <div class="col-sm-6 col-md-2">
          <div class="row-stat">
            <p class="row-stat-label">Paid Booking</p>
            <h3 class="row-stat-value">{{ number_format($paidCount) }}</h3>
            <!--span class="label label-danger row-stat-badge">+5%</span-->
          </div> <!-- /.row-stat -->
        </div> <!-- /.col -->

        <div class="col-sm-6 col-md-2">
          <div class="row-stat">
            <p class="row-stat-label">Pending Booking</p>
            <h3 class="row-stat-value">{{ number_format($pendCount) }}</h3>
            <!--span class="label label-danger row-stat-badge">+5%</span-->
          </div> <!-- /.row-stat -->
        </div> <!-- /.col -->

        <div class="col-sm-6 col-md-2">
          <div class="row-stat">
            <p class="row-stat-label">Chartered Booking</p>
            <h3 class="row-stat-value">{{ number_format($cBookingsNo) }} [ &#8358; {{ number_format($charteredSum) }} ]</h3>
            <!--span class="label label-danger row-stat-badge">+5%</span-->
          </div> <!-- /.row-stat -->
        </div> <!-- /.col -->
        
      </div> <!-- /.row -->


      <br>



      <div class="row">

        <div class="col-md-8">

          

          <div class="portlet portlet-table">

            <div class="portlet-header">

              <h3>
                <i class="fa fa-group"></i>
                Recent Bookings
              </h3>

              <ul class="portlet-tools pull-right">
                <li>
                  <a class="btn btn-sm btn-default" href="{{ url('trips/search') }}" >
                    Book a Trip
                  </a>
                </li>
              </ul>

            </div> <!-- /.portlet-header -->

            <div class="portlet-content">


              <div class="table-responsive">

                <table id="user-signups" class="table table-striped table-bordered table-checkable"> 
                  <thead> 
                    <tr>
                      <th>Booking code</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Final Cost</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead> 

                  <tbody> 
                    {{-- */$x=0;/* --}}
                    @foreach($bookings as $item)

                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $item['booking_code'] }}</td>
                            <td>{{ $item['trip']['sourcepark']['name'] }}</td>
                            <td>{{ $item['trip']['destpark']['name'] }}</td>
                            <td>&#8358;&#8358;{{ number_format(((Auth::user()->role->name != 'Operator')?$item['final_cost']:$item['final_cost'] - 150)) }}</td>
                            <td>{{ $item['status'] }}</td>
                            <td>
                                <a href="{{ url('bookings/' . $item['id'] ) }}" title="View">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                </a> 

                                <!--a href="{{ url('bookings/' . $item['id'] . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                </a--> 
                                <!--
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['bookings', $item['id']],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit', 'onclick'=>'ConfirmDelete()']) !!}
                                {!! Form::close() !!}
                                -->
                            </td>
                        </tr>
                    @endforeach  

                  </tbody> 

                </table>
                  

              </div> <!-- /.table-responsive -->
                  
            </div> <!-- /.portlet-content -->

            <!--div class="portlet-footer">
              <div class="text-right">                  
                Apply to Selected: &nbsp;&nbsp;
                <select id="apply-selected" name="table-select" class="ui-select2" style="width: 125px">
                  <option value="">Select Action</option>
                  <option value="approve">Approve</option>
                  <option value="edit">Edit</option>
                  <option value="delete">Delete</option>
                  
                </select>
              </div>
            </div> <!-- /.portlet-footer -->

          </div> <!-- /.portlet -->



        </div> <!-- /.col -->



        <div class="col-md-4">

          <h4>Quick Actions</h4>
          <hr/>
          <div class="list-group">

	          <a class="list-group-item" href="{{ url('bookings') }}">
	            <i class="fa fa-list"></i>
	            &nbsp;&nbsp;<strong>View </strong> Bookings
	          </a>

	          <a class="list-group-item" href="{{ url('trips') }}">
	            <i class="fa fa-copy"></i>
	            &nbsp;&nbsp;<strong>Manage</strong> Trips
	          </a>

	          <a class="list-group-item" href="{{ url('trips/create')  }}">
	            <i class="fa fa-pencil"></i>
	            &nbsp;&nbsp;<strong>Add a</strong> Trip
	          </a>
              <a class="list-group-item" href="{{ url('download-trips') }}">
                  <i class="fa fa-file-excel-o"></i>
                  &nbsp;&nbsp;<strong>Download</strong> Excel Template
              </a>

              <a class="list-group-item" href="{{ url('upload-trip') }}">
                  <i class="fa fa-upload"></i>
                  &nbsp;&nbsp;<strong>Upload</strong> Trips In Bulk
              </a>

            <a class="list-group-item" href="{{ url('guarantees')  }}">
              <i class="fa fa-signal"></i>
              &nbsp;&nbsp;<strong>View </strong>Guarantee History
            </a>

            <a class="list-group-item" href="{{ url('fin-summary')  }}">
              <i class="fa fa-money"></i>
              &nbsp;&nbsp;<strong>View </strong>financial Summary
            </a>

	          <!--a class="list-group-item" href="javascript:;">
	            <i class="fa fa-times"></i>
	            &nbsp;&nbsp;<strong>Delete</strong> Invoice
	          </a-->

	       </div>


	

        </div> <!-- /.col -->

      </div> <!-- /.row -->

      

  @endsection    