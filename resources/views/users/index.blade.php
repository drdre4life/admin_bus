@extends('layouts.master')

@section('content')

    <div class="content-header">
        <h2 class="content-header-title">Users </h2>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Dashboard</a></li>
          <li class="active">Users </li>
        </ol>
      </div> <!-- /.content-header -->

      

      <div class="row">

        <div class="col-md-10 col-sm-8">

          <div class="portlet">

            <div class="portlet-content">           

              <div class="table-responsive">

              <table 
                class="table table-striped table-bordered table-hover table-highlight table-checkable" 
                data-provide="datatable" 
                data-display-rows="10"
                data-info="true"
                data-search="true"
                data-length-change="true"
                data-paginate="true"
              >
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Name</th>
                      <th>Operator</th>
                      <th>Role</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {{-- */$x=0;/* --}}
                    @foreach($users as $item)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ url('users', $item->id) }}">{{ $item->last_name.' '.$item->first_name }}</a></td>
                            <td>{{ (!empty($item->operator))?$item->operator->name:"" }}</td>
                            <td>{{ (!empty($item->role))?$item->role->name:"" }}</td>
                            <td>
                                <a href="{{ url('users/' . $item->id . '/edit') }}" title="Edit">
                                    <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-edit"></i></button>
                                </a> 
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['users', $item->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                    {!! Form::button('<i class="fa fa-times"></i>', ['class' => 'btn btn-danger btn-xs', 'escape'=>false,'title'=>'Delete', 'type'=>'submit']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div> <!-- /.table-responsive -->
              

            </div> <!-- /.portlet-content -->

          </div> <!-- /.portlet -->

        

        </div> <!-- /.col -->

        <div class="col-md-2 col-sm-4">

            <ul id="myTab" class="nav nav-pills nav-stacked">
            <li class="active">
              <a href="{{ url('users/create') }}">
                <i class="fa fa-plus"></i> 
                Add New User
              </a>
            </li>
          </ul>

        </div>

      </div> <!-- /.row -->    



@endsection
