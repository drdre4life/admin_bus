@extends('layouts.master')

@section('content')

    <h1>User</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Last Name</th><th>First Name</th><th>Email</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $user->id }}</td> <td> {{ $user->last_name }} </td><td> {{ $user->first_name }} </td><td> {{ $user->email }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection